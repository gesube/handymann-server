package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.ToolDAO;
import de.fhmuenster.handymann.dto.requests.ToolRequestDTO;
import de.fhmuenster.handymann.dto.responses.ToolResponseDTO;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Session;
import de.fhmuenster.handymann.entities.Tool;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/tools")
@Api(value = "tools", description = "endpoint for tool management")
public class ToolController {

    // FA-12: Der Anwender muss eine Möglichkeit zur Nachbestellung von Werkzeugen besitzen.

    @Autowired
    private UserVerificationService userService;

    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ToolDAO toolDao;

    /**
     * REST GET Method to find all tools
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<ToolResponseDTO> - a list of ToolResponseDTO objects with the tools if there was
     *         no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<ToolResponseDTO> getAllToolsForUser(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);
            final User user = session.getUser();
            final Occupation occupationOfUser = user.getEmployee().getOccupation();

            final List<Tool> tools =
                    this.toolDao.findByAvailableForTheseOccupationsIn(occupationOfUser);

            if (tools.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfResponseDtos(tools);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to find a tool by its id (/{id})
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the tool to search for
     * @return ToolResponseDTO - a ToolResponseDTO object with the tool if there was no error, else
     *         null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ToolResponseDTO getToolDetailsById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Tool> tool = this.toolDao.findById(id);

            if (!tool.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildResponseDto(tool.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * REST POST Method to order tools (/order/)
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the tool to search for
     * @return ToolResponseDTO - a ToolResponseDTO object with the tool if there was no error, else
     *         null
     * @throws IOException if there is an error with the response.sendError method
     */
    @PostMapping(value = "/order")
    @ResponseStatus(HttpStatus.OK)
    public List<ToolResponseDTO> orderTools(final HttpServletRequest request,
            final HttpServletResponse response, @RequestBody final List<ToolRequestDTO> requestBody)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);
            final User user = session.getUser();
            final List<ToolResponseDTO> responseList = new ArrayList<>();

            for (final ToolRequestDTO requestDto : requestBody) {
                final Optional<Tool> tool = this.toolDao.findById(requestDto.getToolId());

                if (tool.isPresent()) {
                    final Tool entityToUpdate = tool.get();

                    if (entityToUpdate.getToolOrders().get(user) == null) {
                        entityToUpdate.addToolOrder(user, requestDto.getQuantity());
                    } else {
                        entityToUpdate.getToolOrders().get(user).add(requestDto.getQuantity());
                    }

                    this.toolDao.save(entityToUpdate);
                    final ToolResponseDTO responseDto = this.buildResponseDto(tool.get());
                    responseList.add(responseDto);
                }
            }

            return responseList;
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * Builds a ToolResponseDTO from a Tool.
     *
     * @param tool - the tool to build the responseDTO upon
     * @return ToolResponseDTO - the built responseDTO.
     */
    private ToolResponseDTO buildResponseDto(final Tool tool) {
        final ToolResponseDTO responseDto =
                this.mappingService.getModelMapper().map(tool, ToolResponseDTO.class);

        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(tool.getImage().getFileLocation()));

        return responseDto;
    }

    /**
     * Builds a list of ToolResponseDTO from a list of Tools.
     *
     * @param tools - the list of tools to build the list of responseDTOs upon
     * @return List<ToolResponseDTO> - the built list of responseDTOs.
     */
    private List<ToolResponseDTO> buildListOfResponseDtos(final List<Tool> tools) {
        final List<ToolResponseDTO> responseDtoList = new ArrayList<>();

        for (final Tool tool : tools) {
            responseDtoList.add(this.buildResponseDto(tool));
        }

        return responseDtoList;
    }

}

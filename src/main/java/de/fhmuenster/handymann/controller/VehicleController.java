package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dao.VehicleDAO;
import de.fhmuenster.handymann.dto.requests.MiniVehicleRequestDTO;
import de.fhmuenster.handymann.dto.responses.VehicleResponseDTO;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.Vehicle;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/vehicles")
@Api(value = "vehicles", description = "endpoint for vehicle management")
public class VehicleController {

    @Autowired
    private UserVerificationService userService;

    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private VehicleDAO vehDao;

    @Autowired
    private TeamDAO teamDao;

    /**
     * REST GET Method to find all vehicles
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<VehicleResponseDTO> - a list of VehicleResponseDTO objects with the vehicles if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleResponseDTO> getAllVehicles(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final List<Vehicle> vehicles = this.vehDao.findAll();

            if (vehicles.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfResponseDtos(vehicles);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }
    
    /**
     * REST GET Method to find all available vehicles
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<VehicleResponseDTO> - a list of VehicleResponseDTO objects with the vehicles if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/available/")
    @ResponseStatus(HttpStatus.OK)
    public List<VehicleResponseDTO> getAllAvailableVehicles(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final List<Vehicle> vehicles = this.vehDao.findByCurrentPossessorIsNull();

            if (vehicles.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfResponseDtos(vehicles);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to find a vehicle by its id (/{id})
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the tool to search for
     * @return VehicleResponseDTO - a VehicleResponseDTO object with the vehicle if there was no
     *         error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public VehicleResponseDTO getVehicleDetailsById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Vehicle> vehicle = this.vehDao.findById(id);

            if (!vehicle.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildResponseDto(vehicle.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * REST PATCH Method to update the current possessor of a vehicle by its id
     * (/{id}/currentPossessor)
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the vehicle to search for
     * @param requestBody - a {@link MiniVehicleeRequestDTO} as the body of the request
     * @return VehicleResponseDTO - a VehicleResponseDTO object with the machine if there was no
     *         error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @PatchMapping("/{id}/currentPossessor")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public VehicleResponseDTO updateCurrentPossessor(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id,
            @Valid @RequestBody final MiniVehicleRequestDTO requestBody) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Vehicle> vehicle = this.vehDao.findById(id);
            final Optional<Team> team = this.teamDao.findById(requestBody.getTeamId());

            if (!vehicle.isPresent() || !team.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                final Vehicle entityToUpdate = vehicle.get();
                entityToUpdate.setCurrentPossessor(team.get());
                final Vehicle savedEntity = this.vehDao.save(entityToUpdate);

                return this.buildResponseDto(savedEntity);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * Builds a VehicleResponseDTO from a Vehicle.
     *
     * @param vehicle - the vehicle to build the responseDTO upon
     * @return VehicleResponseDTO - the built responseDTO.
     */
    private VehicleResponseDTO buildResponseDto(final Vehicle vehicle) {
        final VehicleResponseDTO responseDto =
                this.mappingService.getModelMapper().map(vehicle, VehicleResponseDTO.class);
        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(vehicle.getImage().getFileLocation()));

        return responseDto;
    }

    /**
     * Builds a list of vehicleResponseDTOs from a list of Vehicles.
     *
     * @param vehicles - the list of vehicles to build the list of responseDTOs upon
     * @return List<VehicleResponseDTO> - the built list of responseDTOs.
     */
    private List<VehicleResponseDTO> buildListOfResponseDtos(final List<Vehicle> vehicles) {
        final List<VehicleResponseDTO> responseDtoList = new ArrayList<>();

        for (final Vehicle vehicle : vehicles) {
            responseDtoList.add(this.buildResponseDto(vehicle));
        }

        return responseDtoList;
    }
}

package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.JobDAO;
import de.fhmuenster.handymann.dao.JobSiteDAO;
import de.fhmuenster.handymann.dao.TaskDAO;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dto.requests.TaskRequestDTO;
import de.fhmuenster.handymann.dto.responses.MiniTaskResponseDTO;
import de.fhmuenster.handymann.dto.responses.TaskResponseDTO;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Session;
import de.fhmuenster.handymann.entities.Task;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.enums.EnumStatus;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/tasks")
@Api(value = "tasks", description = "endpoint for task management")
public class TaskController {

	@Autowired
	private UserVerificationService userService;

	@Autowired
	private ModelMappingService mappingService;

	@Autowired
	private TaskDAO taskDao;

	@Autowired
	private JobSiteDAO jsDao;

	@Autowired
	private JobDAO jDao;

	@Autowired
	private ImageService iService;

	@Autowired
	private TeamDAO teamDao;

	/**
	 * REST GET Method to find a Task by its id
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return TaskResponseDTO - a TaskResponseDTO object, if there was no error, else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/{taskId}")
	@ResponseStatus(HttpStatus.OK)
	public TaskResponseDTO getTaskDetailsById(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("taskId") final Long taskId)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			this.userService.getSession(request);
			final Optional<Task> task = this.taskDao.findById(taskId);

			if (!task.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildTaskResponseDto(task.get());
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}

	}

	/**
	 * REST GET Method to find all tasks of job
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @param creationdate - date of job-creationdate
	 * @return List<TaskResponseDTO> - a list of TaskResponseDTO objects, if there was no error,
	 *         else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/job/{jobId}")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskResponseDTO> getTasksByJob(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("jobId") final Long jobId)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			this.userService.getSession(request);

			final Optional<Job> job = this.jDao.findById(jobId);

			if (!job.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}

			final List<Task> tasks = this.taskDao.findByJob(job.get());

			if (tasks.isEmpty()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildListOfTaskResponseDtos(tasks);
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST GET Method to find all tasks of jobsite
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @param creationdate - date of jobsite-creationdate
	 * @return List<TaskResponseDTO> - a list of TaskResponseDTO objects, if there was no error,
	 *         else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/jobsite/{jobSiteId}")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskResponseDTO> getTasksByJobSite(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("jobSiteId") final Long jobSiteId)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			this.userService.getSession(request);

			final Optional<JobSite> jobSite = this.jsDao.findById(jobSiteId);

			if (!jobSite.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}

			final List<Task> tasks = this.taskDao.findByJobSite(jobSite.get());

			if (tasks.isEmpty()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildListOfTaskResponseDtos(tasks);
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST GET Method to find all available tasks
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return List<TaskResponseDTO> - a list of TaskResponseDTO objects, if there was no error,
	 *         else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/availableTasks")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskResponseDTO> getAvailableTasksForCurrentJobSiteAndUsersOccupation(
			final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			final Session session = this.userService.getSession(request);
			final User user = session.getUser();
			final Occupation userOccupation = user.getEmployee().getOccupation();

			final List<Team> teams = this.teamDao.findByMembersInOrderByValidForDateDesc(user);

			if (teams.isEmpty()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				final Team currentTeam = teams.get(0);

				if (currentTeam.getCurrentJobSite() == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
					return null;
				} else {
					final List<Task> tasks =
							this.taskDao.findByStatusAndJobSiteAndAvailableForTheseOccupationsIn(
									EnumStatus.OPEN, currentTeam.getCurrentJobSite(),
									userOccupation);

					tasks.addAll(
							this.taskDao.findByStatusAndJobSiteAndAvailableForTheseOccupationsIn(
									EnumStatus.IN_PROGRESS, currentTeam.getCurrentJobSite(),
									userOccupation));

					if (tasks.isEmpty()) {
						response.sendError(HttpServletResponse.SC_NOT_FOUND);
						return null;
					} else {
						return buildListOfTaskResponseDtos(tasks);
					}
				}
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}


	/**
	 * REST GET Method to find all tasks of jobsite
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @param creationdate - date of jobsite-creationdate
	 * @return List<TaskResponseDTO> - a list of TaskResponseDTO objects, if there was no error,
	 *         else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/jobsite/{status}/{id}")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskResponseDTO> getTasksByStatusAndJobSiteAndUserOccupation(
			final HttpServletRequest request, final HttpServletResponse response,
			@PathVariable("status") final String status, @PathVariable("id") final Long id)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			final Session session = this.userService.getSession(request);
			final User user = session.getUser();
			final Occupation userOccupation = user.getEmployee().getOccupation();

			final Optional<JobSite> jobSite = this.jsDao.findById(id);

			if (!jobSite.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				final EnumStatus taskStatus = EnumStatus.fromValue(status);


				final List<Task> tasks =
						this.taskDao.findByStatusAndJobSiteAndAvailableForTheseOccupationsIn(
								taskStatus, jobSite.get(), userOccupation);

				if (tasks.isEmpty()) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
					return null;
				} else {
					return buildListOfTaskResponseDtos(tasks);
				}
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST GET Method to find all tasks of jobsite
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return List<TaskResponseDTO> - a list of TaskResponseDTO objects, if there was no error,
	 *         else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/user/team/")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskResponseDTO> getDoneTasksFromTodayForUsersTeam(
			final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			final Session session = this.userService.getSession(request);
			final User user = session.getUser();
			final List<Team> teams = this.teamDao.findByMembersInOrderByValidForDateDesc(user);

			if (teams.isEmpty()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				final Team currentTeam = teams.get(0);
				
				final Date nowStartDate = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin"))
	                    .withHour(0).withMinute(0).withSecond(0).minusHours(1).toInstant());

	            final Date nowEndDate = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin"))
	                    .withHour(23).withMinute(59).withSecond(59).minusHours(1).toInstant());

				final List<Task> tasks = this.taskDao.findByStatusAndFinishDateBetweenAndAssigneeIn(
						EnumStatus.CLOSED, nowStartDate, nowEndDate, currentTeam.getMembers());

				if (tasks.isEmpty()) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
					return null;
				} else {
					return buildListOfTaskResponseDtos(tasks);
				}
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST POST Method for task creation
	 *
	 * @param request the request object, created by the container
	 * @param response the response object, created by the container - containing the new session id
	 * @param taskRequestDto the DTO container with the task request
	 * @return a TaskResponseDTO object with the task
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@PostMapping("/create")
	@ResponseBody
	public TaskResponseDTO createTask(final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody final TaskRequestDTO taskRequestDto)
			throws IOException {
		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			final Session session = this.userService.getSession(request);

			Task task = new Task();

			task.setCreator(session.getUser());

			// In dieser Methode werden Parameter gesetzt, die bei Create und Update gleich sind
			addTaskDetails(task, taskRequestDto);

			task = this.taskDao.save(task);

			return buildTaskResponseDto(task);
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST PUT Method for task update
	 *
	 * @param request the request object, created by the container
	 * @param response the response object, created by the container - containing the new session id
	 * @param taskRequestDto the DTO container with the task request
	 * @return a TaskResponseDTO object with the task
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@PutMapping("/update/{id}")
	@ResponseBody
	public TaskResponseDTO updateTask(final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody final TaskRequestDTO taskRequestDto,
			@PathVariable("id") final Long id) throws IOException {
		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			this.userService.getSession(request);

			final Optional<Task> optionalTask = this.taskDao.findById(id);

			if (optionalTask.isPresent()) {
				Task task = optionalTask.get();

				if (task.getStatus() != EnumStatus.CLOSED
						&& taskRequestDto.getStatus() == EnumStatus.CLOSED) {
					task.setFinishDate(new Date());
				}

				task.setPriority(taskRequestDto.getPriority());
				task.setStatus(taskRequestDto.getStatus());
				task.setDurationInH(taskRequestDto.getDurationInH());

				task = this.taskDao.save(task);

				return buildTaskResponseDto(task);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST PATCH Method to update the assignee of a task by its id (/{id})
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @param id - the id of the machine to search for
	 * @return TaskResponseDTO - a TaskResponseDTO object with the task if there was no error, else
	 *         null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@PatchMapping("/{id}")
	@ResponseBody
	public TaskResponseDTO updateAssignee(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("id") final Long id)
			throws IOException {
		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			final Session session = this.userService.getSession(request);
			final User user = session.getUser();

			final Optional<Task> optionalTask = this.taskDao.findById(id);

			if (optionalTask.isPresent()) {
				Task task = optionalTask.get();
				task.setAssignee(user);
				task = this.taskDao.save(task);

				return buildTaskResponseDto(task);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * In dieser Methode werden Parameter gesetzt, die bei Create und Update gleich sind
	 */
	private void addTaskDetails(final Task task, final TaskRequestDTO taskRequestDto) {
		if (task.getStatus() != EnumStatus.CLOSED
				&& taskRequestDto.getStatus() == EnumStatus.CLOSED) {
			task.setFinishDate(new Date());
		}

		task.setPriority(taskRequestDto.getPriority());
		task.setStatus(taskRequestDto.getStatus());
		task.setDurationInH(taskRequestDto.getDurationInH());
	}

	/**
	 * Builds a MiniTaskResponseDTO from a Task.
	 *
	 * @param task - the task to build the responseDTO upon
	 * @return MiniTaskResponseDTO - the built responseDTO.
	 */
	private MiniTaskResponseDTO buildMiniTaskResponseDto(final Task task) {
		final MiniTaskResponseDTO responseDto =
				this.mappingService.getModelMapper().map(task, MiniTaskResponseDTO.class);

		return responseDto;
	}

	/**
	 * Builds a list of MiniTaskResponseDTOs from a list of Tasks.
	 *
	 * @param tasks - the list of tasks to build the list of responseDTOs upon
	 * @return List<MiniTaskResponseDTO> - the built list of responseDTOs.
	 */
	private List<MiniTaskResponseDTO> buildListOfMiniTaskResponseDtos(final List<Task> tasks) {
		final List<MiniTaskResponseDTO> responseDtoList = new ArrayList<>();

		for (final Task task : tasks) {
			responseDtoList.add(buildMiniTaskResponseDto(task));
		}

		return responseDtoList;
	}

	/**
	 * Builds a TaskResponseDTO from a Task.
	 *
	 * @param task - the task to build the responseDTO upon
	 * @return TaskResponseDTO - the built responseDTO.
	 */
	private TaskResponseDTO buildTaskResponseDto(final Task task) {
		final TaskResponseDTO responseDto =
				this.mappingService.getModelMapper().map(task, TaskResponseDTO.class);

		final User creator = task.getCreator();
		final User assignee = task.getAssignee();

		if (creator != null) {
			responseDto.getCreatorImage().setImageAsBaseString(
					this.iService.encodeImage(task.getCreator().getImage().getFileLocation()));
		}

		if (assignee != null) {
			responseDto.getCreatorImage().setImageAsBaseString(
					this.iService.encodeImage(task.getAssignee().getImage().getFileLocation()));
		}

		return responseDto;
	}

	/**
	 * Builds a list of TaskResponseDTOs from a list of Tasks.
	 *
	 * @param tasks - the list of tasks to build the list of responseDTOs upon
	 * @return List<TaskResponseDTO> - the built list of responseDTOs.
	 */
	private List<TaskResponseDTO> buildListOfTaskResponseDtos(final List<Task> tasks) {
		final List<TaskResponseDTO> responseDtoList = new ArrayList<>();

		for (final Task task : tasks) {
			responseDtoList.add(buildTaskResponseDto(task));
		}

		return responseDtoList;
	}
}

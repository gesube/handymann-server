package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.MachineDAO;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dto.requests.MiniMachineRequestDTO;
import de.fhmuenster.handymann.dto.responses.MachineResponseDTO;
import de.fhmuenster.handymann.entities.Machine;
import de.fhmuenster.handymann.entities.Session;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/machines")
@Api(value = "machines", description = "endpoint for machine management")
public class MachineController {

    // FA-14: Der Anwender muss eine Möglichkeit zum Finden des Standortes einer Maschine besitzen.

    @Autowired
    private UserVerificationService userService;

    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private MachineDAO macDao;

    @Autowired
    private TeamDAO teamDao;

    /**
     * REST GET Method to find all machines
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<MachineResponseDTO> - a list of MachineResponseDTO objects with the machine if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<MachineResponseDTO> getAllMachines(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final List<Machine> machines = this.macDao.findAll();

            if (machines.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfResponseDtos(machines);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * REST GET Method to find all machines assigned to the requesters team by its id
     * (/user/{userId})
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<MachineResponseDTO> - a list of MachineResponseDTO objects with the machine if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public List<MachineResponseDTO> getMachinesForUsersTeam(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("userId") final Long userId)
            throws IOException {

        try {
            final Session session = this.userService.getSession(request);

            // TODO: diese Zeilen überarbeiten - es gibt sicherlich schönere Lösungen
            final List<Team> teams =
                    this.teamDao.findByMembersInOrderByValidForDateDesc(session.getUser());
            final List<Machine> machines = this.macDao.findByCurrentPossessor(teams.get(0));

            if (machines.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfResponseDtos(machines);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * REST GET Method to find a machine by its id (/{id})
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the machine to search for
     * @return MachineResponseDTO - a MachineResponseDTO object with the machine if there was no
     *         error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MachineResponseDTO getMachineDetailsById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Machine> machine = this.macDao.findById(id);

            if (!machine.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildResponseDto(machine.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * REST PATCH Method to update the current possessor of a machine by its id
     * (/{id}/currentPossessor)
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the machine to search for
     * @param requestBody - a {@link MiniMachineRequestDTO} as the body of the request
     * @return MachineResponseDTO - a MachineResponseDTO object with the machine if there was no
     *         error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @PatchMapping("/{id}/currentPossessor")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public MachineResponseDTO updateCurrentPossessor(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id,
            @Valid @RequestBody final MiniMachineRequestDTO requestBody) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Machine> machine = this.macDao.findById(id);
            final Optional<Team> team = this.teamDao.findById(requestBody.getTeamId());

            if (!machine.isPresent() || !team.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                final Machine entityToUpdate = machine.get();
                entityToUpdate.setCurrentPossessor(team.get());
                final Machine savedEntity = this.macDao.save(entityToUpdate);

                return this.buildResponseDto(savedEntity);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }


    /**
     * Builds a MachineResponseDTO from a Machine.
     *
     * @param machine - the machine to build the responseDTO upon
     * @return MachineResponseDTO - the built responseDTO.
     */
    private MachineResponseDTO buildResponseDto(final Machine machine) {
        final MachineResponseDTO responseDto =
                this.mappingService.getModelMapper().map(machine, MachineResponseDTO.class);

        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(machine.getImage().getFileLocation()));

        return responseDto;
    }

    /**
     * Builds a list of MachineResponseDTOs from a list of Machines.
     *
     * @param machines - the list of machines to build the list of responseDTOs upon
     * @return List<MachineResponseDTO> - the built list of responseDTOs.
     */
    private List<MachineResponseDTO> buildListOfResponseDtos(final List<Machine> machines) {
        final List<MachineResponseDTO> responseDtoList = new ArrayList<>();

        for (final Machine machine : machines) {
            responseDtoList.add(this.buildResponseDto(machine));
        }

        return responseDtoList;
    }
}

package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.MaterialDAO;
import de.fhmuenster.handymann.dto.responses.MaterialResponseDTO;
import de.fhmuenster.handymann.entities.Material;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/materials")
@Api(value = "materials", description = "endpoint for material management")
public class MaterialController {

    @Autowired
    private UserVerificationService userService;

    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private MaterialDAO matDao;

    /**
     * REST GET Method to find all vehicles
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<MaterialResponseDTO> - a list of VehicleResponseDTO objects with the vehicles if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/")
    @ResponseStatus(HttpStatus.OK)
    public List<MaterialResponseDTO> getAllMaterial(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final List<Material> materials = this.matDao.findAll();

            if (materials.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfResponseDtos(materials);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to find a vehicle by its id (/{id})
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the tool to search for
     * @return VehicleResponseDTO - a VehicleResponseDTO object with the vehicle if there was no
     *         error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MaterialResponseDTO getMaterialDetailsById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Material> material = this.matDao.findById(id);

            if (!material.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildResponseDto(material.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * Builds a MaterialResponseDTO from a Material.
     *
     * @param material - the material to build the responseDTO upon
     * @return MaterialResponseDTO - the built responseDTO.
     */
    private MaterialResponseDTO buildResponseDto(final Material material) {
        final MaterialResponseDTO responseDto =
                this.mappingService.getModelMapper().map(material, MaterialResponseDTO.class);
        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(material.getImage().getFileLocation()));

        return responseDto;
    }

    /**
     * Builds a list of MaterialResponseDTOs from a list of Materials.
     *
     * @param materials - the list of materials to build the list of responseDTOs upon
     * @return List<MaterialResponseDTO> - the built list of responseDTOs.
     */
    private List<MaterialResponseDTO> buildListOfResponseDtos(final List<Material> materials) {
        final List<MaterialResponseDTO> responseDtoList = new ArrayList<>();

        for (final Material material : materials) {
            responseDtoList.add(this.buildResponseDto(material));
        }

        return responseDtoList;
    }
}

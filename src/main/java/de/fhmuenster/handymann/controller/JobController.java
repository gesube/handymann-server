package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.JobDAO;
import de.fhmuenster.handymann.dao.JobSiteDAO;
import de.fhmuenster.handymann.dao.MachineDAO;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dao.VehicleDAO;
import de.fhmuenster.handymann.dto.requests.JobRequestDTO;
import de.fhmuenster.handymann.dto.requests.StartAndEndDateRequestDTO;
import de.fhmuenster.handymann.dto.requests.WorkTimeRequestDTO;
import de.fhmuenster.handymann.dto.responses.JobResponseDTO;
import de.fhmuenster.handymann.dto.responses.JobResponseMetadataDTO;
import de.fhmuenster.handymann.dto.responses.MaterialResponseDTO;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Machine;
import de.fhmuenster.handymann.entities.Material;
import de.fhmuenster.handymann.entities.Session;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.Vehicle;
import de.fhmuenster.handymann.entities.embeddables.WorkTime;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/jobs")
@Api(value = "jobs", description = "endpoint for job management")
public class JobController {

    @Autowired
    private UserVerificationService userService;

    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private JobDAO jobDao;

    @Autowired
    private JobSiteDAO jobsitedao;

    @Autowired
    private TeamDAO teamDao;

    @Autowired
    private VehicleDAO vehicleDao;

    @Autowired
    private MachineDAO machineDao;

    @Autowired
    private ImageService imageService;

    /**
     * REST POST Method for job creation
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @param jobRequestDto the DTO container with the job request
     * @return a JobResponseDTO object with the job
     * @throws IOException if there is an error with the response.sendError method
     */
    @PostMapping("/create")
    @ResponseBody
    public JobResponseDTO createJob(final HttpServletRequest request,
            final HttpServletResponse response, @RequestBody final WorkTimeRequestDTO requestDto)
            throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);
            


            Job job = new Job();
            job.setCreator(session.getUser());

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());
            job.setValidForDate(today);
            
            final ZonedDateTime wTStart = ZonedDateTime.ofInstant(requestDto.getWorkTimeStart().toInstant(), ZoneId.of("Europe/Berlin"));
            final ZonedDateTime wTEnd = ZonedDateTime.ofInstant(requestDto.getWorkTimeEnd().toInstant(), ZoneId.of("Europe/Berlin"));
            final ZonedDateTime breakStart = ZonedDateTime.ofInstant(requestDto.getBreakTimeStart().toInstant(), ZoneId.of("Europe/Berlin"));
            final ZonedDateTime breakEnd = ZonedDateTime.ofInstant(requestDto.getBreakTimeEnd().toInstant(), ZoneId.of("Europe/Berlin"));
            
            if (wTStart.isAfter(wTEnd) || wTStart.isAfter(breakStart) || wTStart.isAfter(breakEnd) ||
            		breakStart.isAfter(wTEnd) || breakStart.isAfter(breakEnd) ||
            		breakEnd.isAfter(wTEnd)) {
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return null;
            }
            
            job.setWorkTime(new WorkTime(wTStart, wTEnd, breakStart, breakEnd));
            
            final ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));
            job.setCreationDate(now);
            
            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());
            
            if (team.isPresent()) {
            	final Optional<Job> existentJob = this.jobDao.findByTeam(team.get());
            	if (existentJob.isPresent()) {
            		response.sendError(HttpServletResponse.SC_FORBIDDEN);
                    return null;
            	}
            	
            	
            	job.setTeam(team.get());
            	job.setJobSite(team.get().getCurrentJobSite());
            	job.setCanBeChanged(false);
            	
            	team.get().setCanBeChanged(false);
            	
            	job = this.jobDao.save(job);
            	
            	this.teamDao.save(team.get());
            	this.resetCurrentPossessorOfMaschines(team.get());
            	this.resetCurrentPossessorOfVehicle(team.get());
            	
            	return this.buildJobResponseDto(job);
            }

            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST PUT Method for job update
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @param jobRequestDto the DTO container with the job request
     * @return a JobResponseDTO object with the job
     * @throws IOException if there is an error with the response.sendError method
     */
    @PutMapping("/update/{jobId}")
    @ResponseBody
    public JobResponseDTO updateJob(final HttpServletRequest request,
            final HttpServletResponse response, @RequestBody final JobRequestDTO jobRequestDto,
            @PathVariable("jobId") final Long jobId) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);

            final Optional<Job> optionalJob = this.jobDao.findById(jobId);

            if (optionalJob.isPresent()) {
                Job job = optionalJob.get();

                // In dieser Methode werden Parameter gesetzt, die bei Create und Update gleich sind
                this.addJobDetailsHelper(job, jobRequestDto);

                job = this.jobDao.save(job);

                return this.buildJobResponseDto(job);
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * In dieser Methode werden Parameter gesetzt, die bei Create und Update gleich sind
     */
    private void addJobDetailsHelper(final Job job, final JobRequestDTO jobRequestDto) {
        final Optional<JobSite> jobSite = this.jobsitedao.findById(jobRequestDto.getJobSiteId());
        if (jobSite.isPresent()) {
            job.setJobSite(jobSite.get());
        } else {
            job.setJobSite(null);
        }

        final Optional<Team> team = this.teamDao.findById(jobRequestDto.getTeamId());
        if (team.isPresent()) {
            job.setTeam(team.get());
            this.resetCurrentPossessorOfMaschines(team.get());
            this.resetCurrentPossessorOfVehicle(team.get());
        } else {
            job.setTeam(null);
        }

        job.setCanBeChanged(jobRequestDto.isCanBeChanged());

        // Worktime wird erzeugt mit Start-, End- und Pausezeit

        final ZonedDateTime wTStart = ZonedDateTime.ofInstant(jobRequestDto.getWorkTimes().getWorkTimeStart().toInstant(), ZoneId.of("Europe/Berlin"));
        final ZonedDateTime wTEnd = ZonedDateTime.ofInstant(jobRequestDto.getWorkTimes().getWorkTimeEnd().toInstant(), ZoneId.of("Europe/Berlin"));
        final ZonedDateTime breakStart = ZonedDateTime.ofInstant(jobRequestDto.getWorkTimes().getBreakTimeStart().toInstant(), ZoneId.of("Europe/Berlin"));
        final ZonedDateTime breakEnd = ZonedDateTime.ofInstant(jobRequestDto.getWorkTimes().getBreakTimeEnd().toInstant(), ZoneId.of("Europe/Berlin"));

        job.setWorkTime(new WorkTime(wTStart, wTEnd, breakStart, breakEnd));
    }

    /**
     * Resets the current possessor of the maschines of a team
     *
     * @param team the current possessor
     * @return true if it worked
     */
    private boolean resetCurrentPossessorOfMaschines(final Team team) {
        try {
            final List<Machine> machines = this.machineDao.findByCurrentPossessor(team);
            for (final Machine machine : machines) {
                machine.setCurrentPossessor(null);
            }
            this.machineDao.saveAll(machines);
            return true;
        } catch (final Exception e) {
            return false;
        }

    }

    /**
     * Resets the current possessor of a vehicle of a team
     *
     * @param team the current possessor
     * @return true if it worked
     */
    private boolean resetCurrentPossessorOfVehicle(final Team team) {
        try {
            Optional<Vehicle> vehicle = this.vehicleDao.findByCurrentPossessor(team);
            if (vehicle.isPresent()) {
            	vehicle.get().setCurrentPossessor(null);
                this.vehicleDao.save(vehicle.get());
            }
            return true;
        } catch (final Exception e) {
            return false;
        }

    }


    /**
     * REST GET Method to find all jobs
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return List<JobResponseDTO> - a list of JobResponseDTO objects with the jobs if there was no
     *         error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/getAllJobs")
    @ResponseStatus(HttpStatus.OK)
    public List<JobResponseDTO> getAllJobs(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final List<Job> jobs = this.jobDao.findAll();

            if (jobs.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildListOfJobResponseDtos(jobs);
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }


    /**
     * REST GET Method to find a job by its id
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return JobResponseDTO - a JobResponseDTO with the job if there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{jobId}")
    @ResponseStatus(HttpStatus.OK)
    public JobResponseDTO getJobDetailsById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("jobId") final Long jobId)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Job> job = this.jobDao.findById(jobId);

            if (!job.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildJobResponseDto(job.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to find a job by its id
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return JobResponseDTO - a JobResponseDTO with the job if there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/metadata")
    @ResponseStatus(HttpStatus.OK)
    public JobResponseMetadataDTO getJobResponseMetadataDto(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);
            final List<Team> teams =
                    this.teamDao.findByMembersInOrderByValidForDateDesc(session.getUser());

            if (teams.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                final List<Team> reversedTeams = new ArrayList<>();
                reversedTeams.addAll(teams);
                Collections.reverse(reversedTeams);

                final Optional<Job> newestJob = this.findJob(teams);
                final Optional<Job> oldestJob = this.findJob(reversedTeams);

                if (!newestJob.isPresent()) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    return null;
                } else {
                    final JobResponseMetadataDTO responseDto = new JobResponseMetadataDTO();
                    responseDto.setDateOfNewestJob(newestJob.get().getValidForDate());
                    responseDto.setJobToDisplay(this.buildJobResponseDto(newestJob.get()));

                    if (!oldestJob.isPresent()) {
                        responseDto.setDateOfOldestJob(newestJob.get().getValidForDate());
                    } else {
                        responseDto.setDateOfOldestJob(oldestJob.get().getValidForDate());
                    }

                    return responseDto;
                }
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    private Optional<Job> findJob(final List<Team> teams) {
        for (final Team team : teams) {
            final Optional<Job> newestJob = this.jobDao.findByTeam(team);

            if (newestJob.isPresent()) {
                return newestJob;
            }
        }
        return Optional.ofNullable(null);
    }

    /**
     * REST GET Method to find a job by its id
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return JobResponseDTO - a JobResponseDTO with the job if there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @PostMapping(value = "/startAndEndDate")
    @ResponseStatus(HttpStatus.OK)
    public List<JobResponseDTO> getJobDetailsBetweenStartAndEndDate(
            final HttpServletRequest request, final HttpServletResponse response,
            @RequestBody final StartAndEndDateRequestDTO requestDto) throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);
            final User user = session.getUser();

            final List<Team> teams = this.teamDao.findByValidForDateBetweenAndMembersIn(
                    requestDto.getStartDate(), requestDto.getEndDate(), user);

            if (teams.isEmpty()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                final List<Job> jobs = new ArrayList<>();

                for (final Team team : teams) {
                    final Optional<Job> job = this.jobDao.findByTeam(team);

                    if (job.isPresent()) {
                        jobs.add(job.get());
                    }
                }

                if (jobs.isEmpty()) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    return null;
                } else {
                    return this.buildListOfJobResponseDtos(jobs);
                }

            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to the current job
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param teamId the id of the user
     * @return JobResponseDTO - a JobResponseDTO objects with the job if there was no error, else
     *         null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/current")
    @ResponseStatus(HttpStatus.OK)
    public JobResponseDTO getCurrentJobOfUser(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());

            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());

            if (!team.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                final Optional<Job> job = this.jobDao.findByTeam(team.get());

                if (!job.isPresent()) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    return null;
                } else {
                    return this.buildJobResponseDto(job.get());
                }
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * Builds a JobResponseDTO from a Job.
     *
     * @param job - the job to build the responseDTO upon
     * @return JobResponseDTO - the built responseDTO.
     */
    private JobResponseDTO buildJobResponseDto(final Job job) {
        final JobResponseDTO responseDto =
                this.mappingService.getModelMapper().map(job, JobResponseDTO.class);

        final User creator = job.getCreator();

        if (creator != null) {
            responseDto.getCreatorImage().setImageAsBaseString(
                    this.imageService.encodeImage(creator.getImage().getFileLocation()));
        }

        for (final Map.Entry<Material, BigDecimal> entry : job.getUsedMaterial().entrySet()) {
            final MaterialResponseDTO materialResponseDto = this.mappingService.getModelMapper()
                    .map(entry.getKey(), MaterialResponseDTO.class);

            materialResponseDto.getImage().setImageAsBaseString(
                    this.imageService.encodeImage(entry.getKey().getImage().getFileLocation()));

            responseDto.getUsedMaterialList().add(materialResponseDto);
            responseDto.getUsedMaterialQuantities().add(entry.getValue());
        }


        return responseDto;
    }

    /**
     * Builds a list of JobResponseDTOs from a list of Jobs.
     *
     * @param jobs - the list of jobs to build the list of responseDTOs upon
     * @return List<JobResponseDTO> - the built list of responseDTOs.
     */
    private List<JobResponseDTO> buildListOfJobResponseDtos(final List<Job> jobs) {
        final List<JobResponseDTO> responseDtoList = new ArrayList<>();

        for (final Job job : jobs) {
            responseDtoList.add(this.buildJobResponseDto(job));
        }

        return responseDtoList;

    }

}

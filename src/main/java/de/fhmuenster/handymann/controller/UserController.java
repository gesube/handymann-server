package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.JobDAO;
import de.fhmuenster.handymann.dao.SessionDAO;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dao.UserDAO;
import de.fhmuenster.handymann.dto.requests.LoginRequestDTO;
import de.fhmuenster.handymann.dto.requests.StartAndEndDateRequestDTO;
import de.fhmuenster.handymann.dto.responses.LoginResponseDTO;
import de.fhmuenster.handymann.dto.responses.ProfileResponseDTO;
import de.fhmuenster.handymann.dto.responses.WorkTimeResponseDTO;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.Session;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.embeddables.WorkTime;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/user")
@Api(value = "users", description = "endpoint for user management")
public class UserController {

    @Autowired
    private UserVerificationService sessionService;

    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private UserDAO userDao;

    @Autowired
    private SessionDAO sessionDao;

    @Autowired
    private TeamDAO teamDao;

    @Autowired
    private JobDAO jobDao;

    @Autowired
    private ImageService imageService;

    /**
     * REST POST Method for the user login (/login)
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @param loginRequestDto the DTO container with the login request
     * @return a UserDTO object with the user profile
     * @throws IOException if there is an error with the response.sendError method
     */
    @PostMapping("/login")
    @ResponseBody
    @ApiOperation(value = "login", response = LoginResponseDTO.class)
    public LoginResponseDTO login(final HttpServletRequest request,
            final HttpServletResponse response, @RequestBody final LoginRequestDTO loginRequestDto)
            throws IOException {
        final String ip = request.getRemoteAddr();
        final String name = loginRequestDto.getName();
        final String password = loginRequestDto.getPassword();

        final User user = this.userDao.findByName(name);
        // check login credentials
        if (user == null || !user.getPassword().equals(password)) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "invalid login credentials!");
            return null;
        }

        final Session session = this.createSession(user, ip);
        this.sessionDao.save(session);

        response.setHeader(UserVerificationService.HEADER_SESSION_IDENTIFIER,
                session.getSessionId());
        response.setStatus(HttpServletResponse.SC_CREATED);

        return this.buildLoginResponseDto(user);
    }

    /**
     * REST DELETE Method for the user logout (/logout)
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @throws IOException if there is an error with the response.sendError method
     */
    @DeleteMapping("/logout")
    public void logout(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        try {
            final Session session = this.sessionService.getSession(request);
            this.sessionDao.delete(session);
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
        }
    }

    private Session createSession(final User user, final String ip) {
        final Session session = new Session();
        session.setUser(user);
        session.setDate(new Date());
        session.setIpAdress(ip);
        session.setSessionId(UUID.randomUUID().toString());
        return session;
    }

    /**
     * REST GET Method to find an user by its id (/{id})
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param id - the id of the tool to search for
     * @return ToolResponseDTO - a ToolResponseDTO object with the tool if there was no error, else
     *         null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProfileResponseDTO getProfileById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("id") final Long id)
            throws IOException {

        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.sessionService.getSession(request);
            final Optional<User> user = this.userDao.findById(id);

            if (!user.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildResponseDto(user.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }

    }

    /**
     * REST POST Method to the worktime of the user
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param teamId the id of the user
     * @return List<WorktimeResponseDTO> - a list of WorktimeResponseDTO objects with the worktimes
     *         if there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @PostMapping(value = "/worktime")
    @ResponseStatus(HttpStatus.OK)
    public List<WorkTimeResponseDTO> getWorktimeOfUser(final HttpServletRequest request,
            final HttpServletResponse response,
            @RequestBody final StartAndEndDateRequestDTO worktimeRequest) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.sessionService.getSession(request);

            final List<WorkTime> workTimes = new ArrayList<>();

            final List<Team> teams = this.teamDao.findByValidForDateBetweenAndMembersIn(
                    worktimeRequest.getStartDate(), worktimeRequest.getEndDate(),
                    session.getUser());

            for (final Team team : teams) {
                final Optional<Job> job = this.jobDao.findByTeam(team);

                if (job.isPresent()) {
                	WorkTime workTime = job.get().getWorkTime();
                	workTime.setAssociatedDate(ZonedDateTime.ofInstant(job.get().getValidForDate().toInstant(), ZoneId.of("Europe/Berlin")));
                    workTimes.add(workTime);
                }
            }

            return this.buildListOfWorktimeResponseDtos(workTimes);
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * Builds a LoginResponseDTO from an User.
     *
     * @param user - the user to build the responseDTO upon
     * @return LoginResponseDTO - the built responseDTO.
     */
    private LoginResponseDTO buildLoginResponseDto(final User user) {
        final LoginResponseDTO responseDto =
                this.mappingService.getModelMapper().map(user, LoginResponseDTO.class);

        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(user.getImage().getFileLocation()));

        return responseDto;
    }

    /**
     * Builds a ProfileResponseDTO from an User.
     *
     * @param user - the user to build the responseDTO upon
     * @return ProfileResponseDTO - the built responseDTO.
     */
    private ProfileResponseDTO buildResponseDto(final User user) {
        final ProfileResponseDTO responseDto =
                this.mappingService.getModelMapper().map(user, ProfileResponseDTO.class);

        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(user.getImage().getFileLocation()));

        return responseDto;
    }


    /**
     * Builds a WorktimeResponseDTO from a Worktime.
     *
     * @param worktime - the worktime to build the responseDTO upon
     * @return WorktimeResponseDTO - the built responseDTO.
     */
    private WorkTimeResponseDTO buildWorktimeResponseDto(final WorkTime worktime) {
        final WorkTimeResponseDTO responseDto =
                this.mappingService.getModelMapper().map(worktime, WorkTimeResponseDTO.class);

        return responseDto;
    }

    /**
     * Builds a list of worktimeResponseDTOs from a list of WorkTime.
     *
     * @param worktimes - the list of worktimes to build the list of responseDTOs upon
     * @return List<WorktimeResponseDTO> - the built list of responseDTOs.
     */
    private List<WorkTimeResponseDTO> buildListOfWorktimeResponseDtos(
            final List<WorkTime> worktimes) {
        final List<WorkTimeResponseDTO> responseDtoList = new ArrayList<>();

        for (final WorkTime worktime : worktimes) {
            responseDtoList.add(this.buildWorktimeResponseDto(worktime));
        }

        return responseDtoList;
    }
}

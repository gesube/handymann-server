package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import de.fhmuenster.handymann.dao.JobSiteDAO;
import de.fhmuenster.handymann.dao.TaskDAO;
import de.fhmuenster.handymann.dto.responses.ImageResponseDTO;
import de.fhmuenster.handymann.dto.responses.JobSiteResponseDTO;
import de.fhmuenster.handymann.dto.responses.TaskResponseDTO;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Task;
import de.fhmuenster.handymann.enums.EnumStatus;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/jobsites")
@Api(value = "jobsites", description = "endpoint for job site management")
public class JobSiteController {
	
	@Autowired
	private TaskDAO taskDao;
	
	@Autowired
	private JobSiteDAO jobSiteDao;
	
	@Autowired
	private ModelMappingService mappingService;
	
	@Autowired
	private UserVerificationService userService;
	
	@Autowired
    private ImageService imageService;
	
	/**
	 * REST GET Method to get the open tasks
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @param jobSiteId the id of the jobSite
	 * @return List<TaskResponseDTO> - a list of TaskResponseDTO objects with the tasks if
	 *         there was no error, else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/openTasks/{jobSiteId}")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskResponseDTO> getOpenTasksOfJobSite(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("jobSiteId") final Long jobSiteId)
			throws IOException {
		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			this.userService.getSession(request);
			Optional<JobSite> jobSite = this.jobSiteDao.findById(jobSiteId);

			if (jobSite.isPresent()) {
				List<Task> tasks = this.taskDao.findByJobSite(jobSite.get());
				List<Task> openTasks = new ArrayList<Task> ();
				for (Task task : tasks) {
					if (task.getStatus() == EnumStatus.OPEN) {
						openTasks.add(task);
					}
				}
				
				return buildListOfTaskResponseDtos(openTasks);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}

	}
	
	/**
	 * REST GET Method to find all jobSites
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return List<JobSiteResponseDTO> - a list of JobSiteResponseDTO objects with the jobSites if
	 *         there was no error, else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/")
	@ResponseStatus(HttpStatus.OK)
	public List<JobSiteResponseDTO> getAllJobSites(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			//this.userService.getSession(request);
			final List<JobSite> jobSites = this.jobSiteDao.findAll();

			if (jobSites.isEmpty()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildListOfJobSiteResponseDtos(jobSites);
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}

	}
	
	/**
	 * REST GET Method to find a jobSite by its id
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return List<JobSiteResponseDTO> - a JobSiteResponseDTO object with the jobSite if
	 *         there was no error, else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/{jobSiteId}")
	@ResponseStatus(HttpStatus.OK)
	public JobSiteResponseDTO getJobSiteDetailsById(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("jobSiteId") final Long jobSiteId) throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			this.userService.getSession(request);
			final Optional<JobSite> jobSite = this.jobSiteDao.findById(jobSiteId);
			

			if (!jobSite.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildJobSiteResponseDto(jobSite.get());
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}

	}
	
	/**
	 * Builds a TaskResponseDTO from a Task.
	 *
	 * @param task - the task to build the responseDTO upon
	 * @return TaskResponseDTO - the built responseDTO.
	 */
	private TaskResponseDTO buildResponseDto(final Task task) {
		final TaskResponseDTO responseDto =
				this.mappingService.getModelMapper().map(task, TaskResponseDTO.class);

		return responseDto;
	}

	/**
	 * Builds a list of TaskResponseDTOs from a list of Tasks.
	 *
	 * @param tasks - the list of tasks to build the list of responseDTOs upon
	 * @return List<TaskResponseDTO> - the built list of responseDTOs.
	 */
	private List<TaskResponseDTO> buildListOfTaskResponseDtos(List<Task> tasks) {
		final List<TaskResponseDTO> responseDtoList = new ArrayList<>();

		for (final Task task : tasks) {
			responseDtoList.add(buildResponseDto(task));
		}

		return responseDtoList;
	}
	
	/**
	 * Builds a JobSiteResponseDTO from a JobSite.
	 *
	 * @param jobsite - the jobsite to build the responseDTO upon
	 * @return JobSiteResponseDTO - the built responseDTO.
	 */
	private JobSiteResponseDTO buildJobSiteResponseDto(final JobSite jobSite) {
		final JobSiteResponseDTO responseDto =
				this.mappingService.getModelMapper().map(jobSite, JobSiteResponseDTO.class);
		
		if (jobSite.getCreator() != null && jobSite.getCreator().getImage() != null) {
    		ImageResponseDTO image = this.mappingService.getModelMapper().map(jobSite.getCreator().getImage(), ImageResponseDTO.class);
    		responseDto.setCreatorImage(image);
			responseDto.getCreatorImage().setImageAsBaseString(
					this.imageService.encodeImage(jobSite.getCreator().getImage().getFileLocation()));
		}
		
		final Long countOpen = this.taskDao.countByStatusAndJobSiteIn(EnumStatus.OPEN, jobSite);
		final Long countInProgress = this.taskDao.countByStatusAndJobSiteIn(EnumStatus.IN_PROGRESS, jobSite);
		final Long countClosed = this.taskDao.countByStatusAndJobSiteIn(EnumStatus.CLOSED, jobSite);

		//add CountResult to JobSiteResponseDTO
		responseDto.setCountOpenTasks(countOpen);
		responseDto.setCountClosedTasks(countClosed);
		responseDto.setCountInProgressTasks(countInProgress);
		
		return responseDto;
	}

	/**
	 * Builds a list of jobSiteResponseDTOs from a list of JobSites.
	 *
	 * @param tasks - the list of jobSitess to build the list of responseDTOs upon
	 * @return List<JobSiteResponseDTO> - the built list of responseDTOs.
	 */
	private List<JobSiteResponseDTO> buildListOfJobSiteResponseDtos(List<JobSite> jobSites) {
		final List<JobSiteResponseDTO> responseDtoList = new ArrayList<>();

		for (final JobSite jobSite : jobSites) {
			responseDtoList.add(buildJobSiteResponseDto(jobSite));
		}

		return responseDtoList;
	}
}

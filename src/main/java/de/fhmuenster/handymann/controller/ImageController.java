package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.ImageDAO;
import de.fhmuenster.handymann.dao.JobSiteDAO;
import de.fhmuenster.handymann.dto.requests.ImageRequestDTO;
import de.fhmuenster.handymann.dto.responses.ImageResponseDTO;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Task;
import de.fhmuenster.handymann.enums.EnumEntityType;
import de.fhmuenster.handymann.enums.EnumImageUsageType;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/images")
@Api(value = "images", description = "endpoint for image management")
public class ImageController {

	@Autowired
	private ImageService imageservice;

	@Autowired
	private ImageDAO imageDao;

	@Autowired
	private JobSiteDAO jsDao;

	@Autowired
	private ModelMappingService mappingService;

	@Autowired
	private UserVerificationService userService;

	/**
	 * REST GET Method to find an Image by its id
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return ImageResponseDTO - a ImageResponseDTO object, if there was no error, else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/{imageId}")
	@ResponseStatus(HttpStatus.OK)
	public ImageResponseDTO getImageDetailsById(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("imageId") final Long imageId)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			// this.userService.getSession(request);
			final Optional<Image> image = this.imageDao.findById(imageId);

			if (!image.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildImageResponseDto(image.get());
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}

	}

	/**
	 * REST GET Method to find all images of JobSite by jobSiteid
	 *
	 * @param request the request object, created by the container - containing the session id
	 * @param response the response object, created by the container
	 * @return List<JobSiteResponseDTO> - a JobSiteResponseDTO object with the jobSite if there was
	 *         no error, else null
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@GetMapping(value = "/jobsite/{jobSiteId}")
	@ResponseStatus(HttpStatus.OK)
	public List<ImageResponseDTO> getImagesByJobSite(final HttpServletRequest request,
			final HttpServletResponse response, @PathVariable("jobSiteId") final Long jobSiteId)
			throws IOException {

		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			// this.userService.getSession(request);

			final Optional<JobSite> jobSite = this.jsDao.findById(jobSiteId);

			if (!jobSite.isPresent()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			}
			final JobSite jobsitefound = jobSite.get();
			final List<Image> imagejsf = jobsitefound.getImages();
			final List<Image> images = new ArrayList<>();

			for (int i = 0; i < imagejsf.size(); i++) {
				final Optional<Image> img = this.imageDao.findById(imagejsf.get(i).getId());
				if (img.isPresent()) {
					images.add(img.get());
				}

			}
			// final List<Image> images = this.imageDao.findById(imageids.get(i));

			if (images.isEmpty()) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return null;
			} else {
				return buildListOfImageResponseDtos(images);
			}
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}

	/**
	 * REST POST Method for image creation
	 *
	 * @param request the request object, created by the container
	 * @param response the response object, created by the container - containing the new session id
	 * @param taskRequestDto the DTO container with the task request
	 * @return a TaskResponseDTO object with the task
	 * @throws IOException if there is an error with the response.sendError method
	 */
	@PostMapping("/create")
	@ResponseBody
	public ImageResponseDTO createImage(final HttpServletRequest request,
			final HttpServletResponse response, @RequestBody final ImageRequestDTO imageRequestDto)
			throws IOException {
		try {
			// getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
			// this.userService.getSession(request);

			Image image = new Image();
			// set creationDate of image
			image.setCreationDate(imageRequestDto.getCreationDate());

			// set Name of image
			if (imageRequestDto.getName() != null) {
				final String name = imageRequestDto.getName();
				image.setName(name);
			} else {
				image.setName("replacewithuniquename");
			}


			// set FileLocation of image
			image.setFileLocation(putfiletoressources(image, imageRequestDto));

			image = this.imageDao.save(image);
			
			final Optional<JobSite> optionalJobsite = this.jsDao.findById(imageRequestDto.getEntityId());

			if (optionalJobsite.isPresent()) {
				JobSite jobsite = optionalJobsite.get();
				
				jobsite.addImage(image);
				
				jobsite = this.jsDao.save(jobsite);
			}
			
			return buildImageResponseDto(image);
		} catch (final IllegalArgumentException ex) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
			return null;
		}
	}



	private String putfiletoressources(final Image image, final ImageRequestDTO imageRequestDto) {
		// TODO put Image to Ressources implementieren
		// this.imageservice.getImageFileLocation(image.getName, EnumImageMimeType)
		final String baseString = imageRequestDto.getImageAsBaseString();
		final EnumEntityType checkentity = imageRequestDto.getEntityType();
		EnumImageUsageType usage = null;
		String pathFile = null;
		switch (checkentity.getDisplayName()) {
			case "Baustelle":
				usage = EnumImageUsageType.OTHER;
			case "Material":
				usage = EnumImageUsageType.OTHER;
			case "Werkzeug":
				usage = EnumImageUsageType.OTHER;
			case "Fahrzeug":
				usage = EnumImageUsageType.OTHER;
			case "Maschine":
				usage = EnumImageUsageType.OTHER;
		}
		if (usage == null) {
			usage = EnumImageUsageType.AVATAR;
		}
		pathFile = this.imageservice.decodeImage(baseString, usage);

		return pathFile;
	}


	/**
	 * Builds a ImageResponseDTO from a Image.
	 *
	 * @param image - the image to build the responseDTO upon
	 * @return imageResponseDTO - the built responseDTO.
	 */
	private ImageResponseDTO buildImageResponseDto(final Image image) {
		final ImageResponseDTO responseDto =
				this.mappingService.getModelMapper().map(image, ImageResponseDTO.class);
		responseDto.setImageAsBaseString(this.imageservice.encodeImage(image.getFileLocation()));

		return responseDto;
	}

	/**
	 * Builds a list of ImageResponseDTOs from a list of Images.
	 *
	 * @param images - the list of tasks to build the list of responseDTOs upon
	 * @return List<ImageResponseDTO> - the built list of responseDTOs.
	 */
	private List<ImageResponseDTO> buildListOfImageResponseDtos(final List<Image> images) {
		final List<ImageResponseDTO> responseDtoList = new ArrayList<>();

		for (final Image image : images) {
			responseDtoList.add(buildImageResponseDto(image));
		}

		return responseDtoList;

	}
}

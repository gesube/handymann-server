package de.fhmuenster.handymann.controller;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import de.fhmuenster.handymann.dao.JobSiteDAO;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dao.UserDAO;
import de.fhmuenster.handymann.dao.VehicleDAO;
import de.fhmuenster.handymann.dto.requests.TeamRequestDTO;
import de.fhmuenster.handymann.dto.responses.ProfileResponseDTO;
import de.fhmuenster.handymann.dto.responses.TeamResponseDTO;
import de.fhmuenster.handymann.dto.responses.VehicleResponseDTO;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Session;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.Vehicle;
import de.fhmuenster.handymann.enums.EnumEmployeeStatus;
import de.fhmuenster.handymann.services.ImageService;
import de.fhmuenster.handymann.services.ModelMappingService;
import de.fhmuenster.handymann.services.UserVerificationService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/teams")
@Api(value = "teams", description = "endpoint for team management")
public class TeamController {

    @Autowired
    private UserVerificationService userService;
    @Autowired
    private TeamDAO teamDao;
    @Autowired
    private UserDAO userDao;
    @Autowired
    private JobSiteDAO jobSiteDao;
    @Autowired
    private VehicleDAO vehicleDao;
    @Autowired
    private ModelMappingService mappingService;

    @Autowired
    private ImageService imageService;

    /**
     * REST GET Method to get all team members
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param teamId the id of the team
     * @return List<ProfileResponseDTO> - a list of ProfileResponseDTO objects with the users if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/members/{teamId}")
    @ResponseStatus(HttpStatus.OK)
    public List<ProfileResponseDTO> getMembersOfTeam(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("teamId") final Long teamId)
            throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Team> team = this.teamDao.findById(teamId);

            if (team.isPresent() && team.get().getMembers() != null
                    && !team.get().getMembers().isEmpty() && team.get().isCanBeChanged()) {
                final List<User> members = team.get().getMembers();
                return this.buildListOfProfileResponseDtos(members);
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to get all team members
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param teamId the id of the team
     * @return List<ProfileResponseDTO> - a list of ProfileResponseDTO objects with the users if
     *         there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/non/members/")
    @ResponseStatus(HttpStatus.OK)
    public List<ProfileResponseDTO> getNonMembersOfTeam(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);

            final Date nowStartDate = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin"))
                    .withHour(0).withMinute(0).withSecond(0).minusHours(1).toInstant());

            final Date nowEndDate = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin"))
                    .withHour(23).withMinute(59).withSecond(59).minusHours(1).toInstant());

            final List<Team> teams =
                    this.teamDao.findByValidForDateBetween(nowStartDate, nowEndDate);
            final List<User> users = this.userDao.findAll();

            final List<User> nonMembers = new ArrayList<>();

            for (final User user : users) {
            	if (user.getEmployee().getEmployeeStatus() != EnumEmployeeStatus.MEISTER) {
            		boolean userIsInTeam = false;
                    for (final Team team : teams) {
                        for (final User member : team.getMembers()) {
                            if (member.getId() == user.getId()) {
                                userIsInTeam = true;
                                break;
                            }
                        }
                    }

                    if (!userIsInTeam) {
                        nonMembers.add(user);
                    }
            	}              
            }

            return this.buildListOfProfileResponseDtos(nonMembers);
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to get Team by ID
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @param teamId the id of the team
     * @return teamResponseDTO - a TeamResponseDTO with the team if there was no error, else null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/{teamId}")
    @ResponseStatus(HttpStatus.OK)
    public TeamResponseDTO getTeamDetailsById(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("teamId") final Long teamId)
            throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);
            final Optional<Team> team = this.teamDao.findById(teamId);

            if (!team.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildTeamResponseDto(team.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * REST GET Method to the current team
     *
     * @param request the request object, created by the container - containing the session id
     * @param response the response object, created by the container
     * @return TeamResponseDTO - a TeamResponseDTO objects with the team if there was no error, else
     *         null
     * @throws IOException if there is an error with the response.sendError method
     */
    @GetMapping(value = "/current/overview")
    @ResponseStatus(HttpStatus.OK)
    public TeamResponseDTO getCurrentTeamOfUser(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());

            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());

            if (!team.isPresent() && session.getUser() != null
                    && session.getUser().getEmployee() != null && session.getUser().getEmployee()
                            .getEmployeeStatus() == EnumEmployeeStatus.MEISTER) {
                final Team createdTeam = this.createTeam(session);
                return this.buildTeamResponseDto(createdTeam);
            } else if (!team.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            } else {
                return this.buildTeamResponseDto(team.get());
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        } catch (final Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    private Team createTeam(final Session session) {
        Team team = new Team();

        final Date date = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).toInstant());
        team.setValidForDate(date);
        team.setCanBeChanged(true);
        team.addMember(session.getUser());

        team = this.teamDao.save(team);

        return team;
    }

    /**
     * REST PUT Method to set the vehicle of the current team
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @return a TeamResponseDTO object with the team
     * @throws IOException if there is an error with the response.sendError method
     */
    @PutMapping("/update/vehicle/{vehicleId}")
    @ResponseBody
    public TeamResponseDTO updateCurrentTeamVehicle(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("vehicleId") final Long vehicleId)
            throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());

            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());

            if (team.isPresent()) {
                final Optional<Vehicle> vehicle = this.vehicleDao.findById(vehicleId);

                if (vehicle.isPresent()) {
                    vehicle.get().setCurrentPossessor(team.get());

                    this.vehicleDao.save(vehicle.get());
                    return this.buildTeamResponseDto(team.get());
                }

            }
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        } catch (final Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    /**
     * REST PUT Method to set the jobsite of the current team
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @return a TeamResponseDTO object with the team
     * @throws IOException if there is an error with the response.sendError method
     */
    @PutMapping("/update/jobsite/{jobSiteId}")
    @ResponseBody
    public TeamResponseDTO updateCurrentTeamJobSite(final HttpServletRequest request,
            final HttpServletResponse response, @PathVariable("jobSiteId") final Long jobSiteId)
            throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());

            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());

            if (team.isPresent()) {
                final Optional<JobSite> jobsite = this.jobSiteDao.findById(jobSiteId);

                if (jobsite.isPresent()) {
                    team.get().setCurrentJobSite(jobsite.get());
                    final Team teamResponse = this.teamDao.save(team.get());
                    return this.buildTeamResponseDto(teamResponse);
                }

            }
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        } catch (final Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    /**
     * REST PUT Method to remove the jobsite of the current team
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @return a TeamResponseDTO object with the team
     * @throws IOException if there is an error with the response.sendError method
     */
    @PutMapping("/remove/jobsite/")
    @ResponseBody
    public TeamResponseDTO removeCurrentTeamJobSite(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());

            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());

            if (team.isPresent()) {
                team.get().setCurrentJobSite(null);
                final Team teamResponse = this.teamDao.save(team.get());
                return this.buildTeamResponseDto(teamResponse);
            }
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        } catch (final Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    /**
     * REST PUT Method to remove the vehicle of the current team
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @return a TeamResponseDTO object with the team
     * @throws IOException if there is an error with the response.sendError method
     */
    @PutMapping("/remove/vehicle/")
    @ResponseBody
    public TeamResponseDTO removeCurrentTeamVehicle(final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            final Session session = this.userService.getSession(request);

            final Date today = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                    .withMinute(0).withSecond(0).withNano(0).toInstant());

            final Optional<Team> team =
                    this.teamDao.findByValidForDateAndMembersIn(today, session.getUser());

            if (team.isPresent()) {
                final Optional<Vehicle> vehicle =
                        this.vehicleDao.findByCurrentPossessor(team.get());
                if (vehicle.isPresent()) {
                    vehicle.get().setCurrentPossessor(null);
                    this.vehicleDao.save(vehicle.get());
                    return this.buildTeamResponseDto(team.get());
                }
            }
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        } catch (final Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    /**
     * REST PUT Method for team update
     *
     * @param request the request object, created by the container
     * @param response the response object, created by the container - containing the new session id
     * @param teamRequestDto the DTO container with the team request
     * @return a TeamResponseDTO object with the team
     * @throws IOException if there is an error with the response.sendError method
     */
    @PutMapping("/update/{teamId}")
    @ResponseBody
    public TeamResponseDTO updateTeam(final HttpServletRequest request,
            final HttpServletResponse response, @RequestBody final TeamRequestDTO teamRequestDto,
            @PathVariable("teamId") final Long teamId) throws IOException {
        try {
            // getSession() wirft von sich aus eine Exception, wenn die User-Session invalide ist
            this.userService.getSession(request);

            final Optional<Team> optionalTeam = this.teamDao.findById(teamId);

            if (optionalTeam.isPresent()) {
                Team team = optionalTeam.get();

                if (!team.isCanBeChanged()) {
                    response.sendError(HttpServletResponse.SC_FORBIDDEN);
                    return null;
                }

                team.setCanBeChanged(teamRequestDto.isCanBeChanged());

                team.getMembers().clear();
                if (teamRequestDto.getMemberIds() != null) {
                    for (final long memberId : teamRequestDto.getMemberIds()) {
                        final Optional<User> member = this.userDao.findById(memberId);
                        if (member.isPresent() && !team.getMembers().contains(member.get())) {
                            team.addMember(member.get());
                        }
                    }
                }

                team = this.teamDao.save(team);

                if (team != null) {
                    return this.buildTeamResponseDto(team);
                } else {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    return null;
                }
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return null;
            }
        } catch (final IllegalArgumentException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
            return null;
        }
    }

    /**
     * Builds a ProfileResponseDTO from an User.
     *
     * @param user - the user to build the responseDTO upon
     * @return ProfileResponseDTO - the built responseDTO.
     */
    private ProfileResponseDTO buildProfileResponseDto(final User user) {
        final ProfileResponseDTO responseDto =
                this.mappingService.getModelMapper().map(user, ProfileResponseDTO.class);

        responseDto.getImage().setImageAsBaseString(
                this.imageService.encodeImage(user.getImage().getFileLocation()));

        return responseDto;
    }

    /**
     * Builds a list of ProfileResponseDTOs from a list of Users.
     *
     * @param users - the list of users to build the list of responseDTOs upon
     * @return List<ProfileResponseDTO> - the built list of responseDTOs.
     */
    private List<ProfileResponseDTO> buildListOfProfileResponseDtos(final List<User> users) {
        final List<ProfileResponseDTO> responseDtoList = new ArrayList<>();

        for (final User user : users) {
            responseDtoList.add(this.buildProfileResponseDto(user));
        }

        return responseDtoList;
    }

    /**
     * Builds a TeamResponseDTO from a Team.
     *
     * @param team - the team to build the responseDTO upon
     * @return TeamResponseDTO - the built responseDTO.
     */
    private TeamResponseDTO buildTeamResponseDto(final Team team) {
        final TeamResponseDTO responseDto =
                this.mappingService.getModelMapper().map(team, TeamResponseDTO.class);

        final Optional<Vehicle> vehicle = this.vehicleDao.findByCurrentPossessor(team);

        if (vehicle.isPresent()) {
            final VehicleResponseDTO vehicleDto = this.mappingService.getModelMapper()
                    .map(vehicle.get(), VehicleResponseDTO.class);
            responseDto.setCurrentVehicle(vehicleDto);
        }


        return responseDto;
    }
}

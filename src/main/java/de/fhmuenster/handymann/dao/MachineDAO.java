package de.fhmuenster.handymann.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Machine;
import de.fhmuenster.handymann.entities.Team;

/**
 * DAO interface for the machine related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface MachineDAO extends CrudRepository<Machine, Long> {

    @Override
    public List<Machine> findAll();

    public List<Machine> findByCurrentPossessor(Team currentPossessor);

}

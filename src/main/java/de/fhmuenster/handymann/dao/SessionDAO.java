package de.fhmuenster.handymann.dao;

import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Session;

/**
 * DAO Interface for the session related database interaction
 * 
 * @author Alexander Lüdiger-Schlüter
 *
 */
public interface SessionDAO extends CrudRepository<Session, Long> {

    public Session findBySessionId(String sessionId);
}

package de.fhmuenster.handymann.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.Team;

/**
 * DAO interface for the job related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface JobDAO extends CrudRepository<Job, Long> {

    @Override
    public List<Job> findAll();

    public Optional<Job> findByTeam(Team team);

    public List<Job> findByTeamAndValidForDateBetween(Team team, Date startDate, Date endDate);

    public List<Job> findByTeamOrderByValidForDateDesc(Team team);
}

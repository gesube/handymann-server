package de.fhmuenster.handymann.dao;

import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Colour;

/**
 * DAO interface for the colour related database interaction
 * 
 * @author Ruben van Lück
 *
 */
public interface ColourDAO extends CrudRepository<Colour, Long> {

}

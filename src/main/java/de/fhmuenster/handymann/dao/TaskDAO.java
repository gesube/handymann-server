package de.fhmuenster.handymann.dao;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Task;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.enums.EnumStatus;

/**
 * DAO interface for the task related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface TaskDAO extends CrudRepository<Task, Long> {

    @Override
    public List<Task> findAll();

    public List<Task> findByJobSite(JobSite jobsite);

    public List<Task> findByJob(Job job);

    @Query("SELECT t FROM Task t WHERE  t.status=:status")
    List<Task> fetchTasksStatus(@Param("status") EnumStatus status);

    @Query("SELECT t FROM Task t WHERE  t.job=:job")
    List<Task> fetchTasks(@Param("job") Long jobid);

    List<Task> findByStatusAndFinishDateBetweenAndAssigneeIn(@Param("status") EnumStatus status,
            @Param("finishDate") Date finishDateStart, @Param("finishDate") Date finishDateEnd,
            @Param("assignee") List<User> assignees);

    List<Task> findByStatusAndJobSiteAndAvailableForTheseOccupationsIn(
            @Param("status") EnumStatus status, @Param("jobsite") JobSite jobsite,
            @Param("occupation") Occupation occupation);

    Long countByStatusAndJobSiteIn(@Param("status") EnumStatus status,
            @Param("jobsite") JobSite jobsite);

}

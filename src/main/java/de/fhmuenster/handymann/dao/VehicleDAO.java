package de.fhmuenster.handymann.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.Vehicle;

/**
 * DAO interface for the vehicle related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface VehicleDAO extends CrudRepository<Vehicle, Long> {

    @Override
    public List<Vehicle> findAll();
    
    public List<Vehicle> findByCurrentPossessorIsNull();
    
    public Optional<Vehicle> findByCurrentPossessor(Team team);
}

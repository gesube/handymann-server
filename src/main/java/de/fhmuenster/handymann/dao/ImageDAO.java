package de.fhmuenster.handymann.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import de.fhmuenster.handymann.entities.Image;

/**
 * DAO interface for the image related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface ImageDAO extends CrudRepository<Image, Long> {
	
	public Optional<Image> findById(Long id);

}

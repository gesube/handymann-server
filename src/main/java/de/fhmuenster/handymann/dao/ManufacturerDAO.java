package de.fhmuenster.handymann.dao;

import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Manufacturer;

/**
 * DAO interface for the manufacturer related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface ManufacturerDAO extends CrudRepository<Manufacturer, Long> {

}

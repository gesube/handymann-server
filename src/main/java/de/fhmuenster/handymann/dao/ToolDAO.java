package de.fhmuenster.handymann.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Tool;

/**
 * DAO interface for the tool related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface ToolDAO extends CrudRepository<Tool, Long> {

    @Override
    public List<Tool> findAll();

    public List<Tool> findByAvailableForTheseOccupationsIn(Occupation occupation);

}

package de.fhmuenster.handymann.dao;

import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Occupation;

/**
 * DAO interface for the occupation related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface OccupationDAO extends CrudRepository<Occupation, Long> {

}

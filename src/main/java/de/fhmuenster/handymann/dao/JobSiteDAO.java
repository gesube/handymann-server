package de.fhmuenster.handymann.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.JobSite;

/**
 * DAO interface for the job site related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface JobSiteDAO extends CrudRepository<JobSite, Long> {

    @Override
    public List<JobSite> findAll();

}

package de.fhmuenster.handymann.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.Material;

/**
 * DAO interface for the material related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface MaterialDAO extends CrudRepository<Material, Long> {

    @Override
    public List<Material> findAll();
}

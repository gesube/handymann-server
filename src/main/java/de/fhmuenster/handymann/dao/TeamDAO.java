package de.fhmuenster.handymann.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;

/**
 * DAO interface for the team related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface TeamDAO extends CrudRepository<Team, Long> {

    public List<Team> findByMembersIn(User member);

    @Override
    public List<Team> findAll();

    public List<Team> findByValidForDateBetween(Date startDate, Date endDate);

    public Optional<Team> findByValidForDateAndMembersIn(@Param("validForDate") Date validForDate,
            @Param("member") User member);

    public List<Team> findByValidForDateBetweenAndMembersIn(Date startDate, Date endDate,
            User member);

    public List<Team> findByMembersInOrderByValidForDateDesc(User member);
}

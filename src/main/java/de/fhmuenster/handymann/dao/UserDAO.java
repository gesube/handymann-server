package de.fhmuenster.handymann.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import de.fhmuenster.handymann.entities.User;

/**
 * DAO interface for the user related database interaction
 *
 * @author Ruben van Lück
 *
 */
public interface UserDAO extends CrudRepository<User, Long> {

    public User findByName(String name);

    @Override
    public List<User> findAll();
}

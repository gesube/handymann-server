package de.fhmuenster.handymann.enums;

import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration with every possible status for a {@link de.fhmuenster.handymann.entities.Task}.
 *
 * @author Ruben van Lück
 */
public enum EnumStatus {

    OPEN("noch offen"), IN_PROGRESS("in Bearbeitung"), CLOSED("geschlossen"),

    ;
    private final String displayName;

    private EnumStatus(final String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return this.displayName;
    }

    @JsonCreator
    public static EnumStatus fromValue(final String value) {
        for (final EnumStatus category : values()) {
            if (category.displayName.equalsIgnoreCase(value)) {
                return category;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }

}

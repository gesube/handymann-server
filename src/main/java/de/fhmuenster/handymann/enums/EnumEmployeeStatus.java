package de.fhmuenster.handymann.enums;

import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration with every possible employee status for a
 * {@link de.fhmuenster.handymann.entities.embeddables.Employee}.
 *
 * @author Ruben van Lück
 *
 */
public enum EnumEmployeeStatus {

    UNTERNEHMENSLEITUNG("Unternehmensleitung"), VERWALTUNG("Verwaltung"), MEISTER(
            "Meister"), GESELLE("Geselle"), AUSZUBILDENDER("Auszubildender");

    private final String displayName;

    private EnumEmployeeStatus(final String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return this.displayName;
    }

    @JsonCreator
    public static EnumEmployeeStatus fromValue(final String value) {
        for (final EnumEmployeeStatus role : values()) {
            if (role.displayName.equalsIgnoreCase(value)) {
                return role;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}

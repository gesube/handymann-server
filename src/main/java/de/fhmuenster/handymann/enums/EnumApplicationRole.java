package de.fhmuenster.handymann.enums;

import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration with every possible application role for a
 * {@link de.fhmuenster.handymann.entities.User}.
 *
 * @author Ruben van Lück
 *
 */
public enum EnumApplicationRole {

    ADMINISTRATOR("Administrator"), USER("User");

    private final String displayName;

    private EnumApplicationRole(final String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return this.displayName;
    }

    @JsonCreator
    public static EnumApplicationRole fromValue(final String value) {
        for (final EnumApplicationRole role : values()) {
            if (role.displayName.equalsIgnoreCase(value)) {
                return role;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}

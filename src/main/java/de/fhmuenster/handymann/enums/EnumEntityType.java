package de.fhmuenster.handymann.enums;

import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration with every possible entity type for a
 * {@link de.fhmuenster.handymann.dto.requests.ImageRequestDTO}.
 *
 * @author Ruben van Lück
 *
 */
public enum EnumEntityType {

    JOBSITE("Baustelle"), MATERIAL("Material"), TOOL("Werkzeug"), VEHICLE("Fahrzeug"), MACHINE(
            "Maschine");

    private final String displayName;

    private EnumEntityType(final String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return this.displayName;
    }

    @JsonCreator
    public static EnumEntityType fromValue(final String value) {
        for (final EnumEntityType type : values()) {
            if (type.displayName.equalsIgnoreCase(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}

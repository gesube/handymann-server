package de.fhmuenster.handymann.enums;

import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration with every possible priority for a {@link de.fhmuenster.handymann.entities.Task}.
 * 
 * @author Ruben van Lück
 */
public enum EnumPriority {

    LOW("niedrig"), NORMAL("mittel"), HIGH("hoch")

    ;
    private final String displayName;

    private EnumPriority(final String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return this.displayName;
    }

    @JsonCreator
    public static EnumPriority fromValue(final String value) {
        for (final EnumPriority category : values()) {
            if (category.displayName.equalsIgnoreCase(value)) {
                return category;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}

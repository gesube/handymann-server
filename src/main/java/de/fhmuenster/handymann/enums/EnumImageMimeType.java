package de.fhmuenster.handymann.enums;

/**
 * Enumeration with every allowed value for the accepted and to be processed image mimetypes.
 * 
 * @author Ruben van Lück
 */
public enum EnumImageMimeType {
    JPG("image/jpeg"), PNG("image/png"),

    // if you feel like there's a value missing, just add it to this list


    ;
    private final String mimeType;

    /**
     * @param value - String to set as the mimeType / the description of the enum value
     * @author Ruben van Lück
     */
    private EnumImageMimeType(final String value) {
        this.mimeType = value;
    }

    /**
     * @return String - the value of the description of the enum value
     * @author Ruben van Lück
     */
    public String getMimeType() {
        return this.mimeType;
    }
}

package de.fhmuenster.handymann.enums;

/**
 * Enumeration with every allowed usage type for images.
 *
 * @author Ruben van Lück
 */
public enum EnumImageUsageType {
    AVATAR(512, 512), OTHER(1920, 1080)

    // if you feel like there's a value missing, just add it to this list


    ;
    private final int height;
    private final int width;

    /**
     * @param height - max. height of an image
     * @param width - max. width of an image
     * @author Ruben van Lück
     */
    private EnumImageUsageType(final int height, final int width) {
        this.height = height;
        this.width = width;
    }

    /**
     * @return int - the max. height of an image
     * @author Ruben van Lück
     */
    public int getHeight() {
        return this.height;
    }

    /**
     * @return int - the max. width of an image
     * @author Ruben van Lück
     */
    public int getWidth() {
        return this.width;
    }
}

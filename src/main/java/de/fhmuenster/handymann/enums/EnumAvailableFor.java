package de.fhmuenster.handymann.enums;

import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumeration with every possible option for a
 * {@link de.fhmuenster.handymann.entities.Manufacturer}.
 *
 * @author Ruben van Lück
 *
 */
public enum EnumAvailableFor {

    VEHICLES(" für Fahrzeuge"), TOOLS(" für Werkzeuge"), MASCHINES(" für Maschinen");

    private final String displayName;

    private EnumAvailableFor(final String displayName) {
        this.displayName = displayName;
    }

    @JsonValue
    public String getDisplayName() {
        return this.displayName;
    }

    @JsonCreator
    public static EnumAvailableFor fromValue(final String value) {
        for (final EnumAvailableFor validFor : values()) {
            if (validFor.displayName.equalsIgnoreCase(value)) {
                return validFor;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}

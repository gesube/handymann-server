package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Colour;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.Vehicle;

@Service
public class VehicleBuilder {

    public List<Vehicle> buildEntityListToPersist(final List<Team> teams,
            final List<Manufacturer> manufacturers, final List<Colour> colours,
            final List<Image> images) {
        final List<Vehicle> returnList = new ArrayList<>();

        final Vehicle v1 = new Vehicle();
        v1.setName("Transporter");
        v1.setModel("Höhe: 2,4m");
        v1.setManufacturer(manufacturers.get(0));
        v1.setNumberPlate("MS-HM 2510");
        v1.setColour(colours.get(9));
        v1.setCurrentPossessor(null);
        v1.setImage(images.get(9));

        final Vehicle v2 = new Vehicle();
        v2.setName("Vivaro");
        v2.setModel("Höhe: 2,1m");
        v2.setManufacturer(manufacturers.get(1));
        v2.setNumberPlate("MS-HM 2511");
        v2.setColour(colours.get(9));
        v2.setCurrentPossessor(teams.get(11));
        v2.setImage(images.get(10));

        final Vehicle v3 = new Vehicle();
        v3.setName("Vivaro");
        v3.setModel("Höhe: 2,1m");
        v3.setManufacturer(manufacturers.get(1));
        v3.setNumberPlate("MS-HM 2512");
        v3.setColour(colours.get(9));
        v3.setCurrentPossessor(teams.get(16));
        v3.setImage(images.get(10));

        final Vehicle v4 = new Vehicle();
        v4.setName("Transporter");
        v4.setModel("Höhe: 2,30m");
        v4.setManufacturer(manufacturers.get(5));
        v4.setNumberPlate("MS-HM 2513");
        v4.setColour(colours.get(2));
        v4.setCurrentPossessor(null);
        v4.setImage(images.get(25));

        final Vehicle v5 = new Vehicle();
        v5.setName("Touareg");
        v5.setModel("Höhe: 1,8m");
        v5.setManufacturer(manufacturers.get(5));
        v5.setNumberPlate("MS-HM 2514");
        v5.setColour(colours.get(2));
        v5.setCurrentPossessor(null);
        v5.setImage(images.get(24));

        returnList.add(v1);
        returnList.add(v2);
        returnList.add(v3);
        returnList.add(v4);
        returnList.add(v5);
        return returnList;
    }
}

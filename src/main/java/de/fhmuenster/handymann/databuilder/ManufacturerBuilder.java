package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.enums.EnumAvailableFor;

@Service
public class ManufacturerBuilder {

    public List<Manufacturer> buildEntityListToPersist() {
        final List<Manufacturer> returnList = new ArrayList<>();

        final Manufacturer mf1 = new Manufacturer();
        mf1.setAvailableFor(EnumAvailableFor.VEHICLES);
        mf1.setName("Mercedes");

        final Manufacturer mf2 = new Manufacturer();
        mf2.setAvailableFor(EnumAvailableFor.VEHICLES);
        mf2.setName("Opel");

        final Manufacturer mf3 = new Manufacturer();
        mf3.setAvailableFor(EnumAvailableFor.TOOLS);
        mf3.setName("Makita");

        final Manufacturer mf4 = new Manufacturer();
        mf4.setAvailableFor(EnumAvailableFor.TOOLS);
        mf4.setName("BOSCH");

        final Manufacturer mf5 = new Manufacturer();
        mf5.setAvailableFor(EnumAvailableFor.VEHICLES);
        mf5.setName("VW");

        final Manufacturer mf6 = new Manufacturer();
        mf6.setAvailableFor(EnumAvailableFor.VEHICLES);
        mf6.setName("Mercedes");

        final Manufacturer mf7 = new Manufacturer();
        mf7.setAvailableFor(EnumAvailableFor.TOOLS);
        mf7.setName("HILTI");

        final Manufacturer mf8 = new Manufacturer();
        mf8.setAvailableFor(EnumAvailableFor.TOOLS);
        mf8.setName("Stabila");

        final Manufacturer mf9 = new Manufacturer();
        mf9.setAvailableFor(EnumAvailableFor.VEHICLES);
        mf9.setName("Mercedes");

        final Manufacturer mf10 = new Manufacturer();
        mf10.setAvailableFor(EnumAvailableFor.TOOLS);
        mf10.setName("Metabo");

        final Manufacturer mf11 = new Manufacturer();
        mf11.setAvailableFor(EnumAvailableFor.MASCHINES);
        mf11.setName("Metabo");

        final Manufacturer mf12 = new Manufacturer();
        mf12.setAvailableFor(EnumAvailableFor.MASCHINES);
        mf12.setName("STIHL");

        final Manufacturer mf13 = new Manufacturer();
        mf13.setAvailableFor(EnumAvailableFor.MASCHINES);
        mf13.setName("BOSCH");

        final Manufacturer mf14 = new Manufacturer();
        mf14.setAvailableFor(EnumAvailableFor.MASCHINES);
        mf14.setName("HILTI");

        final Manufacturer mf15 = new Manufacturer();
        mf15.setAvailableFor(EnumAvailableFor.MASCHINES);
        mf15.setName("CAT");

        returnList.add(mf1);
        returnList.add(mf2);
        returnList.add(mf3);
        returnList.add(mf4);
        returnList.add(mf5);
        returnList.add(mf6);
        returnList.add(mf7);
        returnList.add(mf8);
        returnList.add(mf9);
        returnList.add(mf10);
        returnList.add(mf11);
        returnList.add(mf12);
        returnList.add(mf13);
        returnList.add(mf14);
        returnList.add(mf15);



        return returnList;
    }
}

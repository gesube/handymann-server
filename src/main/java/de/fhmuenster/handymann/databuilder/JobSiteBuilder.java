package de.fhmuenster.handymann.databuilder;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.embeddables.Address;
import de.fhmuenster.handymann.entities.embeddables.StartAndEndDates;

@Service
public class JobSiteBuilder {

    public List<JobSite> buildEntityListToPersist(final List<User> users,
            final List<Image> images) {
        final List<JobSite> returnList = new ArrayList<>();

        final JobSite js1 = new JobSite();
        js1.setCreator(users.get(0));
        js1.setDescription("Fachhochschulzentrum");
        js1.setAddress(new Address("Corrensstraße 25", "48149", "Münster", "Deutschland"));
        js1.setStartAndEndDates(
                new StartAndEndDates(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).minusDays(10),
                        ZonedDateTime.now(ZoneId.of("Europe/Berlin")).plusDays(30),
                        ZonedDateTime.now(ZoneId.of("Europe/Berlin")).minusDays(10), null));
        js1.addImage(images.get(3));
        js1.addImage(images.get(4));
        js1.addImage(images.get(5));
        js1.addImage(images.get(6));
        js1.addImage(images.get(7));


        final JobSite js2 = new JobSite();
        js2.setCreator(users.get(1));
        js2.setDescription("Knallerkneipe");
        js2.setAddress(new Address("Hansaring 9", "48149", "Münster", "Deutschland"));
        js2.setStartAndEndDates(
                new StartAndEndDates(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).minusDays(5),
                        ZonedDateTime.now(ZoneId.of("Europe/Berlin")).plusDays(40),
                        ZonedDateTime.now(ZoneId.of("Europe/Berlin")).minusDays(3), null));

        returnList.add(js1);
        returnList.add(js2);

        return returnList;
    }
}

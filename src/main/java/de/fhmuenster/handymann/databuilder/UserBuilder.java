package de.fhmuenster.handymann.databuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.embeddables.Employee;
import de.fhmuenster.handymann.enums.EnumApplicationRole;
import de.fhmuenster.handymann.enums.EnumEmployeeStatus;
import de.fhmuenster.handymann.enums.EnumImageMimeType;
import de.fhmuenster.handymann.services.ImageService;

@Service
public class UserBuilder {

    @Autowired
    private ImageService imageService;

    public List<User> buildEntityListToPersist(final List<Occupation> occupations,
            final List<Image> images) {
        final List<User> returnList = new ArrayList<>();

        final User user1 = new User();
        user1.setName("boss");
        user1.setPassword("boss");
        user1.setEmployee(new Employee(new Long(1001), "1234567890", "Felix", "Blume",
                new BigDecimal(36), EnumEmployeeStatus.UNTERNEHMENSLEITUNG, occupations.get(0)));
        user1.setAppRole(EnumApplicationRole.ADMINISTRATOR);
        user1.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user2 = new User();
        user2.setName("dj");
        user2.setPassword("dj");
        user2.setEmployee(new Employee(new Long(1002), "1234567890", "DJ", "Bobo",
                new BigDecimal(38.5), EnumEmployeeStatus.MEISTER, occupations.get(1)));
        user2.setAppRole(EnumApplicationRole.USER);
        user2.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user3 = new User();
        user3.setName("andy");
        user3.setPassword("andy");
        user3.setEmployee(new Employee(new Long(1003), "1234567890", "Andreas", "Elsholz",
                new BigDecimal(38.5), EnumEmployeeStatus.GESELLE, occupations.get(2)));
        user3.setAppRole(EnumApplicationRole.USER);
        user3.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user4 = new User();
        user4.setName("pietro");
        user4.setPassword("pietro");
        user4.setEmployee(new Employee(new Long(1004), "1234567890", "Pietro", "Lombardi",
                new BigDecimal(38.5), EnumEmployeeStatus.GESELLE, occupations.get(3)));
        user4.setAppRole(EnumApplicationRole.USER);
        user4.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user5 = new User();
        user5.setName("lucy");
        user5.setPassword("lucy");
        user5.setEmployee(new Employee(new Long(1005), "1234567890", "Luci", "Lectric",
                new BigDecimal(38.5), EnumEmployeeStatus.AUSZUBILDENDER, occupations.get(4)));
        user5.setAppRole(EnumApplicationRole.USER);
        user5.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar1", EnumImageMimeType.PNG)));

        returnList.add(user1);
        returnList.add(user2);
        returnList.add(user3);
        returnList.add(user4);
        returnList.add(user5);

        final User user22 = new User();
        user22.setName("kevin");
        user22.setPassword("kevin");
        user22.setEmployee(new Employee(new Long(2001), "1234567890", "Macaulay", "Culkin",
                new BigDecimal(38.5), EnumEmployeeStatus.MEISTER, occupations.get(5)));
        user22.setAppRole(EnumApplicationRole.USER);
        user22.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user23 = new User();
        user23.setName("oliver");
        user23.setPassword("oliver");
        user23.setEmployee(new Employee(new Long(2002), "1234567890", "Oliver", "Petsokat",
                new BigDecimal(38.5), EnumEmployeeStatus.GESELLE, occupations.get(6)));
        user23.setAppRole(EnumApplicationRole.USER);
        user23.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user24 = new User();
        user24.setName("yvonne");
        user24.setPassword("yvonne");
        user24.setEmployee(new Employee(new Long(2003), "1234567890", "Yvonne", "Katterfeld",
                new BigDecimal(38.5), EnumEmployeeStatus.GESELLE, occupations.get(7)));
        user24.setAppRole(EnumApplicationRole.USER);
        user24.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar1", EnumImageMimeType.PNG)));

        final User user25 = new User();
        user25.setName("janne");
        user25.setPassword("janne");
        user25.setEmployee(new Employee(new Long(2004), "1234567890", "Jeanett", "Biedermann",
                new BigDecimal(38.5), EnumEmployeeStatus.AUSZUBILDENDER, occupations.get(8)));
        user25.setAppRole(EnumApplicationRole.USER);
        user25.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar1", EnumImageMimeType.PNG)));

        returnList.add(user22);
        returnList.add(user23);
        returnList.add(user24);
        returnList.add(user25);

        final User user32 = new User();
        user32.setName("david");
        user32.setPassword("david");
        user32.setEmployee(new Employee(new Long(3001), "1234567890", "David", "Hasselhoff",
                new BigDecimal(38.5), EnumEmployeeStatus.MEISTER, occupations.get(2)));
        user32.setAppRole(EnumApplicationRole.USER);
        user32.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user33 = new User();
        user33.setName("wolle");
        user33.setPassword("wolle");
        user33.setEmployee(new Employee(new Long(3002), "1234567890", "Wolfgang", "Petry",
                new BigDecimal(38.5), EnumEmployeeStatus.GESELLE, occupations.get(8)));
        user33.setAppRole(EnumApplicationRole.USER);
        user33.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user34 = new User();
        user34.setName("peter");
        user34.setPassword("peter");
        user34.setEmployee(new Employee(new Long(3003), "1234567890", "Peter", "Maffay",
                new BigDecimal(38.5), EnumEmployeeStatus.GESELLE, occupations.get(4)));
        user34.setAppRole(EnumApplicationRole.USER);
        user34.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        final User user35 = new User();
        user35.setName("franz");
        user35.setPassword("franz");
        user35.setEmployee(new Employee(new Long(3004), "1234567890", "Franz", "Beckenbauer",
                new BigDecimal(38.5), EnumEmployeeStatus.AUSZUBILDENDER, occupations.get(6)));
        user35.setAppRole(EnumApplicationRole.USER);
        user35.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        returnList.add(user32);
        returnList.add(user33);
        returnList.add(user34);
        returnList.add(user35);

        final User user36 = new User();
        user36.setName("toni");
        user36.setPassword("t");
        user36.setEmployee(new Employee(new Long(4001), "1234567890", "Toni", "Teamleiter",
                new BigDecimal(38.5), EnumEmployeeStatus.MEISTER, occupations.get(6)));
        user36.setAppRole(EnumApplicationRole.USER);
        user36.setImage(new Image("avatar", new Date(),
                this.imageService.getImageFileLocation("Avatar2", EnumImageMimeType.PNG)));

        returnList.add(user36);

        return returnList;
    }
}

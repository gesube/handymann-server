package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Colour;

@Service
public class ColourBuilder {

    public List<Colour> buildEntityListToPersist() {
        final List<Colour> returnList = new ArrayList<>();

        final Colour colour1 = new Colour();
        colour1.setName("grün");

        final Colour colour2 = new Colour();
        colour2.setName("schwarz");

        final Colour colour3 = new Colour();
        colour3.setName("weiß");

        final Colour colour4 = new Colour();
        colour4.setName("rot");

        final Colour colour5 = new Colour();
        colour5.setName("gelb");

        final Colour colour6 = new Colour();
        colour6.setName("lila");

        final Colour colour7 = new Colour();
        colour7.setName("hellblau");

        final Colour colour8 = new Colour();
        colour8.setName("dunkelblau");

        final Colour colour9 = new Colour();
        colour9.setName("grau");

        final Colour colour10 = new Colour();
        colour10.setName("pink");

        returnList.add(colour1);
        returnList.add(colour2);
        returnList.add(colour3);
        returnList.add(colour4);
        returnList.add(colour5);
        returnList.add(colour6);
        returnList.add(colour7);
        returnList.add(colour8);
        returnList.add(colour9);
        returnList.add(colour10);

        return returnList;
    }
}

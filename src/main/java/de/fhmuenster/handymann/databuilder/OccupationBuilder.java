package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Occupation;

@Service
public class OccupationBuilder {

    public List<Occupation> buildEntityListToPersist() {
        final List<Occupation> returnList = new ArrayList<>();

        final Occupation occ1 = new Occupation();
        occ1.setName("Elektriker");

        final Occupation occ2 = new Occupation();
        occ2.setName("Schreiner");
        
        final Occupation occ3 = new Occupation();
        occ3.setName("Maurer");
        
        final Occupation occ4 = new Occupation();
        occ4.setName("Fliesenleger");
        
        final Occupation occ5 = new Occupation();
        occ5.setName("Bodenleger");
        
        final Occupation occ6 = new Occupation();
        occ6.setName("Maler");
        
        final Occupation occ7 = new Occupation();
        occ7.setName("Tapezierer");
        
        final Occupation occ8 = new Occupation();
        occ8.setName("Dachdecker");
        
        final Occupation occ9 = new Occupation();
        occ9.setName("Rohrleitungsbauer");
        
        final Occupation occ10 = new Occupation();
        occ10.setName("Trockenbaumonteur");

        returnList.add(occ1);
        returnList.add(occ2);
        returnList.add(occ3);
        returnList.add(occ4);
        returnList.add(occ5);
        returnList.add(occ6);
        returnList.add(occ7);
        returnList.add(occ8);
        returnList.add(occ9);
        returnList.add(occ10);

        return returnList;
    }
}

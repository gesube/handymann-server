package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Colour;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Tool;

@Service
public class ToolBuilder {

    public List<Tool> buildEntityListToPersist(final List<Occupation> occupations,
            final List<Manufacturer> manufacturers, final List<Colour> colours, final List<Image> images) {
        final List<Tool> returnList = new ArrayList<>();

        final Tool tool1 = new Tool();
        tool1.setName("Schraubendreher");
        tool1.setModel("Kreuz");
        tool1.setManufacturer(manufacturers.get(0));
        tool1.setColour(colours.get(0));
        tool1.setAvailableForTheseOccupations(occupations);
        tool1.setImage(images.get(26));

        final Tool tool2 = new Tool();
        tool2.setName("Holzgliederklappmaßstab");
        tool2.setModel("2 Meter");
        tool2.setManufacturer(manufacturers.get(1));
        tool2.setColour(colours.get(1));
        tool2.setAvailableForTheseOccupations(occupations);
        tool2.setImage(images.get(27));
        
        final Tool tool3 = new Tool();
        tool3.setName("Spachtel");
        tool3.setModel("Flächen");
        tool3.setManufacturer(manufacturers.get(2));
        tool3.setColour(colours.get(2));
        tool3.setAvailableForTheseOccupations(occupations);
        tool3.setImage(images.get(28));
        
        final Tool tool4 = new Tool();
        tool4.setName("Wasserwaage");
        tool4.setModel("Laser");
        tool4.setManufacturer(manufacturers.get(3));
        tool4.setColour(colours.get(3));
        tool4.setAvailableForTheseOccupations(occupations);
        tool4.setImage(images.get(29));
        
        final Tool tool5 = new Tool();
        tool5.setName("Reibekelle");
        tool5.setModel("Beton");
        tool5.setManufacturer(manufacturers.get(4));
        tool5.setColour(colours.get(4));
        tool5.setAvailableForTheseOccupations(occupations);
        tool5.setImage(images.get(30));
        
        final Tool tool6 = new Tool();
        tool6.setName("Pinsel");
        tool6.setModel("Grob");
        tool6.setManufacturer(manufacturers.get(5));
        tool6.setColour(colours.get(5));
        tool6.setAvailableForTheseOccupations(occupations);
        tool6.setImage(images.get(31));
        
        final Tool tool7 = new Tool();
        tool7.setName("Tapetenroller");
        tool7.setModel("18 Centimeter");
        tool7.setManufacturer(manufacturers.get(6));
        tool7.setColour(colours.get(6));
        tool7.setAvailableForTheseOccupations(occupations);
        tool7.setImage(images.get(32));
        
        final Tool tool8 = new Tool();
        tool8.setName("Dachrinnenzange");
        tool8.setModel("Kunststoffbeschichtet");
        tool8.setManufacturer(manufacturers.get(7));
        tool8.setColour(colours.get(7));
        tool8.setAvailableForTheseOccupations(occupations);
        tool8.setImage(images.get(33));
        
        final Tool tool9 = new Tool();
        tool9.setName("Kappenheber");
        tool9.setModel("Rostig");
        tool9.setManufacturer(manufacturers.get(8));
        tool9.setColour(colours.get(8));
        tool9.setAvailableForTheseOccupations(occupations);
        tool9.setImage(images.get(34));
        
        final Tool tool10 = new Tool();
        tool10.setName("Hammer");
        tool10.setModel("Gummi");
        tool10.setManufacturer(manufacturers.get(9));
        tool10.setColour(colours.get(9));
        tool10.setAvailableForTheseOccupations(occupations);
        tool10.setImage(images.get(35));
        
        final Tool tool11 = new Tool();
        tool11.setName("Vorschlaghammer");
        tool11.setModel("Groß(1m)");
        tool11.setManufacturer(manufacturers.get(5));
        tool11.setColour(colours.get(2));
        tool11.setAvailableForTheseOccupations(occupations);
        tool11.setImage(images.get(36));
        
        final Tool tool12 = new Tool();
        tool12.setName("Schraubendreher");
        tool12.setModel("Torx");
        tool12.setManufacturer(manufacturers.get(8));
        tool12.setColour(colours.get(2));
        tool12.setAvailableForTheseOccupations(occupations);
        tool12.setImage(images.get(37));
        
        final Tool tool13 = new Tool();
        tool13.setName("Kombizange");
        tool13.setModel("Profi");
        tool13.setManufacturer(manufacturers.get(5));
        tool13.setColour(colours.get(2));
        tool13.setAvailableForTheseOccupations(occupations);
        tool13.setImage(images.get(38));
        
        final Tool tool14 = new Tool();
        tool14.setName("Phasenprüfer");
        tool14.setModel("Exklusiv");
        tool14.setManufacturer(manufacturers.get(6));
        tool14.setColour(colours.get(4));
        tool14.setAvailableForTheseOccupations(occupations);
        tool14.setImage(images.get(39));
        
        final Tool tool15 = new Tool();
        tool15.setName("Akkuschrauber");
        tool15.setModel("MKT150");
        tool15.setManufacturer(manufacturers.get(3));
        tool15.setColour(colours.get(3));
        tool15.setAvailableForTheseOccupations(occupations);
        tool15.setImage(images.get(1));

        returnList.add(tool1);
        returnList.add(tool2);
        returnList.add(tool3);
        returnList.add(tool4);
        returnList.add(tool5);
        returnList.add(tool6);
        returnList.add(tool7);
        returnList.add(tool8);
        returnList.add(tool9);
        returnList.add(tool10);
        returnList.add(tool11);
        returnList.add(tool12);
        returnList.add(tool13);
        returnList.add(tool14);
        returnList.add(tool15);
        return returnList;
    }
}

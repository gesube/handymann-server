package de.fhmuenster.handymann.databuilder;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;

@Service
public class TeamBuilder {

    public List<Team> buildEntityListToPersist(final List<User> users,
            final List<JobSite> jobSites) {
        final List<Team> returnList = new ArrayList<>();

        final Team team1 = new Team();
        team1.addMember(users.get(0));
        team1.addMember(users.get(1));
        team1.addMember(users.get(2));
        team1.addMember(users.get(3));
        team1.addMember(users.get(4));
        team1.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(5).toInstant()));
        team1.setCanBeChanged(false);

        final Team team2 = new Team();
        team2.addMember(users.get(0));
        team2.addMember(users.get(1));
        team2.addMember(users.get(2));
        team2.addMember(users.get(3));
        team2.addMember(users.get(4));
        team2.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(4).toInstant()));
        team2.setCanBeChanged(false);

        final Team team3 = new Team();
        team3.addMember(users.get(0));
        team3.addMember(users.get(1));
        team3.addMember(users.get(2));
        team3.addMember(users.get(3));
        team3.addMember(users.get(4));
        team3.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(3).toInstant()));
        team3.setCanBeChanged(false);

        final Team team4 = new Team();
        team4.addMember(users.get(0));
        team4.addMember(users.get(1));
        team4.addMember(users.get(2));
        team4.addMember(users.get(3));
        team4.addMember(users.get(4));
        team4.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(2).toInstant()));
        team4.setCanBeChanged(false);

        final Team team5 = new Team();
        team5.addMember(users.get(0));
        team5.addMember(users.get(1));
        team5.addMember(users.get(2));
        team5.addMember(users.get(3));
        team5.addMember(users.get(4));
        team5.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(1).toInstant()));
        team5.setCanBeChanged(false);

        final Team team6 = new Team();
        team6.addMember(users.get(0));
        team6.addMember(users.get(1));
        team6.addMember(users.get(2));
        team6.addMember(users.get(3));
        team6.addMember(users.get(4));
        team6.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).toInstant()));
        team6.setCanBeChanged(true);

        // -------------------------------------------------------

        final Team team7 = new Team();
        team7.addMember(users.get(5));
        team7.addMember(users.get(6));
        team7.addMember(users.get(7));
        team7.addMember(users.get(8));
        team7.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(5).toInstant()));
        team7.setCanBeChanged(false);

        final Team team8 = new Team();
        team8.addMember(users.get(5));
        team8.addMember(users.get(6));
        team8.addMember(users.get(7));
        team8.addMember(users.get(8));
        team8.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(4).toInstant()));
        team8.setCanBeChanged(false);

        final Team team9 = new Team();
        team9.addMember(users.get(5));
        team9.addMember(users.get(6));
        team9.addMember(users.get(7));
        team9.addMember(users.get(8));
        team9.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(3).toInstant()));
        team9.setCanBeChanged(false);

        final Team team10 = new Team();
        team10.addMember(users.get(5));
        team10.addMember(users.get(6));
        team10.addMember(users.get(7));
        team10.addMember(users.get(8));
        team10.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(2).toInstant()));
        team10.setCanBeChanged(false);

        final Team team11 = new Team();
        team11.addMember(users.get(5));
        team11.addMember(users.get(6));
        team11.addMember(users.get(7));
        team11.addMember(users.get(8));
        team11.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(1).toInstant()));
        team11.setCanBeChanged(false);

        final Team team12 = new Team();
        team12.addMember(users.get(5));
        team12.addMember(users.get(6));
        team12.addMember(users.get(7));
        team12.addMember(users.get(8));
        team12.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).toInstant()));
        team12.setCanBeChanged(true);
        team12.setCurrentJobSite(jobSites.get(0));


        // -------------------------------------------------------

        final Team team13 = new Team();
        team13.addMember(users.get(9));
        team13.addMember(users.get(10));
        team13.addMember(users.get(11));
        team13.addMember(users.get(12));
        team13.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(5).toInstant()));
        team13.setCanBeChanged(false);

        final Team team14 = new Team();
        team14.addMember(users.get(9));
        team14.addMember(users.get(10));
        team14.addMember(users.get(11));
        team14.addMember(users.get(12));
        team14.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(4).toInstant()));
        team14.setCanBeChanged(false);

        final Team team15 = new Team();
        team15.addMember(users.get(9));
        team15.addMember(users.get(10));
        team15.addMember(users.get(11));
        team15.addMember(users.get(12));
        team15.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(3).toInstant()));
        team15.setCanBeChanged(false);

        final Team team16 = new Team();
        team16.addMember(users.get(9));
        team16.addMember(users.get(10));
        team16.addMember(users.get(11));
        team16.addMember(users.get(12));
        team16.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(2).toInstant()));
        team16.setCanBeChanged(false);

        final Team team17 = new Team();
        team17.addMember(users.get(9));
        team17.addMember(users.get(10));
        team17.addMember(users.get(11));
        team17.addMember(users.get(12));
        team17.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).minusDays(1).toInstant()));
        team17.setCanBeChanged(false);

        final Team team18 = new Team();
        team18.addMember(users.get(9));
        team18.addMember(users.get(10));
        // team18.addMember(users.get(11));
        team18.addMember(users.get(12));
        team18.setValidForDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
                .withMinute(0).withSecond(0).withNano(0).toInstant()));
        team18.setCanBeChanged(false);
        team18.setCurrentJobSite(jobSites.get(0));

        // -------------------------------------------------------


        returnList.add(team1);
        returnList.add(team2);
        returnList.add(team3);
        returnList.add(team4);
        returnList.add(team5);
        returnList.add(team6);
        returnList.add(team7);
        returnList.add(team8);
        returnList.add(team9);
        returnList.add(team10);
        returnList.add(team11);
        returnList.add(team12);
        returnList.add(team13);
        returnList.add(team14);
        returnList.add(team15);
        returnList.add(team16);
        returnList.add(team17);
        returnList.add(team18);

        return returnList;
    }
}

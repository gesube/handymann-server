package de.fhmuenster.handymann.databuilder;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Material;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.embeddables.WorkTime;

@Service
public class JobBuilder {

    public List<Job> buildEntityListToPersist(final List<User> users, final List<JobSite> jobSites,
            final List<Team> teams, final List<Material> materials) {
        final List<Job> returnList = new ArrayList<>();

        final ZonedDateTime validForToday = ZonedDateTime.now(ZoneId.of("Europe/Berlin"))
                .withHour(0).withMinute(0).withSecond(0).withNano(0);

        final ZonedDateTime workTimeBegin = ZonedDateTime.now(ZoneId.of("Europe/Berlin"))
                .withHour(8).withMinute(0).withSecond(0);

        final ZonedDateTime workTimeEnd = workTimeBegin.plusHours(9);

        final ZonedDateTime breakTimeBegin = workTimeBegin.plusHours(5);

        final ZonedDateTime breakTimeEnd = breakTimeBegin.plusHours(1);

        final Job job1 = new Job();
        job1.setCanBeChanged(false);
        job1.setCreator(users.get(1));
        job1.setJobSite(jobSites.get(0));
        job1.setTeam(teams.get(1));
        job1.setValidForDate(Date.from(validForToday.minusDays(4).toInstant()));
        job1.setCreationDate(validForToday.minusDays(3));
        job1.setWorkTime(new WorkTime(workTimeBegin.minusDays(4), workTimeEnd.minusDays(4),
                breakTimeBegin.minusDays(4), breakTimeEnd.minusDays(4)));
        job1.addUsedMaterial(materials.get(0), new BigDecimal(2.5));

        final Job job2 = new Job();
        job2.setCanBeChanged(false);
        job2.setCreator(users.get(1));
        job2.setJobSite(jobSites.get(0));
        job2.setTeam(teams.get(2));
        job2.setValidForDate(Date.from(validForToday.minusDays(3).toInstant()));
        job2.setCreationDate(validForToday.minusDays(2));
        job2.setWorkTime(new WorkTime(workTimeBegin.minusDays(3), workTimeEnd.minusDays(3),
                breakTimeBegin.minusDays(3), breakTimeEnd.minusDays(3)));
        job2.addUsedMaterial(materials.get(1), new BigDecimal(100));
        job2.addUsedMaterial(materials.get(2), new BigDecimal(20));
        job2.addUsedMaterial(materials.get(3), new BigDecimal(30));

        final Job job3 = new Job();
        job3.setCanBeChanged(false);
        job3.setCreator(users.get(5));
        job3.setJobSite(jobSites.get(0));
        job3.setTeam(teams.get(9));
        job3.setValidForDate(Date.from(validForToday.minusDays(2).toInstant()));
        job3.setCreationDate(validForToday.minusDays(1));
        job3.setWorkTime(new WorkTime(workTimeBegin.minusDays(2), workTimeEnd.minusDays(2),
                breakTimeBegin.minusDays(2), breakTimeEnd.minusDays(2)));
        job3.addUsedMaterial(materials.get(4), new BigDecimal(40));
        job3.addUsedMaterial(materials.get(5), new BigDecimal(50));
        job3.addUsedMaterial(materials.get(6), new BigDecimal(10));
        job3.addUsedMaterial(materials.get(7), new BigDecimal(2));
        job3.addUsedMaterial(materials.get(8), new BigDecimal(250));

        final Job job4 = new Job();
        job4.setCanBeChanged(false);
        job4.setCreator(users.get(10));
        job4.setJobSite(jobSites.get(0));
        job4.setTeam(teams.get(16));
        job4.setValidForDate(Date.from(validForToday.minusDays(1).toInstant()));
        job4.setCreationDate(validForToday);
        job4.setWorkTime(new WorkTime(workTimeBegin.minusDays(1), workTimeEnd.minusDays(1),
                breakTimeBegin.minusDays(1), breakTimeEnd.minusDays(1)));
        job4.addUsedMaterial(materials.get(7), new BigDecimal(100));

        final Job job5 = new Job();
        job5.setCanBeChanged(false);
        job5.setCreator(users.get(10));
        job5.setJobSite(jobSites.get(0));
        job5.setTeam(teams.get(17));
        job5.setValidForDate(Date.from(validForToday.minusDays(0).toInstant()));
        job5.setCreationDate(ZonedDateTime.now(ZoneId.of("Europe/Berlin")));
        job5.setWorkTime(new WorkTime(workTimeBegin, workTimeEnd, breakTimeBegin, breakTimeEnd));
        job5.addUsedMaterial(materials.get(8), new BigDecimal(2));
        job5.addUsedMaterial(materials.get(9), new BigDecimal(3));
        
//        final Job job6 = new Job();
//        job6.setCanBeChanged(false);
//        job6.setCreator(users.get(1));
//        job6.setJobSite(jobSites.get(0));
//        job6.setTeam(teams.get(5));
//        job6.setValidForDate(Date.from(validForToday.minusDays(0).toInstant()));
//        job6.setCreationDate(ZonedDateTime.now(ZoneId.of("Europe/Berlin")));
//        job6.setWorkTime(new WorkTime(workTimeBegin, workTimeEnd, breakTimeBegin, breakTimeEnd));
//        job6.addUsedMaterial(materials.get(10), new BigDecimal(2));
//        job6.addUsedMaterial(materials.get(11), new BigDecimal(3));

        returnList.add(job1);
        returnList.add(job2);
        returnList.add(job3);
        returnList.add(job4);
        returnList.add(job5);
//        returnList.add(job6);

        return returnList;
    }
}

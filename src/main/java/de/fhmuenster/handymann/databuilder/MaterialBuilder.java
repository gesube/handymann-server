package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Colour;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.entities.Material;

@Service
public class MaterialBuilder {

    public List<Material> buildEntityListToPersist(final List<Manufacturer> manufacturers,
            final List<Colour> colours, final List<Image> images) {
        final List<Material> returnList = new ArrayList<>();

        final Material material1 = new Material();
        material1.setName("Schrumpfschlauch");
        material1.setModel("Dicke: 10 mm");
        material1.setManufacturer(manufacturers.get(0));
        material1.setColour(colours.get(0));
        material1.setUnit("m");
        material1.setImage(images.get(40));

        final Material material2 = new Material();
        material2.setName("Kabelbinder");
        material2.setModel("Länge: 10 cm");
        material2.setManufacturer(manufacturers.get(1));
        material2.setColour(colours.get(1));
        material2.setUnit("Stk");
        material2.setImage(images.get(41));
        
        final Material material3 = new Material();
        material3.setName("Nägel");
        material3.setModel("kurz");
        material3.setManufacturer(manufacturers.get(2));
        material3.setColour(colours.get(2));
        material3.setUnit("Pkt");
        material3.setImage(images.get(42));
        
        final Material material4 = new Material();
        material4.setName("Schrauben");
        material4.setModel("Kreuz");
        material4.setManufacturer(manufacturers.get(3));
        material4.setColour(colours.get(3));
        material4.setUnit("Pkt");
        material4.setImage(images.get(43));
        
        final Material material5 = new Material();
        material5.setName("Schrauben");
        material5.setModel("Schlitz");
        material5.setManufacturer(manufacturers.get(8));
        material5.setColour(colours.get(8));
        material5.setUnit("Pkt");
        material5.setImage(images.get(43));
        
        final Material material6 = new Material();
        material6.setName("Dübel");
        material6.setModel("Metall");
        material6.setManufacturer(manufacturers.get(5));
        material6.setColour(colours.get(5));
        material6.setUnit("Pkt");
        material6.setImage(images.get(45));
        
        final Material material7 = new Material();
        material7.setName("Dübel");
        material7.setModel("Kunststoff");
        material7.setManufacturer(manufacturers.get(6));
        material7.setColour(colours.get(6));
        material7.setUnit("Pkt");
        material7.setImage(images.get(44));
        
        final Material material8 = new Material();
        material8.setName("Mörtel");
        material8.setModel("Brandschutz");
        material8.setManufacturer(manufacturers.get(7));
        material8.setColour(colours.get(7));
        material8.setUnit("kg");
        material8.setImage(images.get(46));
        
        final Material material9 = new Material();
        material9.setName("Glaswolle");
        material9.setModel("Dämmung");
        material9.setManufacturer(manufacturers.get(4));
        material9.setColour(colours.get(4));
        material9.setUnit("m3");
        material9.setImage(images.get(47));
        
        final Material material10 = new Material();
        material10.setName("Rohr");
        material10.setModel("Kunststoff");
        material10.setManufacturer(manufacturers.get(9));
        material10.setColour(colours.get(9));
        material10.setUnit("m");
        material10.setImage(images.get(48));
        
        final Material material11 = new Material();
        material11.setName("Kabel");
        material11.setModel("3x1.5");
        material11.setManufacturer(manufacturers.get(5));
        material11.setColour(colours.get(2));
        material11.setUnit("m");
        material11.setImage(images.get(49));
        
        final Material material12 = new Material();
        material12.setName("Kalkfarbe");
        material12.setModel("Kalkfarbe für Kellerräume");
        material12.setManufacturer(manufacturers.get(0));
        material12.setColour(colours.get(6));
        material12.setUnit("l");
        material12.setImage(images.get(50));
        
        final Material material13 = new Material();
        material13.setName("Kleister");
        material13.setModel("Tapetenkleister Superfest");
        material13.setManufacturer(manufacturers.get(3));
        material13.setColour(colours.get(0));
        material13.setUnit("kg");
        material13.setImage(images.get(51));
        
        final Material material14 = new Material();
        material14.setName("Konstruktionsholz");
        material14.setModel("Konstruktionsholz 40mmx40mm");
        material14.setManufacturer(manufacturers.get(5));
        material14.setColour(colours.get(7));
        material14.setUnit("m");
        material14.setImage(images.get(52));
        
        final Material material15 = new Material();
        material15.setName("Kies");
        material15.setModel("Kies zum Pflastern");
        material15.setManufacturer(manufacturers.get(8));
        material15.setColour(colours.get(7));
        material15.setUnit("kg");
        material15.setImage(images.get(53));
        
        
        
        returnList.add(material1);
        returnList.add(material2);
        returnList.add(material3);
        returnList.add(material4);
        returnList.add(material5);
        returnList.add(material6);
        returnList.add(material7);
        returnList.add(material8);
        returnList.add(material9);
        returnList.add(material10);
        returnList.add(material11);
        returnList.add(material12);
        returnList.add(material13);
        returnList.add(material14);
        returnList.add(material15);
        
        return returnList;
    }
}

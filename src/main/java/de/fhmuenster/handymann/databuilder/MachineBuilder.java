package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Colour;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Machine;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.entities.Team;

@Service
public class MachineBuilder {

    public List<Machine> buildEntityListToPersist(final List<Team> teams,
            final List<Manufacturer> manufacturers, final List<Colour> colours,
            final List<Image> images) {
        final List<Machine> returnList = new ArrayList<>();

        final Machine machine1 = new Machine();
        machine1.setName("Schlagschrauber");
        machine1.setModel("600 nm");
        machine1.setManufacturer(manufacturers.get(1));
        machine1.setColour(colours.get(0));
        machine1.setCurrentPossessor(teams.get(5));
        machine1.setImage(images.get(11));

        final Machine machine2 = new Machine();
        machine2.setName("Presslufthammer");
        machine2.setModel("beste Ausführung");
        machine2.setManufacturer(manufacturers.get(2));
        machine2.setColour(colours.get(9));
        machine2.setCurrentPossessor(teams.get(11));
        machine2.setImage(images.get(2));

        final Machine machine3 = new Machine();
        machine3.setName("Schlagbohrer");
        machine3.setModel("beste Ausführung");
        machine3.setManufacturer(manufacturers.get(3));
        machine3.setColour(colours.get(2));
        machine3.setCurrentPossessor(teams.get(5));
        machine3.setImage(images.get(12));

        final Machine machine4 = new Machine();
        machine4.setName("Kompaktbagger");
        machine4.setModel("beste Ausführung");
        machine4.setManufacturer(manufacturers.get(4));
        machine4.setColour(colours.get(6));
        machine4.setCurrentPossessor(teams.get(5));
        machine4.setImage(images.get(13));

        final Machine machine5 = new Machine();
        machine5.setName("Planierraupe");
        machine5.setModel("beste Ausführung");
        machine5.setManufacturer(manufacturers.get(5));
        machine5.setColour(colours.get(4));
        machine5.setCurrentPossessor(teams.get(11));
        machine5.setImage(images.get(14));

        final Machine machine6 = new Machine();
        machine6.setName("Mörtelpumpe");
        machine6.setModel("beste Ausführung");
        machine6.setManufacturer(manufacturers.get(6));
        machine6.setColour(colours.get(8));
        machine6.setCurrentPossessor(teams.get(11));
        machine6.setImage(images.get(15));

        final Machine machine7 = new Machine();
        machine7.setName("Rüttelplatte");
        machine7.setModel("beste Ausführung");
        machine7.setManufacturer(manufacturers.get(7));
        machine7.setColour(colours.get(5));
        machine7.setCurrentPossessor(teams.get(11));
        machine7.setImage(images.get(16));

        final Machine machine8 = new Machine();
        machine8.setName("Betonmischmaschine");
        machine8.setModel("beste Ausführung");
        machine8.setManufacturer(manufacturers.get(8));
        machine8.setColour(colours.get(2));
        machine8.setCurrentPossessor(teams.get(11));
        machine8.setImage(images.get(17));

        final Machine machine9 = new Machine();
        machine9.setName("Druckluftschleifer");
        machine9.setModel("beste Ausführung");
        machine9.setManufacturer(manufacturers.get(9));
        machine9.setColour(colours.get(2));
        machine9.setCurrentPossessor(teams.get(16));
        machine9.setImage(images.get(18));

        final Machine machine10 = new Machine();
        machine10.setName("Presszange");
        machine10.setModel("beste Ausführung");
        machine10.setManufacturer(manufacturers.get(8));
        machine10.setColour(colours.get(2));
        machine10.setCurrentPossessor(teams.get(16));
        machine10.setImage(images.get(19));

        final Machine machine11 = new Machine();
        machine11.setName("Druckluftnagler");
        machine11.setModel("beste Ausführung");
        machine11.setManufacturer(manufacturers.get(7));
        machine11.setColour(colours.get(2));
        machine11.setCurrentPossessor(teams.get(16));
        machine11.setImage(images.get(20));

        final Machine machine12 = new Machine();
        machine12.setName("Oberfräse");
        machine12.setModel("beste Ausführung");
        machine12.setManufacturer(manufacturers.get(6));
        machine12.setColour(colours.get(2));
        machine12.setCurrentPossessor(teams.get(16));
        machine12.setImage(images.get(21));

        returnList.add(machine1);
        returnList.add(machine2);
        returnList.add(machine3);
        returnList.add(machine4);
        returnList.add(machine5);
        returnList.add(machine6);
        returnList.add(machine7);
        returnList.add(machine8);
        returnList.add(machine9);
        returnList.add(machine10);
        returnList.add(machine11);
        returnList.add(machine12);

        return returnList;
    }
}

package de.fhmuenster.handymann.databuilder;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.enums.EnumImageMimeType;
import de.fhmuenster.handymann.services.ImageService;


@Service
public class ImageBuilder {

	@Autowired
	private ImageService imageservice;


	public List<Image> buildEntityListToPersist() {
		final List<Image> returnList = new ArrayList<>();

		final Image img1 = new Image();
		img1.setName("Schlagbohrer");
		img1.setFileLocation(
				this.imageservice.getImageFileLocation("BoschSchlagbohrer", EnumImageMimeType.JPG));

		final Image img2 = new Image();
		img2.setName("MakitaAkkuschrauber");
		img2.setFileLocation(this.imageservice.getImageFileLocation("MakitaAkkuschrauber",
				EnumImageMimeType.PNG));

		final Image img3 = new Image();
		img3.setName("Bosch-Presslufthammer");
		img3.setFileLocation(this.imageservice.getImageFileLocation("boschpresslufthammer",
				EnumImageMimeType.JPG));

		final Image img4 = new Image();
		img4.setName("Jobsite_Bild_1");
		img4.setFileLocation(
				this.imageservice.getImageFileLocation("Jobsite_1", EnumImageMimeType.JPG));

		final Image img5 = new Image();
		img5.setName("Jobsite_Bild_2");
		img5.setFileLocation(
				this.imageservice.getImageFileLocation("Jobsite_2", EnumImageMimeType.JPG));

		final Image img6 = new Image();
		img6.setName("Jobsite_Bild_3");
		img6.setFileLocation(
				this.imageservice.getImageFileLocation("Jobsite_3", EnumImageMimeType.JPG));

		final Image img7 = new Image();
		img7.setName("Jobsite_Bild_4");
		img7.setFileLocation(
				this.imageservice.getImageFileLocation("Jobsite_4", EnumImageMimeType.PNG));

		final Image img8 = new Image();
		img8.setName("Jobsite_Bild_5");
		img8.setFileLocation(
				this.imageservice.getImageFileLocation("Jobsite_5", EnumImageMimeType.JPG));

		final Image img9 = new Image();
		img9.setName("Makita_Akkuschrauber");
		img9.setFileLocation(this.imageservice.getImageFileLocation("MakitaAkkuschrauber",
				EnumImageMimeType.PNG));

		final Image img10 = new Image();
		img10.setName("Mercedes_Sprinter");
		img10.setFileLocation(
				this.imageservice.getImageFileLocation("MercedesSprinter", EnumImageMimeType.PNG));

		final Image img11 = new Image();
		img11.setName("Opel_Vivaro");
		img11.setFileLocation(
				this.imageservice.getImageFileLocation("OpelVivaro", EnumImageMimeType.PNG));

		final Image img12 = new Image();
		img12.setName("Schlagschrauber");
		img12.setFileLocation(
				this.imageservice.getImageFileLocation("Schlagschrauber", EnumImageMimeType.JPG));

		final Image img13 = new Image();
		img13.setName("Schlagbohrer");
		img13.setFileLocation(
				this.imageservice.getImageFileLocation("BoschSchlagbohrer", EnumImageMimeType.JPG));

		final Image img14 = new Image();
		img14.setName("Kompaktbagger");
		img14.setFileLocation(
				this.imageservice.getImageFileLocation("Kompaktbagger", EnumImageMimeType.JPG));

		final Image img15 = new Image();
		img15.setName("Planierraupe");
		img15.setFileLocation(
				this.imageservice.getImageFileLocation("Planierraupe", EnumImageMimeType.JPG));

		final Image img16 = new Image();
		img16.setName("Mörtelpumpe");
		img16.setFileLocation(
				this.imageservice.getImageFileLocation("Moertelpumpe", EnumImageMimeType.JPG));

		final Image img17 = new Image();
		img17.setName("Rüttelplatte");
		img17.setFileLocation(
				this.imageservice.getImageFileLocation("Ruettelplatte", EnumImageMimeType.JPG));

		final Image img18 = new Image();
		img18.setName("Betonmischmaschine");
		img18.setFileLocation(this.imageservice.getImageFileLocation("Betonmischmaschine",
				EnumImageMimeType.JPG));

		final Image img19 = new Image();
		img19.setName("Druckluftschleifer");
		img19.setFileLocation(this.imageservice.getImageFileLocation("Druckluftschleifer",
				EnumImageMimeType.JPG));

		final Image img20 = new Image();
		img20.setName("Presszange");
		img20.setFileLocation(
				this.imageservice.getImageFileLocation("Presszange", EnumImageMimeType.JPG));

		final Image img21 = new Image();
		img21.setName("Druckluftnagler");
		img21.setFileLocation(
				this.imageservice.getImageFileLocation("Druckluftnagler", EnumImageMimeType.JPG));

		final Image img22 = new Image();
		img22.setName("Oberfräse");
		img22.setFileLocation(
				this.imageservice.getImageFileLocation("Oberfraese", EnumImageMimeType.JPG));

		final Image img23 = new Image();
		img23.setName("AvatarFrau");
		img23.setFileLocation(
				this.imageservice.getImageFileLocation("Avatar1", EnumImageMimeType.PNG));

		final Image img24 = new Image();
		img24.setName("AvatarMann");
		img24.setFileLocation(
				this.imageservice.getImageFileLocation("Avatar2", EnumImageMimeType.PNG));

		final Image img25 = new Image();
		img25.setName("VWTouareg");
		img25.setFileLocation(
				this.imageservice.getImageFileLocation("Touareg", EnumImageMimeType.JPG));

		final Image img26 = new Image();
		img26.setName("VWTransporterPritschenwagen");
		img26.setFileLocation(
				this.imageservice.getImageFileLocation("Pritschenwagen", EnumImageMimeType.JPG));

		final Image img27 = new Image();
		img27.setName("SchraubendreherKreuz");
		img27.setFileLocation(this.imageservice.getImageFileLocation("SchraubendreherKreuz",
				EnumImageMimeType.JPG));

		final Image img28 = new Image();
		img28.setName("Holzgliederklappmaßstab");
		img28.setFileLocation(this.imageservice.getImageFileLocation("Holzgliederklappmaßstab",
				EnumImageMimeType.JPG));

		final Image img29 = new Image();
		img29.setName("Spachtel");
		img29.setFileLocation(
				this.imageservice.getImageFileLocation("Spachtel", EnumImageMimeType.JPG));

		final Image img30 = new Image();
		img30.setName("Wasserwaage");
		img30.setFileLocation(
				this.imageservice.getImageFileLocation("Wasserwaage", EnumImageMimeType.JPG));

		final Image img31 = new Image();
		img31.setName("Reibekelle");
		img31.setFileLocation(
				this.imageservice.getImageFileLocation("Reibekelle", EnumImageMimeType.JPG));

		final Image img32 = new Image();
		img32.setName("Pinsel");
		img32.setFileLocation(
				this.imageservice.getImageFileLocation("Pinsel", EnumImageMimeType.JPG));

		final Image img33 = new Image();
		img33.setName("Tapetenroller");
		img33.setFileLocation(
				this.imageservice.getImageFileLocation("Tapetenroller", EnumImageMimeType.JPG));

		final Image img34 = new Image();
		img34.setName("Dachrinnenzange");
		img34.setFileLocation(
				this.imageservice.getImageFileLocation("Dachrinnenzange", EnumImageMimeType.JPG));

		final Image img35 = new Image();
		img35.setName("Kappenheber");
		img35.setFileLocation(
				this.imageservice.getImageFileLocation("Kappenheber", EnumImageMimeType.JPG));

		final Image img36 = new Image();
		img36.setName("Hammer");
		img36.setFileLocation(
				this.imageservice.getImageFileLocation("Hammer", EnumImageMimeType.JPG));

		final Image img37 = new Image();
		img37.setName("Vorschlaghammer");
		img37.setFileLocation(
				this.imageservice.getImageFileLocation("Vorschlaghammer", EnumImageMimeType.JPG));

		final Image img38 = new Image();
		img38.setName("Schraubendreher");
		img38.setFileLocation(this.imageservice.getImageFileLocation("Schraubendrehertorx",
				EnumImageMimeType.JPG));

		final Image img39 = new Image();
		img39.setName("Kombizange");
		img39.setFileLocation(
				this.imageservice.getImageFileLocation("Kombizange", EnumImageMimeType.JPG));

		final Image img40 = new Image();
		img40.setName("Phasenprüfer");
		img40.setFileLocation(
				this.imageservice.getImageFileLocation("Phasenprüfer", EnumImageMimeType.JPG));

		final Image img41 = new Image();
		img41.setName("Schrumpfschlauch");
		img41.setFileLocation(
				this.imageservice.getImageFileLocation("Schrumpfschlauch", EnumImageMimeType.JPG));

		final Image img42 = new Image();
		img42.setName("Kabelbinder");
		img42.setFileLocation(
				this.imageservice.getImageFileLocation("Kabelbinder", EnumImageMimeType.JPG));

		final Image img43 = new Image();
		img43.setName("Naegel");
		img43.setFileLocation(
				this.imageservice.getImageFileLocation("Naegel", EnumImageMimeType.JPG));

		final Image img44 = new Image();
		img44.setName("Schrauben");
		img44.setFileLocation(
				this.imageservice.getImageFileLocation("Schrauben", EnumImageMimeType.JPG));

		final Image img45 = new Image();
		img45.setName("Duebel");
		img45.setFileLocation(
				this.imageservice.getImageFileLocation("Duebel", EnumImageMimeType.JPG));

		final Image img46 = new Image();
		img46.setName("DuebelMetall");
		img46.setFileLocation(
				this.imageservice.getImageFileLocation("DuebelMetall", EnumImageMimeType.JPG));

		final Image img47 = new Image();
		img47.setName("Moertel");
		img47.setFileLocation(
				this.imageservice.getImageFileLocation("Moertel", EnumImageMimeType.JPG));

		final Image img48 = new Image();
		img48.setName("Glaswolle");
		img48.setFileLocation(
				this.imageservice.getImageFileLocation("Glaswolle", EnumImageMimeType.JPG));

		final Image img49 = new Image();
		img49.setName("RohrKunstoff");
		img49.setFileLocation(
				this.imageservice.getImageFileLocation("RohrKunstoff", EnumImageMimeType.JPG));

		final Image img50 = new Image();
		img50.setName("Kabel 3x1.5");
		img50.setFileLocation(
				this.imageservice.getImageFileLocation("Kabel315", EnumImageMimeType.JPG));

		final Image img51 = new Image();
		img51.setName("Kalkfarbe");
		img51.setFileLocation(
				this.imageservice.getImageFileLocation("Kalkfarbe", EnumImageMimeType.JPG));

		final Image img52 = new Image();
		img52.setName("Kleister");
		img52.setFileLocation(
				this.imageservice.getImageFileLocation("Kleister", EnumImageMimeType.JPG));

		final Image img53 = new Image();
		img53.setName("Konstruktionsholz");
		img53.setFileLocation(
				this.imageservice.getImageFileLocation("Konstruktionsholz", EnumImageMimeType.JPG));

		final Image img54 = new Image();
		img54.setName("Kies");
		img54.setFileLocation(
				this.imageservice.getImageFileLocation("Kies", EnumImageMimeType.JPG));


		returnList.add(img1);
		returnList.add(img2);
		returnList.add(img3);
		returnList.add(img4);
		returnList.add(img5);
		returnList.add(img6);
		returnList.add(img7);
		returnList.add(img8);
		returnList.add(img9);
		returnList.add(img10);
		returnList.add(img11);
		returnList.add(img12);
		returnList.add(img13);
		returnList.add(img14);
		returnList.add(img15);
		returnList.add(img16);
		returnList.add(img17);
		returnList.add(img18);
		returnList.add(img19);
		returnList.add(img20);
		returnList.add(img21);
		returnList.add(img22);
		returnList.add(img23);
		returnList.add(img24);
		returnList.add(img25);
		returnList.add(img26);
		returnList.add(img27);
		returnList.add(img28);
		returnList.add(img29);
		returnList.add(img30);
		returnList.add(img31);
		returnList.add(img32);
		returnList.add(img33);
		returnList.add(img34);
		returnList.add(img35);
		returnList.add(img36);
		returnList.add(img37);
		returnList.add(img38);
		returnList.add(img39);
		returnList.add(img40);
		returnList.add(img41);
		returnList.add(img42);
		returnList.add(img43);
		returnList.add(img44);
		returnList.add(img45);
		returnList.add(img46);
		returnList.add(img47);
		returnList.add(img48);
		returnList.add(img49);
		returnList.add(img50);
		returnList.add(img51);
		returnList.add(img52);
		returnList.add(img53);
		returnList.add(img54);


		return returnList;
	}

}

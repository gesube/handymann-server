package de.fhmuenster.handymann.databuilder;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.dao.ColourDAO;
import de.fhmuenster.handymann.dao.ImageDAO;
import de.fhmuenster.handymann.dao.JobDAO;
import de.fhmuenster.handymann.dao.JobSiteDAO;
import de.fhmuenster.handymann.dao.MachineDAO;
import de.fhmuenster.handymann.dao.ManufacturerDAO;
import de.fhmuenster.handymann.dao.MaterialDAO;
import de.fhmuenster.handymann.dao.OccupationDAO;
import de.fhmuenster.handymann.dao.TaskDAO;
import de.fhmuenster.handymann.dao.TeamDAO;
import de.fhmuenster.handymann.dao.ToolDAO;
import de.fhmuenster.handymann.dao.UserDAO;
import de.fhmuenster.handymann.dao.VehicleDAO;
import de.fhmuenster.handymann.entities.Colour;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Machine;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.entities.Material;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Task;
import de.fhmuenster.handymann.entities.Team;
import de.fhmuenster.handymann.entities.Tool;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.Vehicle;

@Service
public class DataBuilder {

	@Autowired
	private OccupationDAO oDao;

	@Autowired
	private OccupationBuilder oBuilder;

	@Autowired
	private UserDAO uDao;

	@Autowired
	private UserBuilder uBuilder;

	@Autowired
	private TeamDAO teamDao;

	@Autowired
	private TeamBuilder teamBuilder;

	@Autowired
	private ColourDAO cDao;

	@Autowired
	private ColourBuilder cBuilder;

	@Autowired
	private ManufacturerDAO manDao;

	@Autowired
	private ManufacturerBuilder manBuilder;

	@Autowired
	private VehicleDAO vDao;

	@Autowired
	private VehicleBuilder vBuilder;

	@Autowired
	private ToolDAO toolDao;

	@Autowired
	private ToolBuilder toolBuilder;

	@Autowired
	private MachineDAO macDao;

	@Autowired
	private MachineBuilder macBuilder;

	@Autowired
	private MaterialDAO matDao;

	@Autowired
	private MaterialBuilder matBuilder;

	@Autowired
	private JobSiteDAO jsDao;

	@Autowired
	private JobSiteBuilder jsBuilder;

	@Autowired
	private JobDAO jDao;

	@Autowired
	private JobBuilder jBuilder;

	@Autowired
	private TaskDAO taskDao;

	@Autowired
	private TaskBuilder taskBuilder;

	@Autowired
	private ImageDAO imgDAO;

	@Autowired
	private ImageBuilder imgBuilder;


	@PostConstruct
	public void init() {
		// ------01. Occupations erstellen------
		final List<Occupation> occupations = this.oBuilder.buildEntityListToPersist();
		this.oDao.saveAll(occupations);

		// ------02. Bilder erstellen------
		final List<Image> images = this.imgBuilder.buildEntityListToPersist();
		this.imgDAO.saveAll(images);

		// ------03. User erstellen------
		final List<User> users = this.uBuilder.buildEntityListToPersist(occupations, images);
		this.uDao.saveAll(users);

		// ------11. JobSites erstellen------
		final List<JobSite> jobSites = this.jsBuilder.buildEntityListToPersist(users, images);
		this.jsDao.saveAll(jobSites);

		// ------04. Team erstellen------
		final List<Team> teams = this.teamBuilder.buildEntityListToPersist(users, jobSites);
		this.teamDao.saveAll(teams);

		// ------05. Colours erstellen------
		final List<Colour> colours = this.cBuilder.buildEntityListToPersist();
		this.cDao.saveAll(colours);

		// ------06. Manufacturers erstellen------
		final List<Manufacturer> manufacturer = this.manBuilder.buildEntityListToPersist();
		this.manDao.saveAll(manufacturer);

		// ------07. Vehicles erstellen------
		final List<Vehicle> vehicles =
				this.vBuilder.buildEntityListToPersist(teams, manufacturer, colours, images);
		this.vDao.saveAll(vehicles);

		// ------08. Tools erstellen------
		final List<Tool> tools = this.toolBuilder.buildEntityListToPersist(occupations,
				manufacturer, colours, images);
		this.toolDao.saveAll(tools);

		// ------09. Machines erstellen------
		final List<Machine> machines =
				this.macBuilder.buildEntityListToPersist(teams, manufacturer, colours, images);
		this.macDao.saveAll(machines);

		// ------10. Materials erstellen------
		final List<Material> materials =
				this.matBuilder.buildEntityListToPersist(manufacturer, colours, images);
		this.matDao.saveAll(materials);

		// ------12. Jobs erstellen------
		final List<Job> jobs =
				this.jBuilder.buildEntityListToPersist(users, jobSites, teams, materials);
		this.jDao.saveAll(jobs);

		// ------13. Tasks erstellen------
		final List<Task> tasks =
				this.taskBuilder.buildEntityListToPersist(jobSites, jobs, users, occupations);
		this.taskDao.saveAll(tasks);

	}
}

package de.fhmuenster.handymann.databuilder;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.Task;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.enums.EnumStatus;

@Service
public class TaskBuilder {

	public List<Task> buildEntityListToPersist(final List<JobSite> jobSites, final List<Job> jobs,
			final List<User> users, final List<Occupation> occupations) {
		final List<Task> returnList = new ArrayList<>();

		final Task todoTask1 = new Task();
		todoTask1.setTitle("Bier trinken.");
		todoTask1.setJobSite(jobSites.get(0));
		todoTask1.setStatus(EnumStatus.OPEN);
		todoTask1.setDurationInH(new BigDecimal(0.0));
		todoTask1.setCreator(users.get(0));
		todoTask1.setAvailableForTheseOccupations(occupations);

		final Task todoTask2 = new Task();
		todoTask2.setTitle("Pause machen.");
		todoTask2.setJobSite(jobSites.get(0));
		todoTask2.setStatus(EnumStatus.OPEN);
		todoTask2.setCreator(users.get(0));
		todoTask2.setAvailableForTheseOccupations(occupations);

		final Task doneTask1 = new Task();
		doneTask1.setTitle("Kasten Bier besorgen.");
		doneTask1.setJobSite(jobSites.get(0));
		doneTask1.setStatus(EnumStatus.CLOSED);
		doneTask1.setDurationInH(new BigDecimal(0.5));
		doneTask1.setCreator(users.get(0));
		doneTask1.setAssignee(users.get(3));
		doneTask1.setFinishDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).toInstant()));
		doneTask1.setAvailableForTheseOccupations(occupations);

		final Task doneTask2 = new Task();
		doneTask2.setTitle("Vom Bier holen erholen.");
		doneTask2.setJobSite(jobSites.get(0));
		doneTask2.setStatus(EnumStatus.CLOSED);
		doneTask2.setDurationInH(new BigDecimal(7.5));
		doneTask2.setJob(jobs.get(0));
		doneTask2.setCreator(users.get(0));
		doneTask2.setAssignee(users.get(3));
		doneTask2.setFinishDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).toInstant()));
		doneTask2.setAvailableForTheseOccupations(occupations);

		final Task todoTask3 = new Task();
		todoTask3.setTitle("WLAN Kabel verlegen in allen Räumen.");
		todoTask3.setJobSite(jobSites.get(0));
		todoTask3.setStatus(EnumStatus.OPEN);
		todoTask3.setDurationInH(new BigDecimal(8));
		todoTask3.setJob(jobs.get(0));
		todoTask3.setCreator(users.get(0));
		todoTask3.setAvailableForTheseOccupations(occupations);

		final Task todoTask4 = new Task();
		todoTask4.setTitle("Vom Mittagsschlaf in den Arbeitsmodus wechseln");
		todoTask4.setJobSite(jobSites.get(0));
		todoTask4.setStatus(EnumStatus.OPEN);
		todoTask4.setDurationInH(new BigDecimal(1));
		todoTask4.setJob(jobs.get(0));
		todoTask4.setCreator(users.get(0));
		todoTask4.setAvailableForTheseOccupations(occupations);

		final Task todoTask5 = new Task();
		todoTask5.setTitle("Vorbereitungsmaßnahmen");
		todoTask5.setDescription("Baustelle vorbereiten und andere die Arbeit machen lassen.");
		todoTask5.setJobSite(jobSites.get(0));
		todoTask5.setStatus(EnumStatus.OPEN);
		todoTask5.setDurationInH(new BigDecimal(1.5));
		todoTask5.setJob(jobs.get(0));
		todoTask5.setCreator(users.get(1));
		todoTask5.setAvailableForTheseOccupations(occupations);

		final Task doneTask3 = new Task();
		doneTask3.setTitle("Ohne Mampf kein Kampf");
		doneTask3.setDescription("Plan für das Mittagsessen schmieden.");
		doneTask3.setJobSite(jobSites.get(0));
		doneTask3.setStatus(EnumStatus.CLOSED);
		doneTask3.setDurationInH(new BigDecimal(7.5));
		doneTask3.setJob(jobs.get(0));
		doneTask3.setCreator(users.get(3));
		doneTask2.setFinishDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).toInstant()));
		doneTask3.setAssignee(users.get(3));
		doneTask3.setAvailableForTheseOccupations(occupations);

		final Task doneTask4 = new Task();
		doneTask4.setTitle("Urlaubsplan erstellen");
		doneTask4.setDescription("Und zwar während der Arbeitszeit erstellen.");
		doneTask4.setJobSite(jobSites.get(1));
		doneTask4.setStatus(EnumStatus.CLOSED);
		doneTask4.setDurationInH(new BigDecimal(7.5));
		doneTask4.setFinishDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).toInstant()));
		doneTask4.setJob(jobs.get(0));
		doneTask4.setCreator(users.get(7));
		doneTask4.setAssignee(users.get(3));
		doneTask4.setAvailableForTheseOccupations(occupations);

		final Task doneTask5 = new Task();
		doneTask5.setTitle("Lampe aufhängen");
		doneTask5.setDescription("in der Küche");
		doneTask5.setJobSite(jobSites.get(1));
		doneTask5.setStatus(EnumStatus.CLOSED);
		doneTask5.setDurationInH(new BigDecimal(4.5));
		doneTask5.setJob(jobs.get(0));
		doneTask5.setCreator(users.get(7));
		doneTask5.setAssignee(users.get(3));
		doneTask5.setFinishDate(Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).toInstant()));
		doneTask5.setAvailableForTheseOccupations(occupations);

		final Task todoTask6 = new Task();
		todoTask6.setTitle("Lampe aufhängen");
		todoTask6.setDescription("im Wohnzimmer");
		todoTask6.setJobSite(jobSites.get(0));
		todoTask6.setStatus(EnumStatus.OPEN);
		todoTask6.setDurationInH(new BigDecimal(2.5));
		todoTask6.setJob(jobs.get(0));
		todoTask6.setCreator(users.get(7));
		todoTask6.setAvailableForTheseOccupations(occupations);

		final Task todoTask7 = new Task();
		todoTask7.setTitle("Wand streichen");
		todoTask7.setDescription("am besten alle 4 des Raums");
		todoTask7.setJobSite(jobSites.get(0));
		todoTask7.setStatus(EnumStatus.OPEN);
		todoTask7.setDurationInH(new BigDecimal(7.5));
		todoTask7.setJob(jobs.get(0));
		todoTask7.setCreator(users.get(3));
		todoTask7.setAvailableForTheseOccupations(occupations);

		final Task todoTask8 = new Task();
		todoTask8.setTitle("Team-Building-Maßnahme");
		todoTask8.setDescription("Kaffee kochen.");
		todoTask8.setJobSite(jobSites.get(0));
		todoTask8.setStatus(EnumStatus.OPEN);
		todoTask8.setDurationInH(new BigDecimal(7.5));
		todoTask8.setJob(jobs.get(0));
		todoTask8.setCreator(users.get(2));
		todoTask8.setAvailableForTheseOccupations(occupations);

		final Task todoTask9 = new Task();
		todoTask9.setTitle("Neue Aufgabe überlegen");
		todoTask9.setJobSite(jobSites.get(0));
		todoTask9.setStatus(EnumStatus.OPEN);
		todoTask9.setDurationInH(new BigDecimal(0.5));
		todoTask9.setJob(jobs.get(0));
		todoTask9.setCreator(users.get(1));
		todoTask9.setAvailableForTheseOccupations(occupations);

		final Task todoTask10 = new Task();
		todoTask10.setTitle("Meeting mit Bauherr");
		todoTask10.setDescription("An der wichtigen Baubesprechung teilnehmen.");
		todoTask10.setJobSite(jobSites.get(0));
		todoTask10.setStatus(EnumStatus.OPEN);
		todoTask10.setDurationInH(new BigDecimal(2.5));
		todoTask10.setJob(jobs.get(0));
		todoTask10.setCreator(users.get(1));
		todoTask10.setAvailableForTheseOccupations(occupations);

		returnList.add(todoTask1);
		returnList.add(todoTask2);
		returnList.add(doneTask1);
		returnList.add(doneTask2);
		returnList.add(doneTask3);
		returnList.add(doneTask4);
		returnList.add(doneTask5);
		returnList.add(todoTask3);
		returnList.add(todoTask4);
		returnList.add(todoTask5);
		returnList.add(todoTask6);
		returnList.add(todoTask7);
		returnList.add(todoTask8);
		returnList.add(todoTask9);
		returnList.add(todoTask10);

		for (final Task task : returnList) {
			task.setAvailableForTheseOccupations(new ArrayList<Occupation>());
			for (final Occupation occupation : occupations) {
				task.addAvailableForThisOccupation(occupation);
			}
		}

		return returnList;
	}
}

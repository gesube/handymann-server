package de.fhmuenster.handymann;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandyMannApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandyMannApplication.class, args);
	}
}

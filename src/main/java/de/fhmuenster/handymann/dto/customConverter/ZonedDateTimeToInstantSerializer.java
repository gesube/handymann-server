package de.fhmuenster.handymann.dto.customConverter;

import java.io.IOException;
import java.time.ZonedDateTime;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ZonedDateTimeToInstantSerializer extends JsonSerializer<ZonedDateTime> {

    @Override
    public void serialize(final ZonedDateTime zonedDateTime, final JsonGenerator generator,
            final SerializerProvider provider) throws IOException {
        if (zonedDateTime != null) {
            generator.writeObject(zonedDateTime.toInstant());
        }
    }

}

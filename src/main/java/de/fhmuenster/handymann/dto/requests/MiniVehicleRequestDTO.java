package de.fhmuenster.handymann.dto.requests;

public class MiniVehicleRequestDTO {

    private Long teamId;

    /**
     * Public constructor.
     */
    public MiniVehicleRequestDTO() { /** just for always having the default constructor. */
    }

    public Long getTeamId() {
        return this.teamId;
    }

    public void setTeamId(final Long teamId) {
        this.teamId = teamId;
    }
}

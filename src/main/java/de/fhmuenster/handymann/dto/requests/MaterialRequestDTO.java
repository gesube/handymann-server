package de.fhmuenster.handymann.dto.requests;

public class MaterialRequestDTO {

    private String name;

    private String description;

    private String colourName;

    private String manufacturerName;

    private String unit;

    /**
     * Public constructor.
     */
    public MaterialRequestDTO() { /** just for always having the default constructor. */
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getColourName() {
        return this.colourName;
    }

    public void setColourName(final String colourName) {
        this.colourName = colourName;
    }

    public String getManufacturerName() {
        return this.manufacturerName;
    }

    public void setManufacturerName(final String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(final String unit) {
        this.unit = unit;
    }
}

package de.fhmuenster.handymann.dto.requests;

import java.math.BigDecimal;
import java.util.Date;
import de.fhmuenster.handymann.enums.EnumPriority;
import de.fhmuenster.handymann.enums.EnumStatus;

public class TaskRequestDTO {

	private EnumStatus status;

	private EnumPriority priority;

	private BigDecimal durationInH;

	private Date finishDate;

	/**
	 * Public constructor.
	 */
	public TaskRequestDTO() {

	}

	public EnumStatus getStatus() {
		return this.status;
	}

	public void setStatus(final EnumStatus status) {
		this.status = status;
	}

	public EnumPriority getPriority() {
		return this.priority;
	}

	public void setPriority(final EnumPriority priority) {
		this.priority = priority;
	}

	public BigDecimal getDurationInH() {
		return this.durationInH;
	}

	public void setDurationInH(final BigDecimal durationInH) {
		this.durationInH = durationInH;
	}

	public Date getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(final Date finishDate) {
		this.finishDate = finishDate;
	}
}

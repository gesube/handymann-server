package de.fhmuenster.handymann.dto.requests;

public class LoginRequestDTO {

    private String name;
    private String password;

    /**
     * Public constructor.
     */
    public LoginRequestDTO() { /** just for always having the default constructor. */
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}

package de.fhmuenster.handymann.dto.requests;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class StartAndEndDateRequestDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date endDate;

    /**
     * Public constructor.
     */
    public StartAndEndDateRequestDTO() { /** just for always having the default constructor. */
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }
}

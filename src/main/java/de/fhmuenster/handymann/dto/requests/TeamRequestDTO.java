package de.fhmuenster.handymann.dto.requests;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;

public class TeamRequestDTO {

    private boolean canBeChanged;

    private final List<Long> memberIds = new ArrayList<>();

    /**
     * Public constructor.
     */
    public TeamRequestDTO() { /** just for always having the default constructor. */
    }

    public boolean isCanBeChanged() {
        return this.canBeChanged;
    }

    public void setCanBeChanged(final boolean canBeChanged) {
        this.canBeChanged = canBeChanged;
    }

    public List<Long> getMemberIds() {
        return this.memberIds;
    }
}

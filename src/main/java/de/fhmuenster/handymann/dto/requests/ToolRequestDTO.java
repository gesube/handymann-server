package de.fhmuenster.handymann.dto.requests;

import java.math.BigDecimal;

public class ToolRequestDTO {

	private Long toolId;

	private BigDecimal quantity;

	/**
	 * Public constructor.
	 */
	public ToolRequestDTO() { /** just for always having the default constructor. */
	}

	public Long getToolId() {
		return this.toolId;
	}

	public void setToolId(final Long toolId) {
		this.toolId = toolId;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(final BigDecimal quantity) {
		this.quantity = quantity;
	}
}

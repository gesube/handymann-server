package de.fhmuenster.handymann.dto.requests;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class DateRequestDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date date;

    /**
     * Public constructor.
     */
    public DateRequestDTO() { /** just for always having the default constructor. */
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }
}

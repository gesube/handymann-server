package de.fhmuenster.handymann.dto.requests;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

public class JobRequestDTO {

    private Long jobSiteId;

    private WorkTimeRequestDTO workTimes;

    private Long teamId;

    private boolean canBeChanged;

    private Map<MaterialRequestDTO, BigDecimal> usedMaterial = new HashMap<>();


    /**
     * Public constructor.
     */
    public JobRequestDTO() { /** just for always having the default constructor. */
    }

    public Long getJobSiteId() {
        return this.jobSiteId;
    }

    public void setJobSiteId(final Long jobSiteId) {
        this.jobSiteId = jobSiteId;
    }

    public WorkTimeRequestDTO getWorkTimes() {
        return this.workTimes;
    }

    public void setWorkTimes(final WorkTimeRequestDTO workTimes) {
        this.workTimes = workTimes;
    }

    public Long getTeamId() {
        return this.teamId;
    }

    public void setTeamId(final Long teamId) {
        this.teamId = teamId;
    }

    public boolean isCanBeChanged() {
        return this.canBeChanged;
    }

    public void setCanBeChanged(final boolean canBeChanged) {
        this.canBeChanged = canBeChanged;
    }

    public Map<MaterialRequestDTO, BigDecimal> getUsedMaterial() {
        return this.usedMaterial;
    }

    public void setUsedMaterial(final Map<MaterialRequestDTO, BigDecimal> usedMaterial) {
        this.usedMaterial = usedMaterial;
    }
}

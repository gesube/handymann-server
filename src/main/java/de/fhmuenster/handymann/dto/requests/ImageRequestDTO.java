package de.fhmuenster.handymann.dto.requests;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import de.fhmuenster.handymann.enums.EnumEntityType;

public class ImageRequestDTO {

    private Long id;

    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
			timezone = "Europe/Berlin")
    private Date creationDate = new Date();

    private String imageAsBaseString;

    private EnumEntityType entityType;

    private Long entityId;

    /**
     * Public constructor.
     */
    public ImageRequestDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getImageAsBaseString() {
        return this.imageAsBaseString;
    }

    public void setImageAsBaseString(final String imageAsBaseString) {
        this.imageAsBaseString = imageAsBaseString;
    }

    public EnumEntityType getEntityType() {
        return this.entityType;
    }

    public void setEntityType(final EnumEntityType entityType) {
        this.entityType = entityType;
    }

    public Long getEntityId() {
        return this.entityId;
    }

    public void setEntityId(final Long entityId) {
        this.entityId = entityId;
    }
}

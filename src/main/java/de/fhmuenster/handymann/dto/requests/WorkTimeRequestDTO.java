package de.fhmuenster.handymann.dto.requests;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class WorkTimeRequestDTO {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date workTimeStart;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date workTimeEnd;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date breakTimeStart;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date breakTimeEnd;

    /**
     * Public constructor.
     */
    public WorkTimeRequestDTO() { /** just for always having the default constructor. */
    }

	public Date getWorkTimeStart() {
		return workTimeStart;
	}

	public void setWorkTimeStart(Date workTimeStart) {
		this.workTimeStart = workTimeStart;
	}

	public Date getWorkTimeEnd() {
		return workTimeEnd;
	}

	public void setWorkTimeEnd(Date workTimeEnd) {
		this.workTimeEnd = workTimeEnd;
	}

	public Date getBreakTimeStart() {
		return breakTimeStart;
	}

	public void setBreakTimeStart(Date breakTimeStart) {
		this.breakTimeStart = breakTimeStart;
	}

	public Date getBreakTimeEnd() {
		return breakTimeEnd;
	}

	public void setBreakTimeEnd(Date breakTimeEnd) {
		this.breakTimeEnd = breakTimeEnd;
	}
}

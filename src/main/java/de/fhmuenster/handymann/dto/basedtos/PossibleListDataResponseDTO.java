package de.fhmuenster.handymann.dto.basedtos;

import de.fhmuenster.handymann.dto.responses.ImageResponseDTO;

public class PossibleListDataResponseDTO {

    private Long id;

    private String name;

    private String model;

    private String colourName;

    private String manufacturerName;

    private ImageResponseDTO image;

    /**
     * Public constructor.
     */
    public PossibleListDataResponseDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public String getColourName() {
        return this.colourName;
    }

    public void setColourName(final String colourName) {
        this.colourName = colourName;
    }

    public String getManufacturerName() {
        return this.manufacturerName;
    }

    public void setManufacturerName(final String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public ImageResponseDTO getImage() {
        return this.image;
    }

    public void setImage(final ImageResponseDTO image) {
        this.image = image;
    }
}

package de.fhmuenster.handymann.dto.responses;

import de.fhmuenster.handymann.enums.EnumEmployeeStatus;

public class ProfileResponseDTO {

    private Long id;
    private String name;
    private String employeeTelephoneNumber;
    private String employeeFirstName;
    private String employeeLastName;
    private EnumEmployeeStatus employeeEmployeeStatus;
    private ImageResponseDTO image;

    /**
     * Public constructor.
     */
    public ProfileResponseDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmployeeTelephoneNumber() {
        return this.employeeTelephoneNumber;
    }

    public void setEmployeeTelephoneNumber(final String employeeTelephoneNumber) {
        this.employeeTelephoneNumber = employeeTelephoneNumber;
    }

    public String getEmployeeFirstName() {
        return this.employeeFirstName;
    }

    public void setEmployeeFirstName(final String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public String getEmployeeLastName() {
        return this.employeeLastName;
    }

    public void setEmployeeLastName(final String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public EnumEmployeeStatus getEmployeeEmployeeStatus() {
        return this.employeeEmployeeStatus;
    }

    public void setEmployeeEmployeeStatus(final EnumEmployeeStatus employeeEmployeeStatus) {
        this.employeeEmployeeStatus = employeeEmployeeStatus;
    }

    public ImageResponseDTO getImage() {
        return this.image;
    }

    public void setImage(final ImageResponseDTO image) {
        this.image = image;
    }
}

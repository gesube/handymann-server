package de.fhmuenster.handymann.dto.responses;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class ImageResponseDTO {

	private Long id;

	private String name;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
			timezone = "Europe/Berlin")
	private Date creationDate;

	private String imageAsBaseString;

	/**
	 * Public constructor.
	 */
	public ImageResponseDTO() { /** just for always having the default constructor. */
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getImageAsBaseString() {
		return this.imageAsBaseString;
	}

	public void setImageAsBaseString(final String imageAsBaseString) {
		this.imageAsBaseString = imageAsBaseString;
	}
}

package de.fhmuenster.handymann.dto.responses;

import java.math.BigDecimal;
import de.fhmuenster.handymann.enums.EnumPriority;

public class MiniTaskResponseDTO {

    private Long id;

    private String title;

    private String assigneeName;

    private String description;

    private BigDecimal durationInH;

    private EnumPriority priority;

    /**
     * Public constructor.
     */
    public MiniTaskResponseDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getAssigneeName() {
        return this.assigneeName;
    }

    public void setAssigneeName(final String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public BigDecimal getDurationInH() {
        return this.durationInH;
    }

    public void setDurationInH(final BigDecimal durationInH) {
        this.durationInH = durationInH;
    }

    public EnumPriority getPriority() {
        return this.priority;
    }

    public void setPriority(final EnumPriority priority) {
        this.priority = priority;
    }
}

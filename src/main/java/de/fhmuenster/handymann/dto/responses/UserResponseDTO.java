package de.fhmuenster.handymann.dto.responses;

public class UserResponseDTO {

    private Long id;

    private String name;

    /**
     * Public constructor.
     */
    public UserResponseDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}

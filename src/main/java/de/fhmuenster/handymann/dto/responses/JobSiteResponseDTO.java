package de.fhmuenster.handymann.dto.responses;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class JobSiteResponseDTO {
    private Long id;

    private String street = "null";

    private String postcode = "null";

    private String town = "null";

    private String country = "null";

    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
			timezone = "Europe/Berlin")
    private ZonedDateTime plannedStartDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime plannedEndDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime actualStartDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime actualEndDate;

    private boolean completed;
    
    private UserResponseDTO creator;
    
    private ImageResponseDTO creatorImage;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime creationDate;
    
    private List<ImageResponseDTO> images = new ArrayList<>();
    
    private Long countOpenTasks;
    
    private Long countClosedTasks;
    
    private Long countInProgressTasks;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    public String getTown() {
        return this.town;
    }

    public void setTown(final String town) {
        this.town = town;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public ZonedDateTime getPlannedStartDate() {
        return this.plannedStartDate;
    }

    public void setPlannedStartDate(final ZonedDateTime plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public ZonedDateTime getPlannedEndDate() {
        return this.plannedEndDate;
    }

    public void setPlannedEndDate(final ZonedDateTime plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public ZonedDateTime getActualStartDate() {
        return this.actualStartDate;
    }

    public void setActualStartDate(final ZonedDateTime actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public ZonedDateTime getActualEndDate() {
        return this.actualEndDate;
    }

    public void setActualEndDate(final ZonedDateTime actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    public void setCompleted(final boolean completed) {
        this.completed = completed;
    }

    public UserResponseDTO getCreator() {
        return this.creator;
    }

    public void setCreator(final UserResponseDTO creator) {
        this.creator = creator;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<ImageResponseDTO> getImages() {
        return this.images;
    }

    public void setImages(final List<ImageResponseDTO> images) {
        this.images = images;
    }

	public ImageResponseDTO getCreatorImage() {
		return creatorImage;
	}

	public void setCreatorImage(ImageResponseDTO creatorImage) {
		this.creatorImage = creatorImage;
	}

	public Long getCountOpenTasks() {
		return countOpenTasks;
	}

	public void setCountOpenTasks(Long countOpenTasks) {
		this.countOpenTasks = countOpenTasks;
	}

	public Long getCountClosedTasks() {
		return countClosedTasks;
	}

	public void setCountClosedTasks(Long countClosedTasks) {
		this.countClosedTasks = countClosedTasks;
	}

	public Long getCountInProgressTasks() {
		return countInProgressTasks;
	}

	public void setCountInProgressTasks(Long countInProgressTasks) {
		this.countInProgressTasks = countInProgressTasks;
	}
}

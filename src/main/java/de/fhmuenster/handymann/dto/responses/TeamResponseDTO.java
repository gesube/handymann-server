package de.fhmuenster.handymann.dto.responses;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;

public class TeamResponseDTO {

    private Long id;

    private List<UserResponseDTO> members = new ArrayList<>();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
            timezone = "Europe/Berlin")
    private Date validForDate;

    private boolean canBeChanged;
    
    private JobSiteResponseDTO currentJobSite;
    
    private VehicleResponseDTO currentVehicle;

    /**
     * Public constructor.
     */
    public TeamResponseDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public List<UserResponseDTO> getMembers() {
        return this.members;
    }

    public void setMembers(final List<UserResponseDTO> members) {
        this.members = members;
    }

    public Date getValidForDate() {
        return this.validForDate;
    }

    public void setValidForDate(final Date validForDate) {
        this.validForDate = validForDate;
    }

    public boolean isCanBeChanged() {
        return this.canBeChanged;
    }

    public void setCanBeChanged(final boolean canBeChanged) {
        this.canBeChanged = canBeChanged;
    }

	public JobSiteResponseDTO getCurrentJobSite() {
		return currentJobSite;
	}

	public void setCurrentJobSite(JobSiteResponseDTO currentJobSite) {
		this.currentJobSite = currentJobSite;
	}

	public VehicleResponseDTO getCurrentVehicle() {
		return currentVehicle;
	}

	public void setCurrentVehicle(VehicleResponseDTO currentVehicle) {
		this.currentVehicle = currentVehicle;
	}
}

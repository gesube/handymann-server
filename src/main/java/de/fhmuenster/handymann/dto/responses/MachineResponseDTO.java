package de.fhmuenster.handymann.dto.responses;

import de.fhmuenster.handymann.dto.basedtos.PossibleListDataResponseDTO;

public class MachineResponseDTO extends PossibleListDataResponseDTO {


	private Long currentPossessorId = Long.valueOf(0);

	/**
	 * Public constructor.
	 */
	public MachineResponseDTO() {
		super();
	}

	public Long getCurrentPossessorId() {
		return this.currentPossessorId;
	}

	public void setCurrentPossessorId(final Long currentPossessorId) {
		this.currentPossessorId = currentPossessorId;
	}
}

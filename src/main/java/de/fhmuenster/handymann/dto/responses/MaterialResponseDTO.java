package de.fhmuenster.handymann.dto.responses;

import de.fhmuenster.handymann.dto.basedtos.PossibleListDataResponseDTO;

public class MaterialResponseDTO extends PossibleListDataResponseDTO {

    private String unit;

    /**
     * Public constructor.
     */
    public MaterialResponseDTO() {
        super();
    }


    public String getUnit() {
        return this.unit;
    }

    public void setUnit(final String unit) {
        this.unit = unit;
    }
}

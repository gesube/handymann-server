package de.fhmuenster.handymann.dto.responses;

import de.fhmuenster.handymann.dto.basedtos.PossibleListDataResponseDTO;

public class ToolResponseDTO extends PossibleListDataResponseDTO {

    /**
     * Public constructor.
     */
    public ToolResponseDTO() {
        super();
    }


}

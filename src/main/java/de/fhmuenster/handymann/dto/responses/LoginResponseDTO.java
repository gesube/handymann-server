package de.fhmuenster.handymann.dto.responses;

import de.fhmuenster.handymann.enums.EnumApplicationRole;
import de.fhmuenster.handymann.enums.EnumEmployeeStatus;

public class LoginResponseDTO {

    private Long id;
    private String name;
    private EnumApplicationRole appRole;
    private Long employeeEmployeeNumber;
    private String employeeFirstName;
    private String employeeLastName;
    private EnumEmployeeStatus employeeEmployeeStatus;
    private ImageResponseDTO image;

    /**
     * Public constructor.
     */
    public LoginResponseDTO() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public EnumApplicationRole getAppRole() {
        return this.appRole;
    }

    public void setAppRole(final EnumApplicationRole appRole) {
        this.appRole = appRole;
    }

    public Long getEmployeeEmployeeNumber() {
        return this.employeeEmployeeNumber;
    }

    public void setEmployeeEmployeeNumber(final Long employeeEmployeeNumber) {
        this.employeeEmployeeNumber = employeeEmployeeNumber;
    }

    public String getEmployeeFirstName() {
        return this.employeeFirstName;
    }

    public void setEmployeeFirstName(final String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public String getEmployeeLastName() {
        return this.employeeLastName;
    }

    public void setEmployeeLastName(final String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public EnumEmployeeStatus getEmployeeEmployeeStatus() {
        return this.employeeEmployeeStatus;
    }

    public void setEmployeeEmployeeStatus(final EnumEmployeeStatus employeeEmployeeStatus) {
        this.employeeEmployeeStatus = employeeEmployeeStatus;
    }

    public ImageResponseDTO getImage() {
        return this.image;
    }

    public void setImage(final ImageResponseDTO image) {
        this.image = image;
    }
}

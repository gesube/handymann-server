package de.fhmuenster.handymann.dto.responses;

import de.fhmuenster.handymann.dto.basedtos.PossibleListDataResponseDTO;

public class VehicleResponseDTO extends PossibleListDataResponseDTO {

	private String numberPlate;

	private Long currentPossessorId = Long.valueOf(0);

	/**
	 * Public constructor.
	 */
	public VehicleResponseDTO() {
		super();
	}

	public String getNumberPlate() {
		return this.numberPlate;
	}

	public void setNumberPlate(final String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public Long getCurrentPossessorId() {
		return this.currentPossessorId;
	}

	public void setCurrentPossessorId(final Long currentPossessorId) {
		this.currentPossessorId = currentPossessorId;
	}
}

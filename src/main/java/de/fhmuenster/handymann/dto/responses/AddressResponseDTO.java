package de.fhmuenster.handymann.dto.responses;

public class AddressResponseDTO {

    private String street;

    private String postcode;

    private String town;

    private String country;

    /**
     * Public constructor.
     */
    public AddressResponseDTO() { /** just for always having the default constructor. */
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    public String getTown() {
        return this.town;
    }

    public void setTown(final String town) {
        this.town = town;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }
}

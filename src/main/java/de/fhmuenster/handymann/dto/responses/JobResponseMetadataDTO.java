package de.fhmuenster.handymann.dto.responses;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class JobResponseMetadataDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date dateOfOldestJob;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private Date dateOfNewestJob;

    private JobResponseDTO jobToDisplay;

    /**
     * Public constructor.
     */
    public JobResponseMetadataDTO() { /** just for always having the default constructor. */
    }

    public Date getDateOfOldestJob() {
        return this.dateOfOldestJob;
    }

    public void setDateOfOldestJob(final Date dateOfOldestJob) {
        this.dateOfOldestJob = dateOfOldestJob;
    }

    public Date getDateOfNewestJob() {
        return this.dateOfNewestJob;
    }

    public void setDateOfNewestJob(final Date dateOfNewestJob) {
        this.dateOfNewestJob = dateOfNewestJob;
    }

    public JobResponseDTO getJobToDisplay() {
        return this.jobToDisplay;
    }

    public void setJobToDisplay(final JobResponseDTO jobToDisplay) {
        this.jobToDisplay = jobToDisplay;
    }
}

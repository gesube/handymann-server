package de.fhmuenster.handymann.dto.responses;

import java.time.ZonedDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;

public class WorkTimeResponseDTO {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime workTimeStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime workTimeEnd;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime breakTimeStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime breakTimeEnd;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime associatedDate;

    public ZonedDateTime getWorkTimeStart() {
        return this.workTimeStart;
    }

    public void setWorkTimeStart(final ZonedDateTime workTimeStart) {
        this.workTimeStart = workTimeStart;
    }

    public ZonedDateTime getWorkTimeEnd() {
        return this.workTimeEnd;
    }

    public void setWorkTimeEnd(final ZonedDateTime workTimeEnd) {
        this.workTimeEnd = workTimeEnd;
    }

    public ZonedDateTime getBreakTimeStart() {
        return this.breakTimeStart;
    }

    public void setBreakTimeStart(final ZonedDateTime breakTimeStart) {
        this.breakTimeStart = breakTimeStart;
    }

    public ZonedDateTime getBreakTimeEnd() {
        return this.breakTimeEnd;
    }

    public void setBreakTimeEnd(final ZonedDateTime breakTimeEnd) {
        this.breakTimeEnd = breakTimeEnd;
    }

	public ZonedDateTime getAssociatedDate() {
		return associatedDate;
	}

	public void setAssociatedDate(ZonedDateTime associatedDate) {
		this.associatedDate = associatedDate;
	}
}

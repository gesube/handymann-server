package de.fhmuenster.handymann.dto.responses;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonFormat;

public class JobResponseDTO {

    private Long id;

    private WorkTimeResponseDTO workTime;

    private Long jobSiteId;
    private String jobSiteDescription;
    private AddressResponseDTO jobSiteAddress;

    private final List<MaterialResponseDTO> usedMaterialList = new ArrayList<>();
    private final List<BigDecimal> usedMaterialQuantities = new ArrayList<>();

    private Long teamId;

    private UserResponseDTO creator;
    private ImageResponseDTO creatorImage;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
            timezone = "Europe/Berlin")
    private ZonedDateTime creationDate;

    private boolean canBeChanged;

    /**
     * Public constructor.
     */
    public JobResponseDTO() {/** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public WorkTimeResponseDTO getWorkTime() {
        return this.workTime;
    }

    public void setWorkTime(final WorkTimeResponseDTO workTime) {
        this.workTime = workTime;
    }

    public Long getJobSiteId() {
        return this.jobSiteId;
    }

    public void setJobSiteId(final Long jobSiteId) {
        this.jobSiteId = jobSiteId;
    }

    public String getJobSiteDescription() {
        return this.jobSiteDescription;
    }

    public void setJobSiteDescription(final String jobSiteDescription) {
        this.jobSiteDescription = jobSiteDescription;
    }

    public AddressResponseDTO getJobSiteAddress() {
        return this.jobSiteAddress;
    }

    public void setJobSiteAddress(final AddressResponseDTO jobSiteAddress) {
        this.jobSiteAddress = jobSiteAddress;
    }

    public List<MaterialResponseDTO> getUsedMaterialList() {
        return this.usedMaterialList;
    }

    public List<BigDecimal> getUsedMaterialQuantities() {
        return this.usedMaterialQuantities;
    }

    public Long getTeamId() {
        return this.teamId;
    }

    public void setTeamId(final Long teamId) {
        this.teamId = teamId;
    }

    public UserResponseDTO getCreator() {
        return this.creator;
    }

    public void setCreator(final UserResponseDTO creator) {
        this.creator = creator;
    }

    public ImageResponseDTO getCreatorImage() {
        return this.creatorImage;
    }

    public void setCreatorImage(final ImageResponseDTO creatorImage) {
        this.creatorImage = creatorImage;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isCanBeChanged() {
        return this.canBeChanged;
    }

    public void setCanBeChanged(final boolean canBeChanged) {
        this.canBeChanged = canBeChanged;
    }
}

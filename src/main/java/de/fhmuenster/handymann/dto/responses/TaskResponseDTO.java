package de.fhmuenster.handymann.dto.responses;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import de.fhmuenster.handymann.enums.EnumPriority;
import de.fhmuenster.handymann.enums.EnumStatus;

public class TaskResponseDTO {

	private Long id;

	private String title;

	private Long creatorId;

	private String creatorName;

	private ImageResponseDTO creatorImage;

	private Long assigneeId;

	private String assigneeName;

	private ImageResponseDTO assigneeImage;

	private String description;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
			timezone = "Europe/Berlin")
	private Date creationDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy 00:00:00 Z",
			timezone = "Europe/Berlin")
	private Date finishDate;

	private BigDecimal durationInH;

	private EnumPriority priority;

	private EnumStatus status;

	private Long jobId;

	/**
	 * Public constructor.
	 */
	public TaskResponseDTO() { /** just for always having the default constructor. */
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public Long getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(final Long creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorName() {
		return this.creatorName;
	}

	public void setCreatorName(final String creatorName) {
		this.creatorName = creatorName;
	}

	public ImageResponseDTO getCreatorImage() {
		return this.creatorImage;
	}

	public void setCreatorImage(final ImageResponseDTO creatorImage) {
		this.creatorImage = creatorImage;
	}

	public Long getAssigneeId() {
		return this.assigneeId;
	}

	public void setAssigneeId(final Long assigneeId) {
		this.assigneeId = assigneeId;
	}

	public String getAssigneeName() {
		return this.assigneeName;
	}

	public void setAssigneeName(final String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public ImageResponseDTO getAssigneeImage() {
		return this.assigneeImage;
	}

	public void setAssigneeImage(final ImageResponseDTO assigneeImage) {
		this.assigneeImage = assigneeImage;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(final Date finishDate) {
		this.finishDate = finishDate;
	}

	public BigDecimal getDurationInH() {
		return this.durationInH;
	}

	public void setDurationInH(final BigDecimal durationInH) {
		this.durationInH = durationInH;
	}

	public EnumPriority getPriority() {
		return this.priority;
	}

	public void setPriority(final EnumPriority priority) {
		this.priority = priority;
	}

	public EnumStatus getStatus() {
		return this.status;
	}

	public void setStatus(final EnumStatus status) {
		this.status = status;
	}

	public Long getJobId() {
		return this.jobId;
	}

	public void setJobId(final Long jobId) {
		this.jobId = jobId;
	}
}

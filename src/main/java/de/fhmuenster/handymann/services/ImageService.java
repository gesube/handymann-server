package de.fhmuenster.handymann.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.enums.EnumImageMimeType;
import de.fhmuenster.handymann.enums.EnumImageUsageType;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.resizers.configurations.Rendering;

/**
 * provides methods for managing images
 *
 * @author Ruben van Lück
 */
@Service
public class ImageService {

    private static final Logger LOGGER = Logger.getLogger(ImageService.class);
    private final Path RESOURCE_DIR = Paths.get("src", "main", "resources", "IMG");
    private final Path TEMP_DIR = Paths.get("src", "main", "resources", "TEMP-IMG");

    @Autowired
    private DateService dateService;

    // einkommentieren, wenn ImageTest ausgeführt werden soll
    // private final DateService dateService = new DateService();

    /**
     * Decodes the given base64 encoded String into a byte[], detects the mimetype based on the
     * bytes,<br>
     * checks, if the mimetype is allowed and creates a file with an unique name in the archive
     * directory.
     *
     * @param codedString - an image as a base64 encoded String
     * @param usage - which kind of image it is (e.g. an avatar)
     * @return String - location and name of the image
     * @author Ruben van Lück
     */
    public String decodeImage(final String codedString, final EnumImageUsageType usage) {
        if (codedString != null && usage != null) {
            String toDecode = codedString;

            if (codedString.startsWith("data:")) {
                toDecode = codedString.substring(codedString.indexOf(',') + 1);
            }

            byte[] dataByteArray = null;
            String extension = null;

            try {
                dataByteArray = this.decodeString(toDecode);
                extension = this.detectType(dataByteArray);
            } catch (final Exception e) {
                LOGGER.debug("Decoding of the base64 string failed!");
                LOGGER.debug(e.getClass(), e);
            }

            try {
                File img = this.createFile(dataByteArray, extension);
                img = this.archiveImage(img, usage);
                return img.getAbsolutePath();
            } catch (final IOException e) {
                LOGGER.error("File couldn't be written into the archive directory.");
                LOGGER.error(e.getClass(), e);
            }
        }
        return null;
    }

    /**
     * Decodes the given base64 encoded String into a byte[]
     *
     * @param codedString - an image as a base64 encoded String
     * @return byte[] - the image as byte[]
     * @author Ruben van Lück
     */
    private byte[] decodeString(final String codedString) {
        return Base64.decodeBase64(codedString);
    }

    /**
     * Detects the mimetype based on the bytes,<br>
     * checks, if the mimetype is allowed and returns the correct file extension
     *
     * @param dataByteArray - an image as byte[]
     * @return String - the file extension, if the mimetype is allowed, otherwise null
     * @author Ruben van Lück
     */
    private String detectType(final byte[] dataByteArray) {
        final Tika tika = new Tika();
        final String mediaType = tika.detect(dataByteArray);

        for (final EnumImageMimeType b : EnumImageMimeType.values()) {
            if (b.getMimeType().equalsIgnoreCase(mediaType)) {
                return "." + b.name();
            }
        }
        return null;
    }


    /**
     * Creates a unique file name, converts the given byte[] to a file and saves the file into the
     * temp directory.
     *
     * @param dataByteArray - a file as byte[]
     * @param ext - the file extension
     * @return File - the created file
     * @throws IOException - if the temp directory doesn't exist or the byte[] couldn't be converted
     * @author Ruben van Lück
     */
    private File createFile(final byte[] dataByteArray, final String ext) throws IOException {
        if (dataByteArray == null || ext == null) {
            throw new IOException();
        } else {
            final String timestamp = this.dateService.createTimestamp();
            final String uuid = UUID.randomUUID().toString().substring(0, 15).toLowerCase();
            final String name = timestamp + "-" + uuid + ext;
            final File dir = new File(this.TEMP_DIR.toString());
            final File file = new File(dir + File.separator + name);

            FileUtils.writeByteArrayToFile(file, dataByteArray);
            return file;
        }
    }

    /**
     * Scales a copy of an image to the given size and saves the scaled copy into the archive
     * directory. <strong>CAUTION:</strong> JPGs without JFIF header will be converted incorrectly.
     *
     * @param file - an image
     * @param usage - which kind of image it is (e.g. an avatar)
     * @return File - the archived image
     * @throws IOException - if the archive directory doesn't exist
     * @author Ruben van Lück
     */
    private File archiveImage(final File file, final EnumImageUsageType usage) throws IOException {
        final File archiveDir = new File(this.RESOURCE_DIR.toAbsolutePath().toString());
        Thumbnails.of(file).size(usage.getWidth(), usage.getHeight()).rendering(Rendering.QUALITY);
        FileUtils.moveFileToDirectory(file, archiveDir, true);
        return new File(archiveDir + File.separator + file.getName());
    }

    /**
     * Encodes the given image to a base64 encoded string. <strong>CAUTION:</strong> JPGs without
     * JFIF header will be converted incorrectly.
     *
     * @param pathToImage - an image
     * @return String - a base64 encoded image - if no image was given, <code>null</code> is
     *         returned
     * @author Ruben van Lück
     */
    public String encodeImage(final String pathToImage) {
        String encodedString = null;
        final Tika tika = new Tika();

        if (pathToImage != null && pathToImage.toCharArray().length > 0) {
            final File file = new File(pathToImage);
            byte[] imgArray;

            try {
                String mediaType = tika.detect(file);
                mediaType = mediaType.toLowerCase();
                imgArray = FileUtils.readFileToByteArray(file);
                imgArray = Base64.encodeBase64(imgArray);
                encodedString = "data:" + mediaType + ";base64," + new String(imgArray);
            } catch (final IOException e) {
                LOGGER.error(e.getClass(), e);
            }
        }
        return encodedString;
    }


    /**
     * Gets the location of an image file with the given name and type.
     *
     * @param imageName - the name of the image file
     * @param imageType - the type of the image file
     * @return String - the location of the image file as a String
     * @author Ruben van Lück
     */
    public String getImageFileLocation(final String imageName, final EnumImageMimeType imageType) {
        String fileEnding = "";

        switch (imageType) {
            case JPG:
                fileEnding = ".jpg";
                break;
            case PNG:
                fileEnding = ".png";
                break;
            default:
                break;
        }

        final File file = new File(this.RESOURCE_DIR.toAbsolutePath().toString() + File.separator
                + imageName + fileEnding);
        return file.getAbsolutePath();
    }
}

package de.fhmuenster.handymann.services;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.dao.SessionDAO;
import de.fhmuenster.handymann.entities.Session;

/**
 *
 * @author Alexander Lüdiger-Schlüter
 * @author Ruben van Lück Service for User access validation
 */
@Service
public class UserVerificationService {
    public final static String HEADER_SESSION_IDENTIFIER = "session";

    @Autowired
    private SessionDAO sessionDao;

    /**
     * Gets session matching to the session id of the request and checks it for validity
     *
     * @param request the request containing the session id
     * @throws IllegalArgumentException if the session is invalid or not found
     */
    public Session getSession(final HttpServletRequest request) {
        final String sessionId = request.getHeader(HEADER_SESSION_IDENTIFIER);

        if (!this.checkValidSessionId(sessionId)) {
            throw new IllegalArgumentException("invalid session!");
        }

        final Session session = this.sessionDao.findBySessionId(sessionId);
        if (session == null) {
            throw new IllegalArgumentException("session not found!");
        }

        return session;
    }

    private boolean checkValidSessionId(final String sessionId) {
        if (sessionId == null) {
            return false;
        }
        return sessionId.matches(
                "[0-9a-f]{8,9}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$");
    }


    /**
     * Checks whether the given user exists in one of the teams held by the project.
     *
     * @param project - a Project object containing a list of Team objects
     * @param user - the User object to search for
     * @return boolean - true, if the given user was found, false otherwise
     */
    /**
     * public boolean hasUserAccessToProject(final Project project, final User user) { if
     * (project.getCreator().equals(user)) { return true; } else { for (final Team team :
     * project.getTeams()) { for (final User teammember : team.getUsers()) { if
     * (teammember.equals(user)) { return true; } } } } return false; }
     */
}

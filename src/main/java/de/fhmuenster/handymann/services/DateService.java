/**
 *
 */
package de.fhmuenster.handymann.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import org.springframework.stereotype.Service;

/**
 * provides methods for determining the current date
 * 
 * @author Ruben van Lück
 */
@Service
public class DateService {

    /**
     * Creates a String of the current German time, formatted to <code>"yyyyMMddHHmmssSSSSS"</code>.
     * 
     * @return String - the current timestamp, converted to German time<br>
     * @author Ruben van Lück
     */
    public String createTimestamp() {
        return new SimpleDateFormat("yyyyMMddHHmmssSSSSS")
                .format(Calendar.getInstance(TimeZone.getDefault()).getTime());
    }
}

package de.fhmuenster.handymann.services;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import de.fhmuenster.handymann.dto.responses.ImageResponseDTO;
import de.fhmuenster.handymann.dto.responses.JobResponseDTO;
import de.fhmuenster.handymann.dto.responses.JobSiteResponseDTO;
import de.fhmuenster.handymann.dto.responses.MaterialResponseDTO;
import de.fhmuenster.handymann.dto.responses.ToolResponseDTO;
import de.fhmuenster.handymann.dto.responses.UserResponseDTO;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Job;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Material;
import de.fhmuenster.handymann.entities.Tool;
import de.fhmuenster.handymann.entities.User;

/**
 * Service for mapping an entity to a DTO and reverse
 *
 * @author Ruben van Lück
 *
 */
@Service
public class ModelMappingService {

    private final static ModelMapper modelMapper = new ModelMapper();

    public ModelMappingService() {
        modelMapper.addMappings(new PropertyMap<JobSite, JobSiteResponseDTO>() {
            @Override
            protected void configure() {
                this.map().setCountry(this.source.getAddress().getCountry());
                this.map().setStreet(this.source.getAddress().getStreet());
                this.map().setTown(this.source.getAddress().getTown());
                this.map().setPostcode(this.source.getAddress().getPostcode());
                this.map()
                        .setActualStartDate(this.source.getStartAndEndDates().getActualStartDate());
                this.map().setActualEndDate(this.source.getStartAndEndDates().getActualEndDate());
                this.map().setPlannedStartDate(
                        this.source.getStartAndEndDates().getPlannedStartDate());
                this.map().setPlannedEndDate(this.source.getStartAndEndDates().getPlannedEndDate());
            }
        });
    }

    /**
     * Gets the global ModelMapper instance.
     *
     * @return ModelMapper - the global ModelMapper instance
     */
    public ModelMapper getModelMapper() {
        return modelMapper;
    }

    public List<JobResponseDTO> mapJobsToJobResponseDtos(final List<Job> jobs) {
        final Type listType = new TypeToken<List<JobResponseDTO>>() {}.getType();
        return modelMapper.map(jobs, listType);
    }

    public List<JobSiteResponseDTO> mapJobSitesToJobSiteResponseDtos(final List<JobSite> jobsites) {
        final Type listType = new TypeToken<List<JobSiteResponseDTO>>() {}.getType();
        return modelMapper.map(jobsites, listType);
    }

    public List<UserResponseDTO> mapUsersToUserResponseDtos(final List<User> users) {
        final Type listType = new TypeToken<List<JobResponseDTO>>() {}.getType();
        return modelMapper.map(users, listType);
    }

    public List<ToolResponseDTO> mapToolsToToolResponseDtos(final List<Tool> tools) {
        final Type listType = new TypeToken<List<ToolResponseDTO>>() {}.getType();
        return modelMapper.map(tools, listType);
    }

    public List<ImageResponseDTO> mapImagesToImageResponseDtos(final List<Image> images) {
        final Type listType = new TypeToken<List<ImageResponseDTO>>() {}.getType();
        return modelMapper.map(images, listType);
    }

    public Map<MaterialResponseDTO, BigDecimal> mapUsedMaterialsMap(
            final Map<Material, BigDecimal> materials) {
        final Type mapType = new TypeToken<Map<MaterialResponseDTO, BigDecimal>>() {}.getType();
        return modelMapper.map(materials, mapType);
    }


}

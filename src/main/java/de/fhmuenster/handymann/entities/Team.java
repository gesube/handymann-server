package de.fhmuenster.handymann.entities;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Entity for persisting teams.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "TEAM")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToMany
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.MERGE})
    private List<User> members = new ArrayList<>();

    @Column(name = "validForDate", nullable = false)
    private Date validForDate = Date.from(ZonedDateTime.now(ZoneId.of("Europe/Berlin")).withHour(0)
            .withMinute(0).withSecond(0).withNano(0).toInstant());

    @Column(name = "isCanBeChanged", nullable = false)
    private boolean canBeChanged = true;

    @ManyToOne
    @JoinColumn(nullable = true)
    private JobSite currentJobSite;

    /**
     * Public constructor.
     */
    public Team() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public List<User> getMembers() {
        return this.members;
    }

    public void setMembers(final List<User> members) {
        this.members = members;
    }

    public void addMember(final User member) {
        this.members.add(member);
    }

    public Date getValidForDate() {
        return this.validForDate;
    }

    public void setValidForDate(final Date validForDate) {
        this.validForDate = validForDate;
    }

    public boolean isCanBeChanged() {
        return this.canBeChanged;
    }

    public void setCanBeChanged(final boolean canBeChanged) {
        this.canBeChanged = canBeChanged;
    }

    public JobSite getCurrentJobSite() {
        return this.currentJobSite;
    }

    public void setCurrentJobSite(final JobSite currentJobSite) {
        this.currentJobSite = currentJobSite;
    }
}

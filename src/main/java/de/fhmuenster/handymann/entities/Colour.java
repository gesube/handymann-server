package de.fhmuenster.handymann.entities;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;

/**
 * Entity for persisting colours.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "COLOUR")
public class Colour {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "creationDate", nullable = false)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime creationDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    /**
     * Public constructor.
     */
    public Colour() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }
}

package de.fhmuenster.handymann.entities;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;
import de.fhmuenster.handymann.entities.embeddables.Employee;
import de.fhmuenster.handymann.enums.EnumApplicationRole;

/**
 * Entity for persisting users.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "password", nullable = false)
    private String password;

    @Embedded
    private Employee employee;

    @Column(name = "creationDate", nullable = false)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime creationDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private EnumApplicationRole appRole;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Image image;

    /**
     * Public constructor.
     */
    public User() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }


    public void setName(final String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(final Employee employee) {
        this.employee = employee;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public EnumApplicationRole getAppRole() {
        return this.appRole;
    }

    public void setAppRole(final EnumApplicationRole appRole) {
        this.appRole = appRole;
    }

    public Image getImage() {
        return this.image;
    }

    public void setImage(final Image image) {
        this.image = image;
    }
}

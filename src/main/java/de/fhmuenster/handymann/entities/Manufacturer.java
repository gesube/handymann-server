package de.fhmuenster.handymann.entities;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;
import de.fhmuenster.handymann.enums.EnumAvailableFor;

/**
 * Entity for persisting manufacturers.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "MANUFACTURER")
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "creationDate", nullable = false)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime creationDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @Column(name = "availableFor")
    @Enumerated(EnumType.STRING)
    private EnumAvailableFor availableFor;

    /**
     * Public constructor.
     */
    public Manufacturer() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public EnumAvailableFor getAvailableFor() {
        return this.availableFor;
    }

    public void setAvailableFor(final EnumAvailableFor availableFor) {
        this.availableFor = availableFor;
    }
}

package de.fhmuenster.handymann.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import de.fhmuenster.handymann.entities.superclasses.MaterialMachineToolVehicle;

/**
 * Entity for persisting machines.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "MACHINE")
public class Machine extends MaterialMachineToolVehicle {


    @OneToOne
    @JoinColumn(nullable = true)
    private Team currentPossessor;

    /**
     * Public constructor.
     */
    public Machine() { /** just for always having the default constructor. */
    }

    public Team getCurrentPossessor() {
        return this.currentPossessor;
    }

    public void setCurrentPossessor(final Team currentPossessor) {
        this.currentPossessor = currentPossessor;
    }
}

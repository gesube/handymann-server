package de.fhmuenster.handymann.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import de.fhmuenster.handymann.entities.superclasses.MaterialMachineToolVehicle;

/**
 * Entity for persisting materials.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "MATERIAL")
public class Material extends MaterialMachineToolVehicle {


	@Column(name = "unit", nullable = false)
    private String unit;
	
    /**
     * Public constructor.
     */
    public Material() { /** just for always having the default constructor. */
    }
    
    public String getUnit() {
    	return this.unit;
    }
    
    public void setUnit(final String unit) {
    	this.unit = unit;
    }
    

}

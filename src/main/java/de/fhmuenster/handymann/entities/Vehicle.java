package de.fhmuenster.handymann.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import de.fhmuenster.handymann.entities.superclasses.MaterialMachineToolVehicle;

/**
 * Entity for persisting vehicles.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "VEHICLE")
public class Vehicle extends MaterialMachineToolVehicle {

    @Column(name = "numberPlate", nullable = false)
    private String numberPlate;

    @OneToOne
    @JoinColumn(nullable = true)
    private Team currentPossessor;

    /**
     * Public constructor.
     */
    public Vehicle() { /** just for always having the default constructor. */
    }

    public String getNumberPlate() {
        return this.numberPlate;
    }

    public void setNumberPlate(final String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public Team getCurrentPossessor() {
        return this.currentPossessor;
    }

    public void setCurrentPossessor(final Team currentPossessor) {
        this.currentPossessor = currentPossessor;
    }
}

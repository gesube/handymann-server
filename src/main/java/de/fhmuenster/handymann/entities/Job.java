package de.fhmuenster.handymann.entities;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;
import de.fhmuenster.handymann.entities.embeddables.WorkTime;

/**
 * Entity for persisting jobs.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "JOB")
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private User creator;

    @Column(name = "creationDate", nullable = false)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime creationDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    private JobSite jobSite;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "JOB_USED_MATERIAL")
    @MapKeyColumn(name = "material")
    private Map<Material, BigDecimal> usedMaterial = new HashMap<>();

    @Embedded
    private WorkTime workTime;

    @ManyToOne
    @Cascade({CascadeType.MERGE})
    private Team team;

    @Column(name = "validForDate", nullable = false)
    private Date validForDate = new Date();

    @Column(name = "isCanBeChanged", nullable = false)
    private boolean canBeChanged = true;

    /**
     * Public constructor.
     */
    public Job() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public User getCreator() {
        return this.creator;
    }

    public void setCreator(final User creator) {
        this.creator = creator;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public JobSite getJobSite() {
        return this.jobSite;
    }

    public void setJobSite(final JobSite jobSite) {
        this.jobSite = jobSite;
    }

    public Map<Material, BigDecimal> getUsedMaterial() {
        return this.usedMaterial;
    }

    public void setUsedMaterial(final Map<Material, BigDecimal> usedMaterial) {
        this.usedMaterial = usedMaterial;
    }

    public void addUsedMaterial(final Material key, final BigDecimal value) {
        this.usedMaterial.put(key, value);
    }

    public WorkTime getWorkTime() {
        return this.workTime;
    }

    public void setWorkTime(final WorkTime workTime) {
        this.workTime = workTime;
    }

    public Team getTeam() {
        return this.team;
    }

    public void setTeam(final Team team) {
        this.team = team;
    }

    public Date getValidForDate() {
        return this.validForDate;
    }

    public void setValidForDate(final Date validForDate) {
        this.validForDate = validForDate;
    }

    public boolean isCanBeChanged() {
        return this.canBeChanged;
    }

    public void setCanBeChanged(final boolean canBeChanged) {
        this.canBeChanged = canBeChanged;
    }
}

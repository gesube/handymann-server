package de.fhmuenster.handymann.entities.embeddables;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;

/**
 * Embeddable class for start and end dates for project management.
 *
 * @author Ruben van Lück
 */
@Embeddable
public class StartAndEndDates {

    @Column(name = "plannedStartDate", nullable = true)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime plannedStartDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));;

    @Column(name = "plannedEndDate", nullable = true)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime plannedEndDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @Column(name = "actualStartDate", nullable = true)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime actualStartDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @Column(name = "actualEndDate", nullable = true)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime actualEndDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    /**
     * Public constructor.
     */
    public StartAndEndDates() { /** just for always having the default constructor. */
    }

    public StartAndEndDates(final ZonedDateTime plannedStartDate,
            final ZonedDateTime plannedEndDate, final ZonedDateTime actualStartDate,
            final ZonedDateTime actualEndDate) {
        this.setPlannedStartDate(plannedStartDate);
        this.setPlannedEndDate(plannedEndDate);
        this.setActualStartDate(actualStartDate);
        this.setActualEndDate(actualEndDate);
    }

    public ZonedDateTime getPlannedStartDate() {
        return this.plannedStartDate;
    }

    public void setPlannedStartDate(final ZonedDateTime plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public ZonedDateTime getPlannedEndDate() {
        return this.plannedEndDate;
    }

    public void setPlannedEndDate(final ZonedDateTime plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public ZonedDateTime getActualStartDate() {
        return this.actualStartDate;
    }

    public void setActualStartDate(final ZonedDateTime actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public ZonedDateTime getActualEndDate() {
        return this.actualEndDate;
    }

    public void setActualEndDate(final ZonedDateTime actualEndDate) {
        this.actualEndDate = actualEndDate;
    }
}

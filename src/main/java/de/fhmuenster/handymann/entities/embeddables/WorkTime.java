package de.fhmuenster.handymann.entities.embeddables;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;

/**
 * Embeddable class for work time data.
 *
 * @author Ruben van Lück
 */
@Embeddable
public class WorkTime {

    @Column(name = "workTimeStart")
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime workTimeStart;

    @Column(name = "workTimeEnd")
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime workTimeEnd;

    @Column(name = "breakTimeStart")
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime breakTimeStart;

    @Column(name = "breakTimeEnd")
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime breakTimeEnd;
    
    // Nur für Response benötigt, wird über Job persistiert
    private ZonedDateTime associatedDate;

    /**
     * Public constructor.
     */
    public WorkTime() { /** just for always having the default constructor. */
    }

    public WorkTime(final ZonedDateTime workTimeStart, final ZonedDateTime workTimeEnd,
            final ZonedDateTime breakTimeStart, final ZonedDateTime breakTimeEnd) {
        this.setWorkTimeStart(workTimeStart);
        this.setWorkTimeEnd(workTimeEnd);
        this.setBreakTimeStart(breakTimeStart);
        this.setBreakTimeEnd(breakTimeEnd);
    }

    public ZonedDateTime getWorkTimeStart() {
        return this.workTimeStart;
    }

    public void setWorkTimeStart(final ZonedDateTime workTimeStart) {
        this.workTimeStart = workTimeStart;
    }

    public ZonedDateTime getWorkTimeEnd() {
        return this.workTimeEnd;
    }

    public void setWorkTimeEnd(final ZonedDateTime workTimeEnd) {
        this.workTimeEnd = workTimeEnd;
    }

    public ZonedDateTime getBreakTimeStart() {
        return this.breakTimeStart;
    }

    public void setBreakTimeStart(final ZonedDateTime breakTimeStart) {
        this.breakTimeStart = breakTimeStart;
    }

    public ZonedDateTime getBreakTimeEnd() {
        return this.breakTimeEnd;
    }

    public void setBreakTimeEnd(final ZonedDateTime breakTimeEnd) {
        this.breakTimeEnd = breakTimeEnd;
    }

	public ZonedDateTime getAssociatedDate() {
		return associatedDate;
	}

	public void setAssociatedDate(ZonedDateTime associatedDate) {
		this.associatedDate = associatedDate;
	}
}

package de.fhmuenster.handymann.entities.embeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Embeddable class for address data.
 *
 * @author Ruben van Lück
 */
@Embeddable
public class Address {

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "postcode", nullable = false)
    private String postcode;

    @Column(name = "town", nullable = false)
    private String town;

    @Column(name = "country", nullable = false)
    private String country;

    /**
     * Public constructor.
     */
    public Address() { /** just for always having the default constructor. */
    }

    public Address(final String street, final String postcode, final String town,
            final String country) {
        this.setStreet(street);
        this.setPostcode(postcode);
        this.setTown(town);
        this.setCountry(country);
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    public String getTown() {
        return this.town;
    }

    public void setTown(final String town) {
        this.town = town;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

}

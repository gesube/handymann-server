package de.fhmuenster.handymann.entities.embeddables;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.enums.EnumEmployeeStatus;

/**
 * Embeddable class for employee data.
 *
 * @author Ruben van Lück
 */
@Embeddable
public class Employee {

    @Column(name = "employeeNumber", nullable = false, unique = true)
    private Long employeeNumber;

    @Column(name = "telephoneNumber", nullable = true)
    private String telephoneNumber;

    @Column(name = "firstName", nullable = false)
    private String firstName;

    @Column(name = "lastName", nullable = false)
    private String lastName;

    @Column(name = "workTimePerWeek")
    private BigDecimal workTimePerWeek;

    @Column(name = "employeeStatus")
    @Enumerated(EnumType.STRING)
    private EnumEmployeeStatus employeeStatus;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(nullable = false)
    private Occupation occupation;

    /**
     * Public constructor.
     */
    public Employee() { /** just for always having the default constructor. */
    }

    public Employee(final Long employeeNumber, final String firstName, final String lastName,
            final BigDecimal workTimePerWeek, final EnumEmployeeStatus employeeStatus,
            final Occupation occupation) {
        this.setEmployeeNumber(employeeNumber);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setWorkTimePerWeek(workTimePerWeek);
        this.setEmployeeStatus(employeeStatus);
        this.setOccupation(occupation);
    }

    public Employee(final Long employeeNumber, final String telephoneNumber, final String firstName,
            final String lastName, final BigDecimal workTimePerWeek,
            final EnumEmployeeStatus employeeStatus, final Occupation occupation) {
        this.setEmployeeNumber(employeeNumber);
        this.setTelephoneNumber(telephoneNumber);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setWorkTimePerWeek(workTimePerWeek);
        this.setEmployeeStatus(employeeStatus);
        this.setOccupation(occupation);
    }

    public Long getEmployeeNumber() {
        return this.employeeNumber;
    }

    public void setEmployeeNumber(final Long employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getTelephoneNumber() {
        return this.telephoneNumber;
    }

    public void setTelephoneNumber(final String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getWorkTimePerWeek() {
        return this.workTimePerWeek;
    }

    public void setWorkTimePerWeek(final BigDecimal workTimePerWeek) {
        this.workTimePerWeek = workTimePerWeek;
    }

    public EnumEmployeeStatus getEmployeeStatus() {
        return this.employeeStatus;
    }

    public void setEmployeeStatus(final EnumEmployeeStatus employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public Occupation getOccupation() {
        return this.occupation;
    }

    public void setOccupation(final Occupation occupation) {
        this.occupation = occupation;
    }
}

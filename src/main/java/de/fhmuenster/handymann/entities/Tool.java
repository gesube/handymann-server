package de.fhmuenster.handymann.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import de.fhmuenster.handymann.entities.superclasses.MaterialMachineToolVehicle;

/**
 * Entity for persisting tools.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "TOOL")
public class Tool extends MaterialMachineToolVehicle {


    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @Fetch(FetchMode.SELECT)
    private List<Occupation> availableForTheseOccupations = new ArrayList<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "TOOL_ORDERS")
    @MapKeyColumn(name = "user")
    private Map<User, BigDecimal> toolOrders = new HashMap<>();

    /**
     * Public constructor.
     */
    public Tool() { /** just for always having the default constructor. */
    }

    public List<Occupation> getAvailableForTheseOccupations() {
        return this.availableForTheseOccupations;
    }

    public void setAvailableForTheseOccupations(
            final List<Occupation> availableForTheseOccupations) {
        this.availableForTheseOccupations = availableForTheseOccupations;
    }

    public void addAvailableForTheseOccupation(final Occupation availableForTheseOccupation) {
        this.availableForTheseOccupations.add(availableForTheseOccupation);
    }

    public Map<User, BigDecimal> getToolOrders() {
        return this.toolOrders;
    }

    public void setToolOrders(final Map<User, BigDecimal> toolOrders) {
        this.toolOrders = toolOrders;
    }

    public void addToolOrder(final User key, final BigDecimal value) {
        this.toolOrders.put(key, value);
    }
}

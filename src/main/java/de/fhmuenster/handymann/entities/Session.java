package de.fhmuenster.handymann.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity for persisting sessions.
 * @author Alexander Lüdiger-Schlüter
 */
@Entity @Table(name = "SESSION")
public class Session {
	@Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id")
	private Long id;
	@OneToOne(cascade = CascadeType.MERGE)
	private User user;
	@Column(name = "sessionId", nullable = false, unique = true)
	private String sessionId;
	@Column(name = "ipAdress", nullable = false)
	private String ipAdress;
	@Column(name = "date", nullable = false)
	private Date date;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}

	public Long getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}

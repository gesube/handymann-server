package de.fhmuenster.handymann.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import de.fhmuenster.handymann.enums.EnumPriority;
import de.fhmuenster.handymann.enums.EnumStatus;

/**
 * Entity for persisting tasks.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "TASK")
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "description")
	private String description;

	@OneToOne(cascade = CascadeType.MERGE)
	private User creator;

	@OneToOne(cascade = CascadeType.MERGE)
	private User assignee;

	@Column(name = "creationDate", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date creationDate = new Date();

	@Column(name = "finishDate", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date finishDate;

	@ManyToOne
	@JoinColumn(nullable = false)
	private JobSite jobSite;

	@ManyToOne
	@JoinColumn(nullable = true)
	private Job job;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private EnumStatus status = EnumStatus.OPEN;

	@Column(name = "priority", nullable = false)
	@Enumerated(EnumType.STRING)
	private EnumPriority priority = EnumPriority.NORMAL;

	@Column(name = "durationInH", nullable = true)
	private BigDecimal durationInH = new BigDecimal(0);

	@ManyToMany
	@JoinColumn(nullable = true)
	private List<Occupation> availableForTheseOccupations = new ArrayList<>();

	/**
	 * Public constructor.
	 */
	public Task() { /** just for always having the default constructor. */
	}

	public Long getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public User getCreator() {
		return this.creator;
	}

	public void setCreator(final User creator) {
		this.creator = creator;
	}

	public User getAssignee() {
		return this.assignee;
	}

	public void setAssignee(final User assignee) {
		this.assignee = assignee;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(final Date finishDate) {
		this.finishDate = finishDate;
	}

	public JobSite getJobSite() {
		return this.jobSite;
	}

	public void setJobSite(final JobSite jobSite) {
		this.jobSite = jobSite;
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob(final Job job) {
		this.job = job;
	}

	public EnumStatus getStatus() {
		return this.status;
	}

	public void setStatus(final EnumStatus status) {
		this.status = status;
	}

	public EnumPriority getPriority() {
		return this.priority;
	}

	public void setPriority(final EnumPriority priority) {
		this.priority = priority;
	}

	public BigDecimal getDurationInH() {
		return this.durationInH;
	}

	public void setDurationInH(final BigDecimal durationInH) {
		this.durationInH = durationInH;
	}

	public List<Occupation> getAvailableForTheseOccupations() {
		return this.availableForTheseOccupations;
	}

	public void setAvailableForTheseOccupations(
			final List<Occupation> availableForTheseOccupations) {
		this.availableForTheseOccupations = availableForTheseOccupations;
	}

	public void addAvailableForThisOccupation(final Occupation availableForThisOccupation) {
		this.availableForTheseOccupations.add(availableForThisOccupation);
	}
}

package de.fhmuenster.handymann.entities.dbconverter;

import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Konvertiert automatisch bei DB-Kommunikation zwischen ZonedDateTime (entity attribute) und
 * Timestamp (database column).
 *
 * @author Ruben van Lück
 *
 */
@Converter
public class ZonedDateTimeToTimestampConverter
        implements AttributeConverter<ZonedDateTime, Timestamp> {


    @Override
    public Timestamp convertToDatabaseColumn(final ZonedDateTime zonedDateTime) {
        if (zonedDateTime != null) {
            return Timestamp.from(zonedDateTime.toInstant());
        } else {
            return Timestamp.from(ZonedDateTime
                    .of(9999, 12, 31, 23, 59, 59, 59, ZoneId.systemDefault()).toInstant());
        }
    }

    @Override
    public ZonedDateTime convertToEntityAttribute(final Timestamp timestamp) {
        return ZonedDateTime.ofInstant(timestamp.toInstant(), ZoneId.systemDefault());
    }

}

package de.fhmuenster.handymann.entities;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;
import de.fhmuenster.handymann.entities.embeddables.Address;
import de.fhmuenster.handymann.entities.embeddables.StartAndEndDates;

/**
 * Entity for persisting job sites.
 *
 * @author Ruben van Lück
 */
@Entity
@Table(name = "JOB_SITE")
public class JobSite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Embedded
    private Address address;

    @Column(name = "description", nullable = false)
    private String description;

    @Embedded
    private StartAndEndDates startAndEndDates;

    @Column(name = "isCompleted", nullable = false)
    private boolean completed = false;

    @ManyToOne
    @JoinColumn(nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private User creator;

    @Column(name = "creationDate", nullable = false)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime creationDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @OneToMany
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.MERGE})
    private List<Image> images = new ArrayList<>();

    /**
     * Public constructor.
     */
    public JobSite() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(final Address address) {
        this.address = address;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public StartAndEndDates getStartAndEndDates() {
        return this.startAndEndDates;
    }

    public void setStartAndEndDates(final StartAndEndDates startAndEndDates) {
        this.startAndEndDates = startAndEndDates;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    public void setCompleted(final boolean completed) {
        this.completed = completed;
    }

    public User getCreator() {
        return this.creator;
    }

    public void setCreator(final User creator) {
        this.creator = creator;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<Image> getImages() {
        return this.images;
    }

    public void setImages(final List<Image> images) {
        this.images = images;
    }

    public void addImage(final Image image) {
        this.images.add(image);
    }
}

package de.fhmuenster.handymann.entities.superclasses;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import de.fhmuenster.handymann.entities.Colour;
import de.fhmuenster.handymann.entities.Image;
import de.fhmuenster.handymann.entities.Manufacturer;
import de.fhmuenster.handymann.entities.dbconverter.ZonedDateTimeToTimestampConverter;

/**
 * Superclass for materials, tools and vehicles.
 *
 * @author Ruben van Lück
 */
@MappedSuperclass
public class MaterialMachineToolVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "creationDate", nullable = false)
    @Convert(converter = ZonedDateTimeToTimestampConverter.class)
    private ZonedDateTime creationDate = ZonedDateTime.now(ZoneId.of("Europe/Berlin"));

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "model", nullable = true)
    private String model;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private Colour colour;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private Manufacturer manufacturer;

    @OneToOne(cascade = {CascadeType.MERGE})
    private Image image;

    /**
     * Public constructor.
     */
    public MaterialMachineToolVehicle() { /** just for always having the default constructor. */
    }

    public Long getId() {
        return this.id;
    }

    public ZonedDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(final String model) {
        this.model = model;
    }

    public Colour getColour() {
        return this.colour;
    }

    public void setColour(final Colour colour) {
        this.colour = colour;
    }

    public Manufacturer getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(final Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Image getImage() {
        return this.image;
    }

    public void setImage(final Image image) {
        this.image = image;
    }

}

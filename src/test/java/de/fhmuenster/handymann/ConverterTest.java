package de.fhmuenster.handymann;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import de.fhmuenster.handymann.dto.responses.JobSiteResponseDTO;
import de.fhmuenster.handymann.entities.JobSite;
import de.fhmuenster.handymann.entities.Occupation;
import de.fhmuenster.handymann.entities.User;
import de.fhmuenster.handymann.entities.embeddables.Address;
import de.fhmuenster.handymann.entities.embeddables.Employee;
import de.fhmuenster.handymann.entities.embeddables.StartAndEndDates;
import de.fhmuenster.handymann.enums.EnumApplicationRole;
import de.fhmuenster.handymann.enums.EnumEmployeeStatus;
import de.fhmuenster.handymann.enums.EnumImageMimeType;
import de.fhmuenster.handymann.services.ImageService;

public class ConverterTest {

    private final ImageService imageService = new ImageService();

    @Test
    public void test() {
        // ------01. Occupations erstellen------

        final Occupation occ1 = new Occupation();
        occ1.setName("Elektriker");

        // ------02. User erstellen------

        final User user1 = new User();
        user1.setName("boss");
        user1.setPassword("boss");
        user1.setEmployee(new Employee(new Long(1001), "Felix", "Blume", new BigDecimal(36),
                EnumEmployeeStatus.UNTERNEHMENSLEITUNG, occ1));
        user1.setAppRole(EnumApplicationRole.ADMINISTRATOR);

        // ------03. JobSites erstellen------

        final JobSite js1 = new JobSite();
        js1.setCreator(user1);
        js1.setDescription("Die Baustelle umme Ecke");
        js1.setAddress(new Address("Corrensstraße 25", "48149", "Münster", "Deutschland"));
        js1.setStartAndEndDates(new StartAndEndDates(ZonedDateTime.now(ZoneId.of("Europe/Berlin")),
                ZonedDateTime.now(ZoneId.of("Europe/Berlin")),
                ZonedDateTime.now(ZoneId.of("Europe/Berlin")), null));

        // ------04. Mapping------

        final ModelMapper modelMapper = new ModelMapper();

        modelMapper.addMappings(new PropertyMap<JobSite, JobSiteResponseDTO>() {
            @Override
            protected void configure() {
                this.map()
                        .setActualStartDate(this.source.getStartAndEndDates().getActualStartDate());
                this.map().setActualEndDate(this.source.getStartAndEndDates().getActualEndDate());
                this.map().setPlannedStartDate(
                        this.source.getStartAndEndDates().getPlannedStartDate());
                this.map().setPlannedEndDate(this.source.getStartAndEndDates().getPlannedEndDate());
            }
        });

        final JobSiteResponseDTO responseDto = modelMapper.map(js1, JobSiteResponseDTO.class);

        System.out.println(responseDto.getActualStartDate());
        System.out.println(responseDto.getActualEndDate());
        System.out.println(responseDto.getPlannedStartDate());
        System.out.println(responseDto.getPlannedEndDate());

    }

    @Test
    public void test2() {
        final String result = this.imageService.getImageFileLocation("test", EnumImageMimeType.JPG);
        System.out.println(result);
    }

}
